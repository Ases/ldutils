/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "resources.h"

#include <QString>
#include <QStringList>
#include <QResource>
#include <QStandardPaths>
#include <QApplication>
#include <QDir>
#include <QFile>

#include <src/util/util.h>

namespace Util {

bool LoadResourceFile(const QString& rccFile)
{
    // Search standard paths
    QStringList paths = QStandardPaths::locateAll(QStandardPaths::AppDataLocation, rccFile, QStandardPaths::LocateFile);

    // Search executable location as well.
    paths.append(QApplication::applicationDirPath() + QDir::separator() + rccFile);

    for (const auto& path : paths)
        if (QResource::registerResource(path))
            return true;

    fprintf(stderr, "%s: Unable to find local resource file %s\n",
            QApplication::applicationDisplayName().toUtf8().constData(),
            rccFile.toUtf8().constData());

    return false;
}

QByteArray pubkey()
{
    if (QFile pubkey(QStandardPaths::locate(QStandardPaths::AppDataLocation, "pubkey.asc")); pubkey.exists())
        if (pubkey.open(QIODevice::ReadOnly | QIODevice::Text))
            return Util::ReadFile<QByteArray>(pubkey, 1<<15);

    return QByteArray();
}

} // namespace Util
