/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdio>

#include "cmdline.h"
#include "resources.h"

void usage(const char* prog);

namespace Util {

bool ProcessArgs(const char* appname, const char* version,
                 int argc, char *argv[], int arg)
{
    if (strcmp(argv[arg], "--desktop") == 0 && arg < (argc-1)) {
        setenv("XDG_CURRENT_DESKTOP", argv[++arg], 1);
        return true;
    } else if (strcmp(argv[arg], "--xyzzy") == 0) {
        printf("nothing happens.\n");
        exit(0);
    } else if (strcmp(argv[arg], "--help") == 0 || strcmp(argv[arg], "-?") == 0) {
        usage(argv[0]);
        exit(0);
    } else if (strcmp(argv[arg], "--version") == 0) {
        printf("%s %s\n", appname, version);
        exit(0);
    }

    return false;
}

// Arguments we can only process after the application is created.
bool ProcessArgsPostApp(char* arg)
{
    if (strcmp(arg, "--pubkey") == 0) {
        if (const QByteArray key = Util::pubkey(); !key.isEmpty()) {
            printf("%s\n", key.constData());
            exit(0);
        } else {
            fprintf(stderr, "Public key not found.\n");
            exit(5);
        }

        return true; // This return can't be hit, but some compilers don't realize that.
    }

    return false;
}

} // namespace Util
