/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ui.h"

#include <QTabWidget>
#include <QMessageBox>
#include <QApplication>

#include <src/core/uicolormodel.h>
#include <src/ui/windows/mainwindowbase.h>

namespace Util {

void NextTab(QTabWidget* tabs, int reserve)
{
    if (const int current = tabs->currentIndex(); current < (tabs->count() - reserve))
        tabs->setCurrentIndex(current + 1);
}

void PrevTab(QTabWidget* tabs, int reserve)
{
    if (const int current = tabs->currentIndex(); current > reserve)
        tabs->setCurrentIndex(current - 1);
}

int WarningDialog(MainWindowBase& mainWindow, const QString& winTitle, const QString& msg, QWidget* parent)
{
    if (parent == nullptr)
        parent = &mainWindow;

    const QMessageBox::StandardButton result =
            QMessageBox::warning(parent, winTitle, msg,
                                 QMessageBox::Ok | QMessageBox::Cancel,
                                 QMessageBox::Cancel);

    if (result != QMessageBox::Ok)
        mainWindow.statusMessage(UiType::Warning, QObject::tr("Canceled"));

    return result;
}

bool IsLightTheme()
{
    return QApplication::palette().window().color().lightness() > 127;
}

QString LocalThemeIcon(const QString& relPath)
{
    return QString(":icons/hicolor/") + relPath;
}

} // namespace Util
