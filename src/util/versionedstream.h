/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VERSIONEDSTREAM_H
#define VERSIONEDSTREAM_H

#include <QDataStream>
#include <QString>

class QFile;

// While QDataStream has a version, it's for the QDatastream::Version enum.
// We want our own, for load-time conversions of prior formats.
class VersionedStream : public QDataStream
{
public:
    using QDataStream::QDataStream;

    uint readVersion() { *this >> m_version; return m_version; }
    uint getVersion()  const { return m_version; }
    VersionedStream& writeVersion(uint v) { *this << (m_version = v); return *this; }

    uint readMagic() { uint magic; *this >> magic; return magic; }
    VersionedStream& writeMagic(uint magic) { *this << magic; return *this; }

    bool openWrite(QFile& file, unsigned int magic, unsigned int version);
    bool openRead(QFile& file, unsigned int magic, unsigned int minVersion, unsigned int maxVersion);

    enum Error {
        NoError,
        BadMagic,      // bad magic number
        BadVersion,    // bad version
        FileOpenError, // file open failed
        FileError,     // file.error() not NoError
        StreamError,   // stream.status() not Ok
    };

    Error error();
    const QString& errorString();

private:
    uint    m_version;
    Error   m_error;
    QString m_errorString;
};

#endif // VERSIONEDSTREAM_H
