/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PACK_H
#define PACK_H

// Bitfield packing helpers.  For stored formats, we cannot depend on the
// compiler's bitfield packing, which can differ between big/little endian,
// and between compilers.

template <int start, int bits, typename T0>
T0 unpack(T0 src) {
    return T0((src >> start) & ((1U << bits) - 1));
}

template <int start, int bits, typename T0, typename T1>
void pack(T0& dst, T1 val) {
    dst = (dst & ~(((1U << bits) - 1) << start)) | (T0(val) << start);
}

#endif // PACK_H
