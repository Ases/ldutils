/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONS_H
#define ICONS_H

#include <QIcon>

class QAction;
class QMenu;
class QTabWidget;
class QAbstractButton;

class Icons
{
public:
    // Encapsulate icon selection strategy
    static QIcon get(const char* name);
    static void  defaultIcon(QAction*, const char* iconName);
    static void  defaultIcon(QMenu*, const char* iconName);
    static void  defaultIcon(QTabWidget*, int index, const char* iconName);
    static void  defaultIcon(QAbstractButton*, const char* iconName);

private:
    // static
};

#endif // ICONS_H
