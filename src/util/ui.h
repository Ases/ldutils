/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UI_H
#define UI_H

class QTabWidget;
class QString;
class QWidget;
class MainWindowBase;

namespace Util {
void NextTab(QTabWidget* tabs, int reserve = 0);
void PrevTab(QTabWidget* tabs, int reserve = 0);

// Helper to display warning dialog and also set status text.
int WarningDialog(MainWindowBase&, const QString& winTitle, const QString& msg, QWidget* parent = nullptr);

// Returns true if the underlying theme is light.
bool IsLightTheme();

// Use light or dark them icon, given path under the icon directory.
QString LocalThemeIcon(const QString& relPath);
}

#endif // UI_H
