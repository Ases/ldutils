/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <algorithm>

#include <QtGlobal>
#include <QApplication>
#include <QDesktopWidget>
#include <QString>
#include <QIcon>
#include <QList>
#include <QFileInfo>
#include <QTreeView>
#include <QHeaderView>
#include <QAbstractProxyModel>
#include <QSortFilterProxyModel>
#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QRegExp>
#include <QToolButton>
#include <QAction>

#include "reverseadapter.h"
#include "util.h"

namespace {

// Factor out common UID/GID code.  Technically this template can't be seen
// outside the file scope, but we'll put it in an anonymous namespace for
// general style reasons.
    template <typename T, typename ID> T GetPwId(const QByteArray& id_s, const ID& idFromPwFn)
    {
        if (id_s.size() == 0) // we ignore empty strings: not an error
            return T(-1);

        if (id_s[0] == '#' && id_s.size() >= 2 && isdigit(id_s[1])) {
            // #numeric style: get UID from part of string after '#'
            errno = 0;

            const T id = T(strtoul(id_s.data()+1, nullptr, 10));
            return (errno == 0) ? id : T(-1);
        } else {
            // name style: look it up
            passwd* pw = getpwnam(id_s.data());
            return (pw != nullptr) ? idFromPwFn(getpwnam(id_s.data())) : T(-1);
        }

        return T(-1);
    }
}

namespace Util {

QByteArray ExpandDirName(const QString &in)
{
    return ExpandDirName(in.toUtf8());
}

QByteArray ExpandDirName(const char* in)
{
    return ExpandDirName(QByteArray(in));
}

QByteArray ExpandDirName(const QByteArray &in)
{
    QByteArray out = "";
    int pos = 0;

    // PITA: replace ~ or ~user with user's home directory
    if (in.startsWith("~")) {
        const int slashPos = in.indexOf('/');
        if (slashPos > 0) {
            // we found a leading ~ and a slash.  Extract middle part as a name:
            const QByteArray name = in.mid(1, slashPos-1);
            QByteArray homedir;

            if (name.isEmpty()) {
                homedir = qgetenv("HOME");
            } else {
                struct passwd *pw = getpwnam(name); // TODO: use getpwnam_r
                if (pw != nullptr)
                    homedir = pw->pw_dir;
            }

            // We found one: replace it in the QString
            if (!homedir.isNull())
                out += homedir;

            pos = slashPos;
        }
    }

    // Expand environment variables, given as either ${VAR} or $VAR
    int varPos;
    while ((varPos = in.indexOf('$', pos)) >= 0) {
        if (varPos >= (in.length()-2))  // mustn't be at end of string
            break;

        out += in.mid(pos, varPos-pos);
        ++varPos; // skip '$'

        // find variable end. May be ${foo} or $foo/...
        int varEnd;
        if (in[varPos] == '{') {
            ++varPos; // skip '{'
            varEnd = in.indexOf('}', varPos+2);
            if (varEnd < 0) // unterminated
                break;
        } else {
            for (varEnd = varPos+1; varEnd < in.length(); ++varEnd) {
                if (!isalpha(in[varEnd]) && !isdigit(in[varEnd]) && !(in[varEnd] == '_'))
                    break;
            }
        }

        const QByteArray var = in.mid(varPos, varEnd-varPos);

        if (var.length() > 0) {
            const char* envValue = getenv(var.data());
            if (envValue != nullptr)
                out += envValue;
        }

        pos = varEnd;

        if (in[pos] == '}')  // skip any trailing '}'
            ++pos;
    }

    // add rest of input string
    out += in.mid(pos);

    return out;
}

QIcon ReadIcon(const QString &prefix, const QString& suffix, const QVector<QSize> &sizes)
{
    QIcon icon;

    for (const auto& size : sizes) {
        const QString sizeName = prefix + QString::number(size.width()) + 'x'
                + QString::number(size.height()) + suffix;

        if (QFileInfo(sizeName).exists())
            icon.addFile(sizeName);
    }

    return icon;
}

uid_t GetUid(const QByteArray &uid_s)
{
   return GetPwId<uid_t>(uid_s, [](const passwd* pw) { return pw->pw_uid; });
}

uid_t GetGid(const QByteArray &gid_s)
{
   return GetPwId<gid_t>(gid_s, [](const passwd* pw) { return pw->pw_gid; });
}

void ResizeViewForData(QTreeView &view, bool ignoreHeaders, int minColumnWidth, int pad)
{
    if (view.model() == nullptr)
        return;

    const bool headersHidden = view.isHeaderHidden();

    if (ignoreHeaders)
        view.setHeaderHidden(true);

    for (int column = 0; column < view.model()->columnCount(); ++column) {
        view.resizeColumnToContents(column);

        if (pad > 0)
            view.setColumnWidth(column, view.columnWidth(column) + pad);

        if (view.columnWidth(column) < minColumnWidth)
            view.setColumnWidth(column, minColumnWidth);
    }

    if (ignoreHeaders)
        view.setHeaderHidden(headersHidden);
}

void ResizeColumns(QTreeView& view, const QVector<uint>& ratios)
{
    const uint total = std::accumulate(ratios.begin(), ratios.end(), 0U);
    const uint width = uint(view.viewport()->width());

    if (view.model() == nullptr || total == 0)
        return;

    for (int column = 0; column < view.model()->columnCount(); ++column)
        view.setColumnWidth(column, (column < ratios.size() ? ratios.at(column) : 0) * width / total);
}

QModelIndex clickPosIndex(const QTreeView* treeView, const QHeaderView& headerView, const QPoint& pos)
{
    if (treeView == nullptr)
        return QModelIndex();

    const QPoint localPos = treeView->mapFromParent(QPoint(pos.x(), pos.y() - headerView.height()));

    return treeView->indexAt(localPos);
}

QModelIndex MapDown(const QModelIndex &idx)
{
    const QAbstractItemModel* model = idx.model();
    QModelIndex mapped = idx;

    while (const auto proxy = dynamic_cast<const QAbstractProxyModel*>(model)) {
        mapped = proxy->mapToSource(mapped);
        model  = proxy->sourceModel();
    }

    return mapped;
}

QModelIndexList& MapDown(QModelIndexList &idxList)
{
    for (auto& m : idxList)
        m = MapDown(m);

    return idxList;
}

QModelIndexList MapDown(const QModelIndexList &idx)
{
    QModelIndexList mapped = idx;
    return Util::MapDown(mapped);
}

const QAbstractItemModel* MapDown(const QAbstractItemModel* model)
{
    while (const auto proxy = dynamic_cast<const QAbstractProxyModel*>(model))
        model = proxy->sourceModel();

    return model;
}

QAbstractItemModel* MapDown(QAbstractItemModel* model)
{
    while (auto proxy = dynamic_cast<QAbstractProxyModel*>(model))
        model = proxy->sourceModel();

    return model;
}

QItemSelectionModel& MapDown(QItemSelectionModel& dst,
                             const QItemSelectionModel& src,
                             QItemSelectionModel::SelectionFlags flags)
{
    // First map all the indexes
    for (auto idx : MapDown(src.selection().indexes()))
        dst.select(idx, flags);

    // Now update the current item
    dst.select(MapDown(src.currentIndex()), QItemSelectionModel::Current);

    return dst;
}

QModelIndex MapUp(const QAbstractItemModel* model, const QModelIndex &idx)
{
    QModelIndex mapped = idx;

    // This must be mapped from bottom to top, which is inconvenient.
    QList<const QAbstractProxyModel*> proxies;
    proxies.reserve(4);

    while (const auto proxy = dynamic_cast<const QAbstractProxyModel*>(model)) {
        if (proxy == idx.model())
            break;
        proxies.push_front(proxy);
        model = proxy->sourceModel();
    }

    for (const auto& p : proxies)
        mapped = p->mapFromSource(mapped);

    return mapped;
}

QModelIndexList& MapUp(const QAbstractItemModel* model, QModelIndexList &idx)
{
    for (auto& m : idx)
        m = MapUp(model, m);

    return idx;
}

QModelIndexList MapUp(const QAbstractItemModel* model, const QModelIndexList& idx)
{
    QModelIndexList list = idx;
    return MapUp(model, list);
}

bool RowMatches(const QAbstractItemModel& model, const QModelIndex& parent,
                int row, const QRegExp &regex, int keyColumn, int filterRole)
{
    if (regex.isEmpty())
        return true;

    const int captures = regex.captureCount();
    const bool all     = keyColumn < 0;

    const int beginColumn = all ? 0 : keyColumn;
    const int endColumn   = all ? model.columnCount() : (beginColumn + 1);

    bool matched = false;
    int  matches = 0; // count column matches

    for (int column = beginColumn; column < endColumn && !matched; ++column) {
        const QModelIndex dataIndex = model.index(row, column, parent);

        if (regex.indexIn(model.data(dataIndex, filterRole).toString()) >= 0) {
            if (!all || captures == 0) {
                matched = true;  // If only looking for one field, or there are no captures, we found it.
            } else {
                if (matches++ >= captures)  // we found one.  Look for the next cap next time.
                    matched = true;
            }
        }
    }

    return matched;
}

bool RowMatches(const QAbstractItemModel& model, const QModelIndex& item,
                      const QRegExp &regex, int keyColumn, int filterRole)
{
    return RowMatches(model, model.parent(item), item.row(), regex, keyColumn, filterRole);
}

// Convenience: recurse through an QAbstractItemModel, calling function.
void recurse(const QAbstractItemModel& model, const std::function<void(const QModelIndex&)>& fn,
             const QModelIndex& parent, bool root)
{
    if (parent.isValid() || root)
        fn(parent);

    for (int row = 0; row < model.rowCount(parent); ++row)
        recurse(model, fn, model.index(row, 0, parent), false);
}

// Create savable reference to given index
SavableIndex SaveIndex(QModelIndex idx)
{
    SavableIndex path;
    path.reserve(8);

    if (!idx.isValid()) // invalid index, store as empty list
        return path;

    // For backward compatibility with prior usage, we store the column as a negative, and if it's not
    // present on load, assume 0.
    path.append(-idx.column() - 1);  // -1 so column 0 becomes negative

    for (; idx.isValid(); idx = idx.parent())
        path.append(idx.row());

    return path;
}

// Create index from savable reference
QModelIndex RestoreIndex(QAbstractItemModel& model, const Util::SavableIndex& path, int column)
{
    QModelIndex idx;

    // Restore invalid index
    if (path.isEmpty())
        return QModelIndex();

    for (auto pos : reverse_adapter(path)) {
        // If negative, that's the column.
        if (pos < 0) {
            column = -pos - 1; // -1 to avoid zero ambiguity (so we can tell col 0 from row 0)
            continue;
        }

        if (pos < model.rowCount(idx)) // must be in range
            idx = model.index(pos, 0, idx);
        else
            return QModelIndex();
    }

    return model.sibling(idx.row(), column, idx);
}

// Convenience: remove rows from a model matching predicate.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void removeRows(QAbstractItemModel& model,
                const std::function<bool(const QModelIndex&)>& predicate, const QModelIndex& parent)
{
    // List of index, vector-of-ranges pairs
    QList<QPair<QModelIndex, QList<QPair<int, int>>>> toRemove;

    // Build up removal list
    const std::function<void(const QModelIndex&)> sweep = [&](const QModelIndex& parent) {
        for (int row = 0; row < model.rowCount(parent); ++row) {
            const QModelIndex pos = model.index(row, 0, parent);

            if (predicate(pos)) {
                if (toRemove.isEmpty() || toRemove.front().first != parent)
                    toRemove.push_front(qMakePair(parent, decltype(toRemove)::value_type::second_type()));

                auto& rangeVec = toRemove.front().second;
                if (rangeVec.isEmpty() || (rangeVec.front().first + rangeVec.front().second != row))
                    rangeVec.prepend(qMakePair(row, 1));
                else
                    ++rangeVec.front().second;
            }

            sweep(pos);
        }
    };

    sweep(parent);

    // Remove batches.
    for (const auto& parentRanges : toRemove)
        for (const auto& range : parentRanges.second)
            model.removeRows(range.first, range.second, parentRanges.first);
}

// Convenience: remove rows from a model matching selection.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void removeRows(QAbstractItemModel& model,
                const QItemSelectionModel* selection, const QSortFilterProxyModel* filter,
                const QModelIndex& parent)
{
    if (selection != nullptr) {
        Util::removeRows(model, [selection, filter](const QModelIndex& idx) {
            const QModelIndex filterIdx = Util::MapUp(filter, idx);
            return filterIdx.model() != nullptr &&
                   selection->isRowSelected(filterIdx.row(), filterIdx.parent());
        }, parent);
    }
}

// Open the given TreeView nodes to display items matching given predicate.
bool OpenToMatch(const QAbstractItemModel& model, QTreeView& view,
                 const std::function<bool(const QModelIndex&)>& fn,
                 const QModelIndex& parent)
{
    if (fn(parent)) {
        view.setExpanded(parent, true);
        view.setCurrentIndex(parent);
        return true;
    }

    bool rc = false;

    for (int row = 0; row < model.rowCount(parent) && !rc; ++row)
        rc = rc || OpenToMatch(model, view, fn, model.index(row, 0, parent));

    if (rc)
        view.setExpanded(parent, true);

    return rc;
}

// Map a popup position to glocal coordinates, but keeping it on the screen containing the mouse cursor.
QRect MapOnScreen(QWidget *widget, const QPoint& pos, const QSize& size)
{
    const QRect newGeo = QRect(widget->parentWidget()->mapToGlobal(pos) -
                               QPoint(size.width() / 2, size.height() / 2),
                               size);
    // Make sure it stays on the screen containing the widget, so we don't span screens.
    // TODO: availableGeometry doesn't seem to actually exclude task bars in KDE, so we don't use it
    // here.  It should, per the docs, but doesn't as of Qt 5.9 on KDE.  Instead, we hack it with
    // a 30 pixel margin on all sides.  Certainly wrong sometimes.
    const QRect screenGeo    = QApplication::desktop()->screenGeometry(widget);
    const QRect availableGeo = screenGeo.adjusted(30, 30, -30, -30);

    const int moveX = std::min(availableGeo.right() - newGeo.right(), 0) +
                      std::max(availableGeo.left()  - newGeo.left(), 0);

    const int moveY = std::min(availableGeo.bottom() - newGeo.bottom(), 0) +
                      std::max(availableGeo.top()    - newGeo.top(), 0);

    return newGeo.translated(moveX, moveY);
}

QColor GetTBColor(const QToolButton* tb)
{
    // Get color from tool button.  Eep!  Probably glacially slow, but that's ok here.
    return tb->icon().pixmap(16).toImage().pixelColor(1, 1);
}

void SetTBColor(QToolButton* tb, const QColor& color)
{
    if (!color.isValid())
        return;

    const int size = std::max(tb->size().width(), 20);

    QPixmap px(QSize(size, size));
    px.fill(color);
    tb->setIcon(px);
}

void setupActionTooltips(QWidget* root)
{
    if (root != nullptr)
        for (QWidget* w : root->findChildren<QWidget*>())
            for (QAction* action : w->actions())
                action->setWhatsThis(action->toolTip());
}

void setupActionTooltips(QWidget& root)
{
    setupActionTooltips(&root);
}

} // namespace Util

namespace SIM {

QVariant data(const QStandardItemModel &m, int row, int col) {
    return m.data(m.index(row, col));
}

bool isSet(const QStandardItemModel &m, int row, int col) {
    return m.item(row, col)->checkState() == Qt::Checked;
}

QColor bg(const QStandardItemModel &m, int row, int col) {
    return m.item(row, col)->background().color();
}

QIcon icon(const QStandardItemModel &m, int row, int col)
{
    return m.item(row, col)->icon();
}

} // namespace SIM
