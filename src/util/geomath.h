/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOMATH_H
#define GEOMATH_H

#include <cmath>
#include <type_traits>

#include "math.h"

namespace GeoMath {
    static const constexpr double earthRadiusM = 6371008.8; // radius of earth around 39N

    // great circle distance using haversine formula
    template <class T>
    T greatCircleDist(T lhsLatRad, T lhsLonRad, T rhsLatRad, T rhsLonRad)
    {
        const T latDiff = rhsLatRad - lhsLatRad;
        const T lonDiff = rhsLonRad - lhsLonRad;

        const T a = Math::sqr(sin(latDiff*T(0.5))) + cos(lhsLatRad) * cos(rhsLatRad) * Math::sqr(sin(lonDiff*T(0.5)));
        const T c = T(2.0) * atan2(sqrt(a), sqrt(T(1.0)-a));

        return c * T(earthRadiusM);
    }
}

#endif // GEOMATH_H
