/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef REVERSEADAPTER_H
#define REVERSEADAPTER_H

namespace Util
{

// Adapter to allow range based for loops to iterate backwards in containers with a reverse_iterator.
template <typename T> class reverse_adapter {
public:
    reverse_adapter(T& container) :  m_container(&container) { }
    reverse_adapter(const T& container) :  m_const_container(&container) { }

    typename T::reverse_iterator begin() { return m_container->rbegin(); }
    typename T::reverse_iterator end() { return m_container->rend(); }

    typename T::const_reverse_iterator cbegin() { return m_const_container->crbegin(); }
    typename T::const_reverse_iterator cend() { return m_const_container->crend(); }

private:
    union {
        T* m_container;
        const T* m_const_container;
    };
};

} // namespace Util


#endif // REVERSEADAPTER_H
