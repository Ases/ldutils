/*
    Copyright 2018 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PIPE_H
#define PIPE_H

#include <fcntl.h>
#include <unistd.h>
#include <cassert>

class Pipe
{
public:
    Pipe(int flags = 0) {
        if (pipe2(fd, flags) != 0)
            fd[0] = fd[1] = -1;
    }

    bool ok() const { return fd[0] != -1 && fd[1] != -1; }

    ~Pipe() { close(fd[0]); close(fd[1]); }

    int operator[](int x) const { assert(x >= 0 && x < 2); return fd[x]; }

protected:
    int fd[2];
};

#endif // PIPE_H
