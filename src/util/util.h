/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UTIL_H
#define UTIL_H

#include <cassert>
#include <cstdarg>
#include <array>
#include <climits>
#include <sys/types.h>
#include <pwd.h>
#include <type_traits>

#include <QVariant>
#include <QColor>
#include <QByteArray>
#include <QFile>
#include <QVector>
#include <QSize>
#include <QModelIndexList>
#include <QRect>
#include <QPoint>
#include <QItemSelectionModel>

#include "src/util/roles.h"

class QIcon;
class QTreeView;
class QRegExp;
class QStandardItemModel;
class QAbstractProxyModel;
class QItemSelectionModel;
class QSortFilterProxyModel;
class QToolButton;
class QHeaderView;

namespace Util {

// Expand ~ and $VAR or ${VAR} in pathnames
QByteArray ExpandDirName(const char* in);
QByteArray ExpandDirName(const QByteArray& in);
QByteArray ExpandDirName(const QString &in);

// Icon helper
QIcon ReadIcon(const QString& prefix,
               const QString& suffix = ".png",
               const QVector<QSize>& sizes = {
                  { 16, 16 }, { 32, 32 }, { 64, 64 }, { 128, 128 }
               });

// For deleting C-api style points to arrays of allocated data
template <class T>
void FreeCArray(T*** data, int count)
{
    for (int i = 0; i < count; ++i)
        free((*data)[i]);

    free(*data);
    *data = nullptr;
}

uid_t GetUid(const QByteArray& uid_s);
uid_t GetGid(const QByteArray& gid_s);

template <typename R, typename... T>
R PathCat(const T&... path)
{
    const std::array<R, sizeof...(T)> paths = { path... };

    R out;
    for (const auto& p : paths)
        out += p + "/";

    out.truncate(out.size()-1);

    return out;
}

template <typename T> T& RemoveNewline(T& line)
{
    if (line.size() > 0 && line[line.size()-1] == '\n')
        line.resize(line.size()-1);
    return line;
}

template <typename R>
inline R ReadFile(QFile& file, int maxLen)
{
    R contents = file.read(maxLen);
    return RemoveNewline(contents);
}

template <>
inline quint64 ReadFile<quint64>(QFile& file, [[maybe_unused]] int size)
{
    char buf[64];

    assert(unsigned(size) <= sizeof(buf));

    if (file.read(buf, sizeof(buf)) < 0)
        return 0;

    return strtoull(buf, nullptr, 10);
}

template <>
inline qint64 ReadFile<qint64>(QFile& file, [[maybe_unused]] int size)
{
    char buf[64];

    assert(unsigned(size) <= sizeof(buf));

    if (file.read(buf, sizeof(buf)) < 0)
        return 0;

    return strtoll(buf, nullptr, 10);
}

template <>
inline QList<QByteArray> ReadFile<QList<QByteArray>>(QFile& file, int maxLen)
{
    return file.read(maxLen).split('\n');
}

template <typename R>
inline R ReadFile(const QString& path, int maxLen = 1024)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return R();

    return ReadFile<R>(file, maxLen);
}

template <typename R, typename... T>
inline R ReadFilev(int maxLen, const char* fmt, const T&... data)
{
    char buf[PATH_MAX];

    const int rc = snprintf(buf, sizeof(buf), fmt, data...);
    if (rc < 0 || size_t(rc) >= sizeof(buf))
        return R();

    return ReadFile<R>(buf, maxLen);
}

// To facilitate incrementing over enums
template <typename T>
inline T inc(T& x) { return x = T(typename std::underlying_type<T>::type(x) + 1); }

// Resize view to fit all displayed data
void ResizeViewForData(QTreeView& view, bool ignoreHeaders = false,
                       int minColumnWidth = 20, int pad = 0);

void ResizeColumns(QTreeView& view, const QVector<uint>& ratios);

QModelIndex clickPosIndex(const QTreeView*, const QHeaderView&, const QPoint& pos);

template <typename T>
T Clamp(const T& val, const T& min, const T& max)  { return std::max(std::min(val, max), min); }

// Map an index all the way down a filter chain
QModelIndex MapDown(const QModelIndex&);
QModelIndexList& MapDown(QModelIndexList&);
QModelIndexList MapDown(const QModelIndexList&);
const QAbstractItemModel* MapDown(const QAbstractItemModel*);
QAbstractItemModel* MapDown(QAbstractItemModel*);
QItemSelectionModel& MapDown(QItemSelectionModel& dst,
                             const QItemSelectionModel& src,
                             QItemSelectionModel::SelectionFlags = QItemSelectionModel::Select);

// Map an index all the way up a filter chain
QModelIndex MapUp(const QAbstractItemModel*, const QModelIndex&);
QModelIndexList& MapUp(const QAbstractItemModel*, QModelIndexList&);
QModelIndexList MapUp(const QAbstractItemModel*, const QModelIndexList&);

// Lerp
template <typename T>
inline T Lerp(const T& v0, const T& v1, float factor) {
    return v0 + (v1 - v0) * T(factor);
}

template <>
inline QColor Lerp(const QColor& c0, const QColor& c1, float factor) 
{
    qreal h0, s0, v0, a0, h1, s1, v1, a1;

    c0.getHsvF(&h0, &s0, &v0, &a0);
    c1.getHsvF(&h1, &s1, &v1, &a1);

    return QColor::fromHsvF(Lerp(h0, h1, factor),
                            Lerp(s0, s1, factor),
                            Lerp(v0, v1, factor),
                            Lerp(a0, a1, factor));
}
    
// Regex match against one, or all columns
bool RowMatches(const QAbstractItemModel& model,
                const QModelIndex& item,
                const QRegExp& regex,
                int keyColumn,   // -1 to match all columns
                int filterRole = Util::CopyRole);

bool RowMatches(const QAbstractItemModel& model,
                const QModelIndex& parent, int row,
                const QRegExp& regex,
                int keyColumn,   // -1 to match all columns
                int filterRole = Util::CopyRole);  // -1 to match all

// Convenience: recurse through an QAbstractItemModel, calling function.
void recurse(const QAbstractItemModel& model, const std::function<void(const QModelIndex&)>& fn,
             const QModelIndex& parent = QModelIndex(), bool root = false);

// Create savable reference to given row (doesn't restore column)
typedef QVector<int> SavableIndex;

SavableIndex SaveIndex(QModelIndex);

// Create index from savable reference
QModelIndex RestoreIndex(QAbstractItemModel& model, const QVector<int>&, int column = 0);

// Convenience: remove rows from a model matching predicate.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void removeRows(QAbstractItemModel& model,
                const std::function<bool(const QModelIndex&)>& predicate, const QModelIndex& parent = QModelIndex());

// Convenience: remove rows from a model matching selection.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void removeRows(QAbstractItemModel& model,
                const QItemSelectionModel* selection, const QSortFilterProxyModel* filter = nullptr,
                const QModelIndex& parent = QModelIndex());

// Open the given TreeView nodes to display items matching given predicate.
bool OpenToMatch(const QAbstractItemModel& model, QTreeView&, const std::function<bool(const QModelIndex&)>& fn,
                 const QModelIndex& parent = QModelIndex());

// Map a popup position to glocal coordinates, but keeping it on the screen containing the mouse cursor.
QRect MapOnScreen(QWidget* widget, const QPoint& geo, const QSize& size);

// Functions to help QToolButtons act like color selector buttons.
QColor GetTBColor(const QToolButton*);
void SetTBColor(QToolButton*, const QColor&);

// Set WhatsThis to match ToolTips, recursively for widget
void setupActionTooltips(QWidget*);
void setupActionTooltips(QWidget& root);

} // namespace Util

// Convenience functions to pull things out of standard item models
namespace SIM {
QVariant data(const QStandardItemModel& m, int row, int col);
bool     isSet(const QStandardItemModel& m, int row, int col);
QColor   bg(const QStandardItemModel& m, int row, int col);
QIcon    icon(const QStandardItemModel& m, int row, int col);
} // end namespace SIM

#endif // UTIL_H
