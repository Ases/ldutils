/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNITS_H
#define UNITS_H

#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <tuple>
#include <QVariant>
#include <QString>
#include <QStringRef>
#include <QHash>
#include <QMap>
#include <QMutex>
#include <QTimeZone>

#include <src/core/settings.h>
#include <src/util/util.h>
#include <src/util/math.h>

class QComboBox;

// Individual unit ranges must begin on multiples of 0x1000 (for range detection)
enum class Format {
    // date and time
    AutoTime,

    // Duration
    _DurBegin = 0x1000,
    DurS = _DurBegin,     // seconds from nSec
    DurM,                 // mm.mm minutes
    DurH,                 // hh.hhh hours
    DurD,                 // dd.ddd days
    DurMS,                // mmmm:ss
    DurHMS,               // hhhh:mm:ss
    DurDHMS,              // dddd:hh:mm:ss
    _DurEnd,

    _PctBegin = 0x2000,
    Percent = _PctBegin,   // percent
    PctFloat,              // generic float
    _PctEnd,

    _SizeBegin = 0x3000,
    AutoBinary = _SizeBegin, // dynamic selection from binary size prefixes
    AutoDecimal,           // dynamic selection from decimal size prefixes
    Bytes,                 // plain old bytes
    KiB,                   // bytes as KiB
    MiB,                   // bytes as MiB
    GiB,                   // bytes as GiB
    TiB,                   // bytes as TiB
    PiB,                   // bytes as PiB
    EiB,                   // bytes as EiB
    KB,                    // kilobytes
    MB,                    // megabytes
    GB,                    // gigabytes
    TB,                    // terabytes
    PB,                    // petabytes
    EB,                    // exabytes
    _SizeEnd,

    _DistBegin = 0x4000,
    AutoDistMetric = _DistBegin,
    AutoDistImperial,
    _DistNonAuto,
    Distmm = _DistNonAuto,
    Distm,
    Distkm,
    DistAU,
    Distft,
    Distmi,
    DistSmoot,
    _DistEnd,

    _SpeedBegin = 0x5000,
    SpeedMPS = _SpeedBegin,
    SpeedKPH,
    SpeedMPH,
    SpeedFPS,
    SpeedMPKm,
    SpeedMPMi,
    _SpeedEnd,

    _AreaBegin = 0x6000,
    Aream2  = _AreaBegin,
    Areakm2,
    Areaft2,
    Areami2,
    _AreaEnd,

    _TempBegin = 0x7000,
    TempC = _TempBegin,
    TempK,
    TempF,
    _TempEnd,

    _SlopeBegin = 0x8000,
    SlopePct = _SlopeBegin,   // slope, percent
    Degrees,                  // slope, degrees
    Radians,                  // slope, percent
    RiseRun,                  // slope, rise over run
    _SlopeEnd,

    _GeoPosBegin = 0x9000,
    GeoPosDMS = _GeoPosBegin, // degrees / minutes / sec
    GeoPosDeg,                // decimal degrees
    GeoPosRad,                // radians
    _GeoPosEnd,

    _PowerBegin = 0xa000,
    Watt = _PowerBegin,       // watts
    kW,                       // kilowatts
    hp,                       // horsepower
    kcalperhr,                // kcal/hr
    _PowerEnd,

    // Datestamps, including time
    _DateBegin = 0xb000,
    DateISO = _DateBegin,
    DateLocaleShort,
    DateLocaleLong,
    DateyyyyMMMdd_hhmmss,
    DateyyyyMMdd_hhmmss,
    DateyyMMMdd_hhmmss,
    DateyyMMdd_hhmmss,
    DateddMMMyyyy_hhmmss,
    DateddMMyyyy_hhmmss,
    DateddMMMyy_hhmmss,
    DateddMMyy_hhmmss,
    DateyyyyMMMdd,
    DateyyyyMMdd,
    DateyyMMMdd,
    DateyyMMdd,
    DateddMMMyyyy,
    DateddMMyyyy,
    DateddMMMyy,
    DateddMMyy,
    Datehhmmss,
    Datehhmm,
    Datehh_mm_ss,
    Datehh_mm,
    _DateEnd,

    _WeightBegin = 0xc000,
    g   = _WeightBegin,
    kg,
    oz,
    lb,
    stone,
    ton,
    _WeightEnd,

    _RawNumBegin = 0xd000,
    Float = _RawNumBegin,  // raw float value
    Int,                   // raw int value
    _RawNumEnd,

    _StringBegin = 0xe000,
    String = _StringBegin, // raw string
    _StringEnd,

    _EnergyBegin = 0xf000,
    Wh = _EnergyBegin,
    kWh,
    cal,      // physics calorie
    kcal,     // dietary calorie (1000 calories)
    J,        // joules
    kJ,       // kilojoules
    MJ,       // megajoules
    _EnergyEnd,

    _AccelBegin = 0x10000,
    AccelMPS2 = _AccelBegin,
    AccelFPS2,
    _AccelEnd,

    // Time without dates
    _TimeBegin = 0x11000,
    Timehhmmss = _TimeBegin,
    Timehhmm,
    Timehh_mm_ss,
    Timehh_mm,
    _TimeEnd,

    _BeatsBegin = 0x12000,
    BeatsPerS = _BeatsBegin, // /sec
    BeatsPerM,               // /min
    BeatsPerH,               // /hr
    BeatsPerD,               // /day
    _BeatsEnd,

    _RevsBegin = 0x13000,
    RevsPerS = _RevsBegin,   // /sec
    RevsPerM,                // /min
    RevsPerH,                // /hr
    RevsPerD,                // /day
    _RevsEnd,

    _TzBegin = 0x120000,
    TzShort = _TzBegin,      // abbreviated TZ
    TzOffset,                // offset, like UTC-5:00
    TzLong,                  // long name
    TzIANA,                  // IANA TZ ID
    _TzEnd,

    _Invalid = 0x7fffffff,
};

// Sanity check range starts
static_assert((uint(Format::_DurBegin)    & 0xfff) == 0);
static_assert((uint(Format::_PctBegin)    & 0xfff) == 0);
static_assert((uint(Format::_SizeBegin)   & 0xfff) == 0);
static_assert((uint(Format::_DistBegin)   & 0xfff) == 0);
static_assert((uint(Format::_SpeedBegin)  & 0xfff) == 0);
static_assert((uint(Format::_AreaBegin)   & 0xfff) == 0);
static_assert((uint(Format::_TempBegin)   & 0xfff) == 0);
static_assert((uint(Format::_SlopeBegin)  & 0xfff) == 0);
static_assert((uint(Format::_GeoPosBegin) & 0xfff) == 0);
static_assert((uint(Format::_PowerBegin)  & 0xfff) == 0);
static_assert((uint(Format::_DateBegin)   & 0xfff) == 0);
static_assert((uint(Format::_WeightBegin) & 0xfff) == 0);
static_assert((uint(Format::_RawNumBegin) & 0xfff) == 0);
static_assert((uint(Format::_StringBegin) & 0xfff) == 0);
static_assert((uint(Format::_EnergyBegin) & 0xfff) == 0);
static_assert((uint(Format::_AccelBegin)  & 0xfff) == 0);
static_assert((uint(Format::_TimeBegin)   & 0xfff) == 0);
static_assert((uint(Format::_BeatsBegin)  & 0xfff) == 0);
static_assert((uint(Format::_RevsBegin)   & 0xfff) == 0);

// For use with sets/etc
inline uint qHash(Format item, uint seed = 0) {
    return qHash(int(item), seed);
}

inline double operator""_B  (long double x) { return double(x); }
inline double operator""_KiB(long double x) { return double(x) * 1024.0_B;   }
inline double operator""_MiB(long double x) { return double(x) * 1024.0_KiB; }
inline double operator""_GiB(long double x) { return double(x) * 1024.0_MiB; }
inline double operator""_TiB(long double x) { return double(x) * 1024.0_GiB; }
inline double operator""_PiB(long double x) { return double(x) * 1024.0_TiB; }
inline double operator""_EiB(long double x) { return double(x) * 1024.0_PiB; }
inline double operator""_KB(long double x)  { return double(x) * 1000.0_B;   }
inline double operator""_MB(long double x)  { return double(x) * 1000.0_KB; }
inline double operator""_GB(long double x)  { return double(x) * 1000.0_MB; }
inline double operator""_TB(long double x)  { return double(x) * 1000.0_GB; }
inline double operator""_PB(long double x)  { return double(x) * 1000.0_TB; }
inline double operator""_EB(long double x)  { return double(x) * 1000.0_PB; }

inline uint64_t operator""_DatemS (unsigned long long x) { return x * 1000000; }     // nS to mS
inline uint64_t operator""_DateS  (unsigned long long x) { return x * 1000_DatemS; } // nS to S
inline uint64_t operator""_DateM  (unsigned long long x) { return x * 60_DateS; }
inline uint64_t operator""_DateH  (unsigned long long x) { return x * 60_DateM; }
inline uint64_t operator""_DateD  (unsigned long long x) { return x * 24_DateH; }

inline double operator""_DatemS (long double x) { return double(x) * 1000000.0; }     // nS to mS
inline double operator""_DateS  (long double x) { return double(x) * 1000.0_DatemS; }  // nS to S
inline double operator""_DateM  (long double x) { return double(x) * 60.0_DateS; }
inline double operator""_DateH  (long double x) { return double(x) * 60.0_DateM; }
inline double operator""_DateD  (long double x) { return double(x) * 24.0_DateH; }

inline uint64_t operator""_B  (unsigned long long x) { return x; }
inline uint64_t operator""_KiB(unsigned long long x) { return x * 1024_B;   }
inline uint64_t operator""_MiB(unsigned long long x) { return x * 1024_KiB; }
inline uint64_t operator""_GiB(unsigned long long x) { return x * 1024_MiB; }
inline uint64_t operator""_TiB(unsigned long long x) { return x * 1024_GiB; }
inline uint64_t operator""_PiB(unsigned long long x) { return x * 1024_TiB; }
inline uint64_t operator""_EiB(unsigned long long x) { return x * 1024_PiB; }
inline uint64_t operator""_KB(unsigned long long x)  { return x * 1000_B;   }
inline uint64_t operator""_MB(unsigned long long x)  { return x * 1000_KiB; }
inline uint64_t operator""_GB(unsigned long long x)  { return x * 1000_MiB; }
inline uint64_t operator""_TB(unsigned long long x)  { return x * 1000_GiB; }
inline uint64_t operator""_PB(unsigned long long x)  { return x * 1000_TiB; }
inline uint64_t operator""_EB(unsigned long long x)  { return x * 1000_PiB; }

inline double operator""_Distmm(long double x)    { return double(x) * (1.0/1000.0); }
inline double operator""_Distm(long double x)     { return double(x); }
inline double operator""_Distkm(long double x)    { return double(x) * 1000.0_Distm; }
inline double operator""_DistAU(long double x)    { return double(x) * 149597870700.0_Distm; }
inline double operator""_Distft(long double x)    { return double(x) * 0.30480_Distm; }
inline double operator""_Distmi(long double x)    { return double(x) * 1609.344_Distm; }
inline double operator""_DistSmoot(long double x) { return double(x) * 1.7018_Distm; }

inline double operator""_SpeedMPS(long double x)  { return double(x); }
inline double operator""_SpeedKPH(long double x)  { return double(x) * 0.27777778_SpeedMPS; }
inline double operator""_SpeedMPH(long double x)  { return double(x) * 0.44704_SpeedMPS; }
inline double operator""_SpeedFPS(long double x)  { return double(x) * 0.68181818_SpeedMPS; }
inline double operator""_SpeedMPKm(long double x) { return 16.666667_SpeedMPS / double(x); }
inline double operator""_SpeedMPMi(long double x) { return 26.8224_SpeedMPS / double(x); }

inline double operator""_Watt(long double x)      { return double(x); }
inline double operator""_kW(long double x)        { return double(x) * 1000.0_Watt; }
inline double operator""_hp(long double x)        { return double(x) * 745.69987_Watt; }
inline double operator""_kcalperhr(long double x) { return double(x) * 0.86042065_Watt; }

inline double operator""_kg(long double x)    { return double(x); }
inline double operator""_g(long double x)     { return double(x) * 0.001_kg; }
inline double operator""_oz(long double x)    { return double(x) * 28.349523_g; }
inline double operator""_lb(long double x)    { return double(x) * 453.59237_g; }
inline double operator""_stone(long double x) { return double(x) * 14.0_lb; }
inline double operator""_ton(long double x)   { return double(x) * 2000.0_lb; }

inline double operator""_Wh(long double x)   { return double(x); }
inline double operator""_kWh(long double x)  { return double(x) * 1000.0_Wh; }
inline double operator""_J(long double x)    { return double(x) / 3600.0_Wh; }
inline double operator""_kJ(long double x)   { return double(x) * 1000.0_J; }
inline double operator""_MJ(long double x)   { return double(x) * 1000.0_kJ; }
inline double operator""_cal(long double x)  { return double(x) * 4.184_J; }
inline double operator""_Cal(long double x)  { return double(x) * 1000.0_cal; }
inline double operator""_kcal(long double x) { return double(x) * 1.0_Cal; }

inline double operator""_perS(long double x) { return double(x); }
inline double operator""_perM(long double x) { return double(x) * 60.0_perS; }
inline double operator""_perH(long double x) { return double(x) * 60.0_perM; }
inline double operator""_perD(long double x) { return double(x) * 24.0_perH; }

class Units : public Settings
{
private:
    typedef std::tuple<QVariant, Format, int> ParseCode;

public:
    Units(Format format, int precision = 2, bool leadingZeros = true,
          char neg = '\0', char pos = '\0') :
        m_format(format),
        m_precision(precision),
        m_leadingZeros(leadingZeros),
        m_UTC(false),
        m_neg(neg),
        m_pos(pos),
        m_range(rangeBegin(format)) // range we belong to
    {
        setupSuffixes();
        setupFormats();
    }

    Units(const Units& rhs);

    QString operator()(const QVariant& value, int forcePrecision = -1) const;
    QString operator()(const QTimeZone& value, const QDateTime& at) const;

    inline QVariant        to(const double value) const;
    inline QVariant        to(const QVariant& value) const;
    static QVariant        to(const QVariant& value, Format fmt, bool utc);
    inline static QVariant to(const double value, Format fmt);
    inline double          toDouble(const double value) const;
    inline static double   toDouble(const QVariant& value, Format fmt);
    static double          toDouble(const double value, Format fmt);
    inline QVariant        from(const QVariant& value) const;
    inline double          fromDouble(const QVariant& value) const;
    static QVariant        from(const QVariant& value, Format fmt, bool utc);
    inline QString         toString(const QVariant& value, Format fmt) const;
    inline QString         toString(const QVariant& value) const;

    Units&    setFormat(Format f);
    Units&    setFormat(const QString&);
    Units&    setPrecision(int p)      { m_precision = p;    return *this; }
    Units&    setLeadingZeros(bool b)  { m_leadingZeros = b; return *this; }
    Units&    setUTC(bool utc)         { m_UTC = utc; return *this; }
    bool      isUTC() const            { return m_UTC; }
    Format    format() const           { return m_format; }
    inline bool isValid() const        { return format() != Format::_Invalid; }
    int       precision() const        { return m_precision; }
    bool      leadingZeros() const     { return m_leadingZeros; }
    const QString& suffix(double value) const;
    double    multiplier(double value) const;
    Format    findSuffix(const QString&) const;
    Format    findSuffix(const QStringRef&) const;
    ParseCode parse(const QStringRef&) const;
    ParseCode parse(const QString&) const;
    bool      validInRange(Format f) const { return f >= rangeBegin() && f < rangeEnd(); }
    const QString& dateFormat() const;
    template <class T = QMap<QStringRef, Format>> T rangeSuffixes() const;

    Format autoUnit(const QVariant& value) { // automatic unit selection
        return autoUnit(value, m_format);
    }

    static const QString& suffix(Format);
    static const QVector<QString>& suffixes(Format);
    static double         multiplier(Format);
    static QString        name(Format);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    // Set by index relative to the units range
    int    rangeIdx() const { return rangeIdx(m_range); }
    int    rangeIdx(Format first) const { return int(m_format) - int(first); }
    void   setIdx(int idx) { setIdx(idx, m_range); }
    void   setIdx(int idx, Format first) { setFormat(Format(idx + int(first))); }

    void   addToComboBox(QComboBox*, Format first = Format::_Invalid) const;

    QString chartLabelFormat(double value = 100.0) const; // format for use in chart axes

    Units& operator=(const Units& rhs);

private:
    static const uint rangeMask = 0xfffff000;

    void setupSuffixes();
    void setupFormats();
    Units::ParseCode parseDuration(const QStringRef& text) const;
    Units::ParseCode parseString(const QStringRef& text) const;
    template <class T> Units::ParseCode parseDateStamp(const QStringRef& text, Format begin, Format end) const;
    Units::ParseCode parseValueSuffix(const QStringRef& text) const;
    static auto parseNumSuffix(const QStringRef& text);

    static Format rangeBegin(Format fmt) { return Format(uint(fmt) & rangeMask); }
    Format rangeBegin() const { return m_range; }
    Format rangeEnd() const;  // returns range end (one past last valid)

    QTimeZone getTZ() const { return m_UTC ? QTimeZone::utc() : QTimeZone::systemTimeZone(); }

    void updateFormat(Format format, int precision);

    Format     m_format;       // format to display
    int        m_precision;    // digits after decimal
    bool       m_leadingZeros; // true to display leading zeros
    bool       m_UTC;          // dates in UTC: else local.
    char       m_neg;          // for lat/lon format
    char       m_pos;          // for lat/lon format
    Format     m_range;        // range, set at construction.

    static QHash<Format, QVector<QString>> m_suffixes;
    static QHash<Format, QString> m_formats;
    static const ParseCode parseError;

    static Format          autoUnit(const QVariant& value, Format fmt);
    QString                convert(const QVariant& value, Format fmt, int precision) const;
    static const QString&  fmtStr(Format f);

    template <typename... T>
    static char* fmtBuffer(char* buf, int buflen, const char* fmt, const T&... param);

    static QMutex cacheLock;
};

inline double Units::toDouble(const double value, Format fmt)
{
    switch (fmt) {
    case Format::TempK:     return value + 273.15;
    case Format::TempF:     return value * 9.0/5.0 + 32.0;
    case Format::Degrees:   return Math::toDeg(std::atan(value));
    case Format::Radians:   return std::atan(value);
    case Format::GeoPosRad: return Math::toRad(value);
    case Format::SpeedMPKm: return 1.0_SpeedMPKm / value;
    case Format::SpeedMPMi: return 1.0_SpeedMPMi / value;
    default:                return value / multiplier(fmt);
    }
}

inline double Units::toDouble(const double value) const
{
    return toDouble(value, m_format);
}

inline double Units::toDouble(const QVariant& var, Format fmt)
{
    return toDouble(var.toDouble(), fmt);
}

inline QVariant Units::to(const double value, Format fmt)
{
    return toDouble(value, fmt);
}

inline QVariant Units::to(const QVariant& value) const
{
    return to(value, autoUnit(value, m_format), isUTC());
}

inline QVariant Units::to(const double value) const
{
    return to(value, autoUnit(value, m_format));
}

inline QVariant Units::from(const QVariant& var) const
{
    return from(var, autoUnit(var, m_format), isUTC());
}

inline double Units::fromDouble(const QVariant& value) const
{
    return from(value).toDouble();
}

inline QString Units::toString(const QVariant& value, Format fmt) const
{
    return convert(value, fmt, m_precision);
}

inline QString Units::toString(const QVariant& value) const
{
    return convert(value, m_format, m_precision);
}

#endif // UNITS_H
