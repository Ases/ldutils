/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MATH_H
#define MATH_H

#include <limits>
#include <cmath>
#include <type_traits>

namespace Math {
    // Weirdly, std:: doesn't provide pi!, before c++20, just this nonstandard define.
    template <typename T> inline T toRad(T v) { return v * M_PI / 180.0; }
    template <typename T> inline T toDeg(T v) { return v * 180.0 / M_PI; }

    template <typename T> inline T sqr(T x)   { return x*x; }
    template <typename T> inline T distSqr(T x, T y)  { return sqr(x) + sqr(y); }

    template <typename T> inline T mix(T a, T b, T x) { return a+(b-a)*x; }

    // This is lifted from the example at https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
    template<class T>
    typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
        almost_equal(T x, T y, int ulp)
    {
        // the machine epsilon has to be scaled to the magnitude of the values used
        // and multiplied by the desired precision in ULPs (units in the last place)
        return std::abs(x-y) <= std::numeric_limits<T>::epsilon() * std::abs(x+y) * ulp
            // unless the result is subnormal
            || std::abs(x-y) < std::numeric_limits<T>::min();
    }
}

#endif // MATH_H
