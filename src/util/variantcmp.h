/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VARIANTCMP_H
#define VARIANTCMP_H

// QVariant comparisons went away in Qt 5.15.  This is our own implementation thereof.
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))

class QVariant;

bool operator<(const QVariant&, const QVariant&);
bool operator>(const QVariant&, const QVariant&);
inline bool operator>=(const QVariant& lhs, const QVariant& rhs) { return !operator<(lhs, rhs); }
inline bool operator<=(const QVariant& lhs, const QVariant& rhs) { return !operator>(lhs, rhs); }

#endif // QT_VERSION

#endif // VARIANTCMP_H
