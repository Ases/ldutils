/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CMDLINE_H
#define CMDLINE_H

// Common cmd line argument processing used by multiple consumers of ldutils

namespace Util {

bool ProcessArgs(const char* appname, const char* version,
                 int argc, char *argv[], int arg);
bool ProcessArgsPostApp(char* arg);

} // namespace Util

#endif // CMDLINE_H
