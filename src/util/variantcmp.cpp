/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QtGlobal>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))

#include <cassert>
#include "variantcmp.h"

#include <QVariant>
#include <QBitArray>
#include <QBitmap>
#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QIcon>
#include <QLine>
#include <QLineF>
#include <QLocale>
#include <QModelIndex>
#include <QPixmap>
#include <QPoint>
#include <QPointF>
#include <QPersistentModelIndex>
#include <QRect>
#include <QRectF>
#include <QRegExp>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QStringList>
#include <QTime>
#include <QUrl>

namespace {
    template <typename T> inline uint lt(const QVariant& lhs, const QVariant& rhs) {
        return lhs.value<T>() < rhs.value<T>();
    }

template <typename T> inline uint gt(const QVariant& lhs, const QVariant& rhs) {
    return lhs.value<T>() > rhs.value<T>();
}
}

bool operator<(const QVariant& lhs, const QVariant& rhs)
{
    if (lhs.type() != rhs.type())
        return lhs.toString() < rhs.toString();

    switch (lhs.type())
    {
    case QVariant::BitArray:              return lt<QBitArray>(lhs, rhs);
    case QVariant::Bitmap:                return lt<QBitmap>(lhs, rhs);
    case QVariant::Bool:                  return lt<bool>(lhs, rhs);
    case QVariant::ByteArray:             return lt<QByteArray>(lhs, rhs);
    case QVariant::Char:                  return lt<QChar>(lhs, rhs);
    case QVariant::Date:                  return lt<QDate>(lhs, rhs);
    case QVariant::DateTime:              return lt<QDateTime>(lhs, rhs);
    case QVariant::Double:                return lt<double>(lhs, rhs);
    case QVariant::Icon:                  return lt<QIcon>(lhs, rhs);
    case QVariant::Int:                   return lt<int>(lhs, rhs);
    case QVariant::Invalid:               return false;
    case QVariant::Line:                  return lt<QLine>(lhs, rhs);
    case QVariant::LineF:                 return lt<QLineF>(lhs, rhs);
    case QVariant::Locale:                return lt<QLocale>(lhs, rhs);
    case QVariant::LongLong:              return lt<qlonglong>(lhs, rhs);
    case QVariant::PersistentModelIndex:  return lt<QPersistentModelIndex>(lhs, rhs);
    case QVariant::Point:                 return lt<QPoint>(lhs, rhs);
    case QVariant::Pixmap:                return lt<QPixmap>(lhs, rhs);
    case QVariant::PointF:                return lt<QPointF>(lhs, rhs);
    case QVariant::ModelIndex:            return lt<QModelIndex>(lhs, rhs);
    case QVariant::Rect:                  return lt<QRect>(lhs, rhs);
    case QVariant::RectF:                 return lt<QRectF>(lhs, rhs);
    case QVariant::RegExp:                return lt<QRegExp>(lhs, rhs);
    case QVariant::Size:                  return lt<QSize>(lhs, rhs);
    case QVariant::SizeF:                 return lt<QSizeF>(lhs, rhs);
    case QVariant::String:                return lt<QString>(lhs, rhs);
    case QVariant::StringList:            return lt<QStringList>(lhs, rhs);
    case QVariant::Time:                  return lt<QTime>(lhs, rhs);
    case QVariant::UInt:                  return lt<uint>(lhs, rhs);
    case QVariant::ULongLong:             return lt<qulonglong>(lhs, rhs);
    case QVariant::Url:                   return lt<QUrl>(lhs, rhs);
    default: assert(0 && "unexpected variant type"); return false;
    }
}

bool operator>(const QVariant& lhs, const QVariant& rhs)
{
    if (lhs.type() != rhs.type())
        return lhs.toString() > rhs.toString();

    switch (lhs.type())
    {
    case QVariant::BitArray:              return gt<QBitArray>(lhs, rhs);
    case QVariant::Bitmap:                return gt<QBitmap>(lhs, rhs);
    case QVariant::Bool:                  return gt<bool>(lhs, rhs);
    case QVariant::ByteArray:             return gt<QByteArray>(lhs, rhs);
    case QVariant::Char:                  return gt<QChar>(lhs, rhs);
    case QVariant::Date:                  return gt<QDate>(lhs, rhs);
    case QVariant::DateTime:              return gt<QDateTime>(lhs, rhs);
    case QVariant::Double:                return gt<double>(lhs, rhs);
    case QVariant::Icon:                  return gt<QIcon>(lhs, rhs);
    case QVariant::Int:                   return gt<int>(lhs, rhs);
    case QVariant::Invalid:               return false;
    case QVariant::Line:                  return gt<QLine>(lhs, rhs);
    case QVariant::LineF:                 return gt<QLineF>(lhs, rhs);
    case QVariant::Locale:                return gt<QLocale>(lhs, rhs);
    case QVariant::LongLong:              return gt<qlonglong>(lhs, rhs);
    case QVariant::PersistentModelIndex:  return gt<QPersistentModelIndex>(lhs, rhs);
    case QVariant::Point:                 return gt<QPoint>(lhs, rhs);
    case QVariant::Pixmap:                return gt<QPixmap>(lhs, rhs);
    case QVariant::PointF:                return gt<QPointF>(lhs, rhs);
    case QVariant::ModelIndex:            return gt<QModelIndex>(lhs, rhs);
    case QVariant::Rect:                  return gt<QRect>(lhs, rhs);
    case QVariant::RectF:                 return gt<QRectF>(lhs, rhs);
    case QVariant::RegExp:                return gt<QRegExp>(lhs, rhs);
    case QVariant::Size:                  return gt<QSize>(lhs, rhs);
    case QVariant::SizeF:                 return gt<QSizeF>(lhs, rhs);
    case QVariant::String:                return gt<QString>(lhs, rhs);
    case QVariant::StringList:            return gt<QStringList>(lhs, rhs);
    case QVariant::Time:                  return gt<QTime>(lhs, rhs);
    case QVariant::UInt:                  return gt<uint>(lhs, rhs);
    case QVariant::ULongLong:             return gt<qulonglong>(lhs, rhs);
    case QVariant::Url:                   return gt<QUrl>(lhs, rhs);
    default: assert(0 && "unexpected variant type"); return false;
    }
}

#endif // QT_VERSION
