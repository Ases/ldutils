/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_H
#define IO_H

#include <QFile>

namespace Io {

inline bool write32(QFile& out, uint32_t val)
{
    return out.putChar(int8_t((val >> 24) & 0xff)) &&
            out.putChar(int8_t((val >> 16) & 0xff)) &&
            out.putChar(int8_t((val >> 8) & 0xff)) &&
            out.putChar(int8_t((val >> 0) & 0xff));
}

inline bool write32(uint32_t val, FILE* out)
{
    return fputc((val >> 24) & 0xff, out) != EOF &&
            fputc((val >> 16) & 0xff, out) != EOF &&
            fputc((val >> 8) & 0xff, out)  != EOF &&
            fputc((val >> 0) & 0xff, out)  != EOF;
}

inline uchar read1(QFile& in, bool& ok)
{
    uchar byte0 = 0;

    if (!in.getChar(reinterpret_cast<char*>(&byte0)))
        ok = false;

    return byte0;
}

inline std::tuple<uchar, uchar, uchar, uchar> read4(QFile& in, bool& ok)
{
    const uchar byte0 = read1(in, ok);
    const uchar byte1 = read1(in, ok);
    const uchar byte2 = read1(in, ok);
    const uchar byte3 = read1(in, ok);
    return std::make_tuple(byte0, byte1, byte2, byte3);
}

inline uint32_t read32(QFile& in, bool& ok)
{
    const auto [byte0, byte1, byte2, byte3] = read4(in, ok);
            return uint32_t((byte0 << 24U) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
}

// Read null terminated QString or QByteArray
template <typename T>
inline bool readZ(QFile& in, T& s)
{
    bool ok = true;
    while (ok) {
        const uint8_t byte0 = read1(in, ok);
        if (byte0 == '\0')
            break;
        s.append(byte0);
    }

    return ok;
}

} // namespace Io
#endif // IO_H
