/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QMutex>
#include <QVector>
#include <QStringList>
#include <cinttypes>
#include <limits>
#include <tuple>
#include <functional>

#include <src/core/modelmetadata.h>
#include <src/util/roles.h>
#include <src/core/settings.h>
#include "treeitem.h"

class QItemSelectionModel;
class QSortFilterProxyModel;
class QIcon;
class QMimeData;
class Units;

class TreeModel : public QAbstractItemModel, public Settings
{
    Q_OBJECT

public:
    explicit TreeModel(TreeItem* root, QObject *parent = nullptr);
    virtual ~TreeModel() override;

    // Queries
    virtual QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const override;

    virtual QModelIndex index(int row, int column,
                              const QModelIndex& parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex& index) const override;

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Convenience: index of sibling within same row as index
    QModelIndex rowSibling(int column, const QModelIndex &index) const {
        return sibling(index.row(), column, index);
    }

    // Convenience function: set sibling data within row
    virtual bool setData(int column, const QModelIndex &idx, const QVariant& value,
                         int role = Qt::DisplayRole);

    // Convenience function: get sibling data within row
    virtual QVariant data(int column, const QModelIndex &idx, int role = Qt::DisplayRole) const;

    // Updates
    virtual bool moveRow(const QModelIndex& srcParent, int srRow,
                         const QModelIndex& dstParent, int dstRow);

    virtual bool moveRows(const QModelIndex &srcParent, int srcRow, int count,
                          const QModelIndex &dstParent, int dstRow) override;

    virtual bool setData(const QModelIndex &idx, const QVariant& value,
                         int role = Qt::DisplayRole) override;
    virtual bool multiSet(const QModelIndexList& indexes, const QVariant& value,
                          int role = Qt::DisplayRole);
    virtual bool clearData(const QModelIndex &idx, int role = Qt::DisplayRole);
    virtual bool clearData(int column, const QModelIndex &idx, int role = Qt::DisplayRole);
    virtual bool setHeaderData(int section, Qt::Orientation orientation,
                               const QVariant& value, int role = Qt::DisplayRole) override;

    virtual bool insertColumns(int position, int columns,
                               const QModelIndex& parent = QModelIndex()) override;
    virtual bool removeColumns(int position, int columns,
                               const QModelIndex& parent = QModelIndex()) override;
    virtual bool insertRows(int position, int rows,
                            const QModelIndex& parent = QModelIndex()) override;
    virtual bool removeRows(int position, int rows,
                            const QModelIndex& parent = QModelIndex()) override;

    virtual void removeRows(const std::function<bool(const QModelIndex&)>& predicate,
                            const QModelIndex& parent = QModelIndex());

    virtual void removeRows(const QItemSelectionModel*, const QSortFilterProxyModel* = nullptr,
                            const QModelIndex& parent = QModelIndex());

    virtual bool appendRow(const QModelIndex& parent = QModelIndex());

    virtual bool appendRow(const TreeItem::ItemData& data,
                           const QModelIndex& parent = QModelIndex());

    virtual bool appendRows(const QVector<TreeItem::ItemData>& data,
                            const QModelIndex& parent = QModelIndex());

    virtual bool insertRow(const TreeModel& srcModel, const QModelIndex& srcIdx,
                           int position, const QModelIndex& parent = QModelIndex());

    // Convenience function to set both icon and its name.
    virtual bool setIcon(const QModelIndex& idx, const QString&);
    virtual bool clearIcon(const QModelIndex& idx);
    virtual bool setSiblingIcon(int column, const QModelIndex& idx, const QString&);

    void setHorizontalHeaderLabels(const QStringList &labels);
    QStringList headerLabels() const;
    int findHeaderColumn(const QString&) const;

    // Apply some operation to a subtree. Function should return true recurse into subtree.
    virtual void apply(const std::function<bool(const QModelIndex&)>& fn,
                       const QModelIndex& parent = QModelIndex()) const;

    // Drops data into a queue for later update from main render thread
    void setDataFromThread(const QModelIndex& idx, const QVariant& value,
                           int role = Qt::DisplayRole);

    // Queries
    virtual int count(const std::function<bool(const QModelIndex&)>& predicate,
                      const QModelIndex& parent = QModelIndex()) const;

    virtual QModelIndex findRow(const QModelIndex& parent,
                                const std::function<bool(const QModelIndex&)>& predicate,
                                int maxDepth = std::numeric_limits<int>::max()) const;

    virtual QModelIndex findRow(const QModelIndex& parent,
                                const QVariant& data, int column, int role = Qt::DisplayRole,
                                int maxDepth = std::numeric_limits<int>::max()) const;

    QModelIndex child(int row, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex lastChild(const QModelIndex& parent = QModelIndex()) const;

    virtual void refresh(); // refresh view from underlying data (e.g, on settings change)

    virtual const Units& units(const QModelIndex& idx) const;
    const Units& units(int column) const;  // default units for column (may be overriden for any given index)

    static void setBrokenIcon(const QIcon&);

    // *** begin Settings API
    virtual void save(QSettings&) const override;
    virtual void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    virtual QDataStream& saveForUndo(QDataStream&, const QModelIndex& parent, int first = 0, int count = -1) const;
    virtual QDataStream& loadForUndo(QDataStream&, const QModelIndex& parent, int first);
    virtual QDataStream& save(QDataStream&, const QModelIndex& parent = QModelIndex()) const;
    virtual QDataStream& load(QDataStream&, const QModelIndex& parent = QModelIndex(), bool append = false);
    virtual QDataStream& load(QDataStream&, const QModelIndex& parent, bool append,
                              const std::function<void()>& preHook, const std::function<void()>& postHook);
    // *** end Stream Save API

    // Helper to obtain data as a particular type
    template <typename T> T as(const QModelIndex& idx, ModelType mt, int role = Util::RawDataRole) const {
        return data(mt, idx, role).value<T>();
    }

    virtual TreeModel& operator=(const TreeModel&);

    // sort items under this parent
    virtual void sort(int column,
                      const QModelIndex& parent = QModelIndex(),
                      Qt::SortOrder order = Qt::AscendingOrder,
                      int role = Qt::DisplayRole,
                      bool recursive = true);

    static QIcon brokenIcon;

signals:
    void itemSaved(qint64) const;  // for save progress
    void itemLoaded(qint64) const; // for load progress

protected:
    virtual void saveItem(const QModelIndex&, QDataStream&, const TreeModel&) const;
    virtual void loadItem(const QModelIndex&, QDataStream&, TreeModel&);

    virtual void copyItem(const QModelIndex& dstIdx, const TreeModel& srcModel, const QModelIndex& srcIdx);

    TreeItem *getItem(const QModelIndex &index) const;

    // For drag and drop
    virtual QStringList mimeTypes() const override;
    virtual QMimeData* mimeData(const QModelIndexList &indexes) const override;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                              int dstRow, int dstCol, const QModelIndex& dstParent) override;

    virtual bool dragMove(TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                          int dstRow, int dstCol, const QModelIndex& dstParent);

    virtual bool dragCopy(TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                          int dstRow, int dstCol, const QModelIndex& dstParent);

    bool isAncestor(const QModelIndex& ancestor, QModelIndex descendent);

    mutable QMutex mutex;

private:
    using QAbstractItemModel::sort; // hide

    static const constexpr char* internalMoveMimeType = "application/x-tag-model";

    TreeItem*    rootItem;
};

#endif // TREEMODEL_H
