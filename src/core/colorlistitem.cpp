/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "colorlistitem.h"
#include "uicolormodel.h"

ColorListItem::ColorListItem(TreeItem* parent) :
    TreeItem(parent)
{
}

ColorListItem::ColorListItem(const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(parent)
{
    if (data.size() >= UiColorModel::_Count) {
        setData(UiColorModel::Name,  data[UiColorModel::Name],  Qt::DisplayRole);
        setData(UiColorModel::Color, data[UiColorModel::Color], Qt::BackgroundRole);
    }
}

QVariant ColorListItem::data(int column, int role) const
{
    if (role == Qt::EditRole)
        role = Qt::DisplayRole;

    if (role == Qt::ForegroundRole && column == UiColorModel::Name)
        return TreeItem::data(UiColorModel::Color, Qt::BackgroundRole);

    return TreeItem::data(column, role);
}

bool ColorListItem::setData(int column, const QVariant &value, int role, bool &changed)
{
    if (role == Qt::EditRole) {
        switch (column) {
        case UiColorModel::Color: role = Qt::BackgroundRole; break;
        default:                  role = Qt::DisplayRole; break;
        }
    }

    return TreeItem::setData(column, value, role, changed);
}

int ColorListItem::columnCount() const
{
    return UiColorModel::_Count;
}

ColorListItem* ColorListItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    return new ColorListItem(data, parent);
}
