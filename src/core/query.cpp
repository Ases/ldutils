/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>
#include <QScopedPointer>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QMap>

#include <src/core/treemodel.h>
#include <src/util/variantcmp.h>

#include "query.h"

namespace Query {

bool Base::isValid() const
{
    return true;
}

// Column bounds: [begin, end)
std::pair<int, int> Context::colBounds(int colCount, int column, int colOverride)
{
    if (colOverride != Base::NoColumn)
        column = colOverride;

    return std::make_pair((column == Base::AnyColumn) ? 0 : column,
                          (column == Base::AnyColumn) ? colCount : (column + 1));
}

void Context::setModel(const QAbstractItemModel* model, Qt::CaseSensitivity cs)
{
    setCaseSensitivity(cs);

    headerMap.clear();
    headers.clear();
    columnUnits.clear();

    if (model == nullptr)
        return;

    columnCount = model->columnCount();
    columnUnits.reserve(columnCount);
    headers.reserve(columnCount);

    const TreeModel* treeModel = dynamic_cast<const TreeModel*>(model);

    for (int col = 0; col < columnCount; ++col) {
        const QString headerString = model->headerData(col, Qt::Horizontal).toString().replace(' ', '_');

        // Remember header names, for parsing those.
        headerMap.insert(headerString.toLower(), col);
        headers.append(headerString);

        // Remember per-column unit suffixes, for parsing those.
        columnUnits.append(treeModel != nullptr ? &treeModel->units(col) : nullptr);
    }
}

QString Context::toString(const Base* node) const
{
    if (node == nullptr)
        return QString();

    QString string;
    node->toString(*this, string);
    return string;
}

// Simple tokenizer for our wee query language
QStringRef Context::nextToken() const
{
    // Regular expression which matches our operators
    static const QRegularExpression operatorExp(" +[<>=!]=|[<>=!]= +| +=~|=~ +| +\\!~|\\!~ +| +[|&^<>()!:]|[|&^<>()!:] +|$");

    const auto match = operatorExp.match(text);

    QStringRef token;

    if (!match.hasMatch())              // no match: empty ref into text
        token = text.mid(text.size());
    else if (match.capturedStart() > 0) // text up to next operator
        token = text.left(match.capturedStart()).trimmed();
    else                                // the operator itself
        token = match.capturedRef().trimmed();

    tokenPos = token.position();
    tokenLen = token.length();

    return token;
}

// Eat token, return next one.
inline QStringRef Context::eat() const
{
    text = text.mid(tokenPos + tokenLen - text.position());
    return nextToken();
}

// Eat given number of characters, return next token.
inline QStringRef Context::eat(int len) const
{
    text = text.mid(len);
    return nextToken();
}

Seq::Type Context::parseSeqType(const QStringRef& token)
{
    if (token == "&" || token == "&&") return Seq::Type::And;
    if (token == "^" || token == "^^") return Seq::Type::Xor;
    if (token == "|" || token == "||") return Seq::Type::Or;
    if (token == ",") return Seq::Type::Seq;

    return Seq::Type::None;
}

Rel::Cmp Context::parseCompare(const QStringRef& token)
{
    if (token == "==") return Rel::Cmp::EQ;
    if (token == "!=") return Rel::Cmp::NE;
    if (token == "<")  return Rel::Cmp::LT;
    if (token == "<=") return Rel::Cmp::LE;
    if (token == ">")  return Rel::Cmp::GT;
    if (token == ">=") return Rel::Cmp::GE;
    if (token == "=~" || token == ':') return Rel::Cmp::Regex;
    if (token == "!~") return Rel::Cmp::NoRegex;

    return Rel::Cmp::None;
}

int Context::parseColumnName(const QStringRef& token) const
{
    if (token == "*")
        return Base::AnyColumn;

    const QString canonical = token.toString().toLower().replace(' ', '_');

    const auto hdr = headerMap.find(canonical);

    return (hdr == headerMap.end()) ? Base::NoColumn : hdr.value();
}

auto Context::parseUnitSuffix(int col) const
{
    const auto [beginCol, endCol] = colBounds(columnCount, col);

    int maxSizeFound = 0;
    std::tuple<QVariant, Format> result;

    for (int c = beginCol; c < endCol; ++c) {
        const QString s = text.toString().toUtf8().data();
        if (c < columnUnits.size()) {
            if (const auto [value, format, size] = columnUnits.at(c)->parse(s); size > 0) {
                if (size > maxSizeFound) {
                    maxSizeFound = size;
                    result = std::make_tuple(value, format);
                }
            }
        }
    }

    if (maxSizeFound > 0) {
        eat(maxSizeFound);  // eat however much the units parser thinks we should.  nom nom.
        return result;
    }

    return std::make_tuple(QVariant(), Format::_Invalid);
}

const Base* Context::unwind(const QStringRef& unwindPt, bool hasError) const
{
    if (hasError)
        error = true;

    text = unwindPt;
    nextToken();
    return nullptr;
}

const Rel* Rel::make(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
                     Format format)
{
    const Rel* rel = new Rel(ctx, pattern, cmpType, caseSensitivity, column, role, format);
    if (rel != nullptr && rel->isValid())
        return rel;

    delete rel;
    ctx.error = true;
    return nullptr;
}

// <column> <cmp> <value> <unit>
const Base* Context::parseColumn() const
{
    bool isNumber;
    const QStringRef unwindPt = text;  // for unwinding

    const int column   = parseColumnName(token());
    const Rel::Cmp cmp = parseCompare(eat());

    if (column == Base::NoColumn)
        return unwind(unwindPt, cmp != Rel::Cmp::None);

    if (cmp == Rel::Cmp::None)
        return unwind(unwindPt, false);

    if (eat().isEmpty())
        return unwind(unwindPt, true);

    const auto [value, fmt] = parseUnitSuffix(column);
    if (!value.isValid())
        return unwind(unwindPt, true);

    // Build relative compare node for the data we parsed.
    if (Rel::isRegex(cmp))
        // Regex match.
        return Rel::make(*this, value.toString(), cmp, caseSensitivity, column, regexRole, fmt);
    else if (double dval = value.toDouble(&isNumber); isNumber)
        // Numeric or string comparisons
        return Rel::make(*this, dval, cmp, caseSensitivity, column, relRole, fmt);
    else if (QDateTime date = value.toDateTime(); date.isValid())
        // Date comparisons
        return Rel::make(*this, date, cmp, caseSensitivity, column, relRole, fmt);
    else if (QTime time = value.toTime(); time.isValid())
        // Time comparisons
        return Rel::make(*this, time, cmp, caseSensitivity, column, relRole, fmt);
    else
        // Relative comparisons
        return Rel::make(*this, value.toString(), cmp, caseSensitivity, column, strRelRole, fmt);
}

// "!" <pattern>
const Base* Context::parseNeg() const
{
    const QStringRef unwindPt = text;  // for unwinding

    if (token() != "!")
        return nullptr;

    eat(); // eat the bang.  nom nom.

    const Base* pattern = parsePattern();
    if (pattern != nullptr)
        return new Neg(pattern);
    else
        return unwind(unwindPt, true);
}

// <pattern> "|" <pattern> ...
// <pattern> "&" <pattern> ...
// <pattern> "," <pattern> ...
const Base* Context::parseSeq(Seq::Type prec, const Base* rhs, Seq::Type* nextType) const
{
    const Base* node;
    Seq* seq = nullptr;
    Seq::Type thisType = Seq::Type::None;
    Seq::Type type;

    while (true) {
        if (rhs) {
            node = rhs;
            rhs = nullptr;
            type = prec;
        } else {
            if (parseSeqType(token()) != Seq::Type::None) {
                delete seq;
                return nullptr;
            }

            if (token().isEmpty()) {
                node = nullptr;
                type = Seq::Type::None;
            } else {
                node = parsePattern();

                if (node == nullptr) {
                    delete seq;
                    return nullptr;
                }

                type = parseSeqType(token());
                if (type != Seq::Type::None)
                    eat();  // eat the sequence type.  nom nom.
            }
        }

        if (nextType != nullptr)
            *nextType = type;

        // End of sequence for this precedence
        if (type == Seq::Type::None || type > prec)
            return (seq == nullptr) ? node : seq->append(node);

        // First node in the sequence
        if (seq == nullptr) {
            seq = new Seq(thisType = type, node);
            continue;
        }

        if (type < thisType) {
            // Higher priority chains become sub-nodes
            seq->append(parseSeq(type, node, &type));
            node = nullptr;
        }

        if (type >= thisType && type != Seq::Type::None) {
            seq->append(node); // add to current sequence

            // Lower priority chain becomes super-node
            if (type > thisType)
                seq = new Seq(thisType = type, seq);
        }
    }
}

const Base* Context::parseRegex() const
{
    const auto [value, fmt] = parseUnitSuffix(Base::AnyColumn);
    if (!value.isValid()) {
        error = true;
        return nullptr;
    }

    return Rel::make(*this, value.toString(), Rel::Cmp::Regex, caseSensitivity, Base::AnyColumn, regexRole, fmt);
}

const Base* Context::parseParen() const
{
    const QStringRef unwindPt = text;  // for unwinding

    if (token() != "(")
        return nullptr;

    eat();  // eat the open paren.  nom nom.
    const Base* seq = parseSeq();

    if (token() != ")") {
        delete seq;
        return unwind(unwindPt, true);
    }

    eat();   // eat the close paren.  nom nom.
    return seq;
}

const Base* Context::parsePattern() const
{
    if (const Base* node = parseNeg(); node != nullptr && !error)
        return node;

    if (const Base* node = parseColumn(); node != nullptr && !error)
        return node;

    if (const Base* node = parseParen(); node != nullptr && !error)
        return node;

    if (const Base* node = parseRegex(); node != nullptr && !error)
        return node;

    return nullptr;
}

// Parse string, return node tree.
const Base* Context::parse(const QString& t) const
{
    if (t.isEmpty())
        return new All();

    text  = QStringRef(&t);
    nextToken();
    error = false;

    if (const Base* node = parseSeq(); node != nullptr)
        return node;

    return new None();
}

bool Context::isValidQuery(const QString& query) const
{
    const QScopedPointer<const Base> node(parse(query));

    return !node.isNull() && node->isValid();
}

bool None::match(const QVariant&) const
{
    return false;
}

bool None::match(const QAbstractItemModel&, const QModelIndex&, int, int) const
{
    return false;
}

void None::toString(const Context&, QString& string) const
{
    string = QObject::tr("<error>");
}

bool None::operator==(const Base& rhsBase) const
{
    return dynamic_cast<const None*>(&rhsBase) == this;
}

bool All::match(const QVariant&) const
{
    return false;
}

bool All::match(const QAbstractItemModel&, const QModelIndex&, int, int) const
{
    return true;
}

void All::toString(const Context&, QString& string) const
{
    string.clear();
}

bool All::operator==(const Base& rhsBase) const
{
    return dynamic_cast<const All*>(&rhsBase) == this;
}

// Relational
bool Rel::match(const QVariant& data) const
{
    if (cmpType == Cmp::Regex) {
        return regex.match(data.toString()).hasMatch();
    } else if (cmpType == Cmp::NoRegex) {
        return !regex.match(data.toString()).hasMatch();
    } else if (data.type() == QVariant::String && pattern.type() == QVariant::String) {
        // Handle case sensitivity for string compares
        const QString* dataStr = static_cast<const QString*>(data.constData());
        const QString* pattStr = static_cast<const QString*>(pattern.constData());

        const int cmp = dataStr->compare(*pattStr, caseSensitivity);
        switch (cmpType) {
        case Cmp::EQ: return cmp == 0;
        case Cmp::NE: return cmp != 0;
        case Cmp::LT: return cmp < 0;
        case Cmp::LE: return cmp <= 0;
        case Cmp::GT: return cmp > 0;
        case Cmp::GE: return cmp >= 0;
        case Cmp::Regex:   assert(0); break;
        case Cmp::NoRegex: assert(0); break;
        case Cmp::None:    assert(0); break;
        }
    } else {
        // Normal comparisons
        switch (cmpType) {
        case Cmp::EQ: return data == pattern;
        case Cmp::NE: return data != pattern;
        case Cmp::LT: return data < pattern;
        case Cmp::LE: return data <= pattern;
        case Cmp::GT: return data > pattern;
        case Cmp::GE: return data >= pattern;
        case Cmp::Regex:   assert(0); break;
        case Cmp::NoRegex: assert(0); break;
        case Cmp::None:    assert(0); break;
        }
    }

    return false;
}

// Relational
bool Rel::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    const auto [beginCol, endCol] = Context::colBounds(model.columnCount(parent), column, col);

    for (int column = beginCol; column < endCol; ++column) {
        // Skip if the units are not compatible.
        if (column < context.columnUnits.size())
            if (format != Format::_Invalid && !context.columnUnits.at(column)->validInRange(format))
                continue;

        if (match(model.data(model.index(row, column, parent), role)))
            return true;
    }

    return false;
}

const QVector<Rel::Cmp>& Rel::comparisons()
{
    static const std::remove_reference<decltype(comparisons())>::type cmpList = {
         Cmp::EQ, Cmp::NE, Cmp::LT, Cmp::LE, Cmp::GT, Cmp::GE, Cmp::Regex, Cmp::NoRegex
    };

    return cmpList;
}

QString Rel::cmpToString(Cmp cmp)
{
    switch (cmp) {
    case Cmp::EQ:      return "==";
    case Cmp::NE:      return "!=";
    case Cmp::LT:      return "<";
    case Cmp::LE:      return "<=";
    case Cmp::GT:      return ">";
    case Cmp::GE:      return ">=";
    case Cmp::Regex:   return "=~";
    case Cmp::NoRegex: return "!~";
    case Cmp::None:    assert(0);
    }

    return "";
}

void Rel::toString(const Context& ctx, QString& string) const
{
    if (column >= 0)
        string += ctx.headers.at(column);
    else if (!isRegex())
        string += "*";

    if (!isRegex() || column >= 0) {
        string += " ";
        string += cmpToString(cmpType);
        string += " ";
    }

    if (column >= 0 && column < ctx.columnUnits.size())
        string += ctx.columnUnits.at(column)->toString(pattern, format);
    else
        string += pattern.toString();
}

bool Rel::operator==(const Base& rhsBase) const
{
    const Rel* rhs = dynamic_cast<const Rel*>(&rhsBase);

    return rhs               == this            &&
        rhs->pattern         == pattern         &&
        rhs->regex           == regex           &&
        rhs->cmpType         == cmpType         &&
        rhs->caseSensitivity == caseSensitivity &&
        rhs->column          == column          &&
        rhs->role            == role            &&
        rhs->format          == format;
}

bool Rel::isValid() const
{
    return !isRegex() || regex.isValid();
}

bool Neg::match(const QVariant& data) const
{
    if (node == nullptr)
        return false;

    return !node->match(data);
}

// Negation
bool Neg::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    if (node == nullptr)
        return false;

    return !node->match(model, parent, row, col);
}

void Neg::toString(const Context& ctx, QString& string) const
{
    string += "! ( ";
    node->toString(ctx, string);
    string += " )";
}

bool Neg::operator==(const Base& rhsBase) const
{
    const Neg* rhs = dynamic_cast<const Neg*>(&rhsBase);

    return rhs == this &&
        *node  == *rhs->node;
}

bool Seq::match(const QVariant& data) const
{
    switch (cmpType) {
    case Type::Seq: [[fallthrough]];
    case Type::And: // match all nodes
        for (const auto& node : cmpNode)
            if (node != nullptr && !node->match(data))
                return false;
        return true;

    case Type::Xor: // exclusive or of two nodes
        if (cmpNode.size() > 1) {
            bool result = (cmpNode[0] == nullptr ? false : cmpNode[0]->match(data));
            for (int i = 1; i < cmpNode.size(); ++i)
                if (cmpNode[i] != nullptr)
                    result = (result != (cmpNode[i] == nullptr ? false : cmpNode[i]->match(data)));

            return result;
        }
        return false;

    case Type::Or: // match any node
        for (const auto& node : cmpNode)
            if (node != nullptr && node->match(data))
                return true;
        return false;

    case Type::None:
        assert(0 && "Unknown comparison");
        break;
    }

    return false;
}

// Sequence
bool Seq::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    switch (cmpType) {
    case Type::And: // match all nodes
        for (const auto& node : cmpNode)
            if (node != nullptr && !node->match(model, parent, row, col))
                return false;
        return true;

    case Type::Xor: // exclusive or of two nodes
        if (cmpNode.size() > 1) {
            bool result = (cmpNode[0] == nullptr ? false : cmpNode[0]->match(model, parent, row, col));
            for (int i = 1; i < cmpNode.size(); ++i)
                result = (result != (cmpNode[i] == nullptr ? false : cmpNode[i]->match(model, parent, row, col)));

            return result;
        }
        return false;

    case Type::Or: // match any node
        for (const auto& node : cmpNode)
            if (node != nullptr && node->match(model, parent, row, col))
                return true;
        return false;

    case Type::Seq: { // match columns in (non-monotonical) increasing order
        const int colCount = model.columnCount(parent);
        col = std::max(col, 0);
        for (const auto& node : cmpNode) {
            while (col < colCount && node != nullptr && !node->match(model, parent, row, col))
                ++col;
            if (col >= colCount)
                return false;
        }

        return true;
    }

    case Type::None:
        assert(0 && "Unknown comparison");
        break;
    }

    return false;
}

void Seq::toString(const Context& ctx, QString& string) const
{
    string += "( ";

    for (const auto& node : cmpNode) {
        if (node != nullptr) {
            node->toString(ctx, string);
            if (node != cmpNode.back()) {
                switch (cmpType) {
                case Type::And: string += " & "; break;
                case Type::Xor: string += " ^ "; break;
                case Type::Or:  string += " | "; break;
                case Type::Seq: string += " , "; break;
                case Type::None: assert(0); break;
                }
            }
        }
    }

    string += " )";
}

bool Seq::operator==(const Base& rhsBase) const
{
    const Seq* rhs = dynamic_cast<const Seq*>(&rhsBase);

    if (rhs != this ||
        rhs->cmpType != cmpType ||
        rhs->cmpNode.size() != cmpNode.size())
        return false;

    for (int i = 0; i < cmpNode.size(); ++i)
        if (*cmpNode.at(i) != *rhs->cmpNode.at(i))
            return false;

    return true;
}

} // namespace Query
