/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CFGDATABASE_H
#define CFGDATABASE_H

#include <QIcon>

#include <src/core/settings.h>
#include <src/core/uicolormodel.h>

class CfgDataBase : public Settings
{
public:
    explicit CfgDataBase(uint32_t version);
    virtual ~CfgDataBase() override;

    virtual void reset() = 0; // reset model to defaults

    QString       colSeparator;         // col separator for copying to clipboard
    QString       rowSeparator;         // row separator for copying to clipboard

    bool          caseSensitiveFilters; // true to make filters case sensitive
    bool          caseSensitiveSorting; // true to make sorting case sensitive
    bool          warnOnClose;          // warn on tab/etc close
    bool          warnOnRemove;         // warn on data deletion
    bool          warnOnRevert;         // warn on revert
    bool          warnOnExit;           // warn on application exit

    bool          inlineCompletion;     // true to use inline style completion
    int           completionListSize;   // number of items in completion popup

    QString       brokenIcon;           // icon to use if icon file not found
    QString       filterValidIconName;  // icon to show if filter is valid
    QString       filterInvalidIconName;// icon to show if filter is invalid
    QString       filterEmptyIconName;  // no filter text

    UiColorModel  uiColor;              // ui color configuration

    QIcon         filterValidIcon;      // For display: Not saved
    QIcon         filterInvalidIcon;    // ...
    QIcon         filterEmptyIcon;      // ...

    int32_t       backupDataCount;      // number of GPS data file backups
    int32_t       dataAutosaveInterval; // auto-save interval, seconds.
    QString       dataAutosavePath;     // auto-save path: use UI .conf path if blank

    int           backupUICount;        // number of UI settings backups
    int           maxUndoCount;         // max size of undo/redo stacks
    float         maxUndoSizeMiB;       // max size of undos in float MiB

    uint32_t      cfgDataVersion;       // save format version, so we can update old cfg data on load
    uint32_t      priorCfgDataVersion;  // version we loaded from, pre-conversion, for other classes to use on load

    Qt::CaseSensitivity getFilterCaseSensitivity() const {
        return caseSensitiveFilters ? Qt::CaseSensitive : Qt::CaseInsensitive;
    }

    Qt::CaseSensitivity getSortCaseSensitivity() const {
        return caseSensitiveSorting ? Qt::CaseSensitive : Qt::CaseInsensitive;
    }

    virtual void applyGlobal(); // apply some globally scoped data after load or update

    // *** begin Settings API
    virtual void save(QSettings&) const override;
    virtual void load(QSettings&) override;
    // *** end Settings API

    static const constexpr QSize iconPad = QSize(4, 4);

private:
    bool operator==(const CfgDataBase&) const = delete;
};

#endif // CFGDATABASE_H
