/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QGuiApplication>
#include <QColor>
#include <QRgb>

#include <src/util/ui.h>
#include "uicolormodel.h"

UiColorModel::UiColorModel(QObject *parent) :
    ColorListModel(parent)
{
    addMissing(); // set default colors
}

QString UiColorModel::name(UiType ml)
{
    switch (ml) {
    case UiType::Normal:             return QObject::tr("Normal");
    case UiType::Info:               return QObject::tr("Info");
    case UiType::Success:            return QObject::tr("Success");
    case UiType::Warning:            return QObject::tr("Warning");
    case UiType::Error:              return QObject::tr("Error");
    case UiType::Critical:           return QObject::tr("Critical");
    case UiType::Cmdline:            return QObject::tr("Cmd Line");
    case UiType::Stdout:             return QObject::tr("Stdout");
    case UiType::Stderr:             return QObject::tr("Stderr");
    case UiType::_Count: assert(0);  break;
    }

    return "n/a";
}


QColor UiColorModel::color(UiType ml)
{
    const QPalette& palette = QGuiApplication::palette();

    const bool light = Util::IsLightTheme();

    switch (ml) {
    case UiType::Normal:             return palette.color(QPalette::Text);
    case UiType::Info:               return palette.color(QPalette::Highlight);
    case UiType::Success:            return light ? QRgb(0xff22aa22) : QRgb(0xff22ee22);
    case UiType::Warning:            return light ? QRgb(0xff999900) : QRgb(0xffcccc00);
    case UiType::Error:              return light ? QRgb(0xffaa5511) : QRgb(0xffee6622);
    case UiType::Critical:           return light ? QRgb(0xffcc0000) : QRgb(0xffff0000);
    case UiType::Cmdline:            return palette.color(QPalette::Highlight);
    case UiType::Stdout:             return palette.color(QPalette::Text);
    case UiType::Stderr:             return light ? QRgb(0xffaa8800) : QRgb(0xffffaa00);
    case UiType::_Count: assert(0);  break;
    }

    return QColor::fromRgb(0xff, 0xff, 0x00);
}

void UiColorModel::addMissing()
{
    // Fill out any part of color array not loaded
    while (rowCount() < int(UiType::_Count))
        appendRow( { name(UiType(rowCount())),
                     color(UiType(rowCount())) }
                 );

    // Trim any excess ones (if # types has shunk from save)
    while (rowCount() > int(UiType::_Count))
        removeRow(rowCount() - 1);
}
