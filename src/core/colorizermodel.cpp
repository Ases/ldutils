/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <QItemSelectionModel>
#include <src/util/roles.h>
#include <src/util/util.h>

#include "colorizermodel.h"

ColorizerModel::ColorizerModel(const TreeModel* colorizeModel, const CfgDataBase& cfgData, QObject* /*parent*/) :
    TreeModel(new ColorizerItem(*this)),
    cfgData(cfgData),
    m_colorizeModel(colorizeModel),
    m_queryCtx(colorizeModel),
    m_dirty(true),
    m_lastMatchItem(nullptr)
{
}

void ColorizerModel::setModel(const TreeModel* colorizeModel)
{
    m_queryCtx.setModel(m_colorizeModel = colorizeModel, Qt::CaseInsensitive);
}

const ColorizerItem* ColorizerModel::getItem(const QModelIndex &idx) const
{
    return static_cast<const ColorizerItem*>(TreeModel::getItem(idx));
}

ColorizerItem* ColorizerModel::getItem(const QModelIndex &idx)
{
    return static_cast<ColorizerItem*>(TreeModel::getItem(idx));
}

QVariant ColorizerModel::data(const QModelIndex& idx, int role) const
{
    if (role == Qt::TextAlignmentRole)
        return QVariant(mdAlignment(idx.column()));

    const ColorizerItem* item = getItem(idx);
    if (item == nullptr)
        return QVariant();

    return item->data(cfgData, idx.column(), role);
}

bool ColorizerModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    m_dirty = true;
    return TreeModel::setData(idx, value, role);
}

bool ColorizerModel::setHeaderData(int /*section*/, Qt::Orientation /*orientation*/,
                                   const QVariant& /*value*/, int /*role*/)
{
    assert(0);
    return false; // not supported
}

bool ColorizerModel::insertColumns(int /*position*/, int /*columns*/, const QModelIndex&)
{
    assert(0);
    return false; // not supported
}

bool ColorizerModel::removeColumns(int /*position*/, int /*columns*/, const QModelIndex&)
{
    assert(0);
    return false; // not supported
}

QVariant ColorizerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (QVariant val = ModelMetaData::headerData<ColorizerModel>(section, orientation, role); val.isValid())
        return val;

    if (role == Qt::DisplayRole)
        return mdName(section);

    return TreeModel::headerData(section, orientation, role);
}

Qt::ItemFlags ColorizerModel::flags(const QModelIndex& idx) const
{
    Qt::ItemFlags flags = TreeModel::flags(idx) | Qt::ItemIsEditable | Qt::ItemIsDragEnabled;

    if (!idx.isValid())
         flags |= Qt::ItemIsDropEnabled;

    switch (idx.column()) {
    case ColorizerModel::FgColor:    [[fallthrough]];
    case ColorizerModel::BgColor:    [[fallthrough]];
    case ColorizerModel::Icon:       flags &= ~Qt::ItemIsSelectable; break;
    case ColorizerModel::MatchCase:  [[fallthrough]];
    case ColorizerModel::HideText:   flags |= Qt::ItemIsUserCheckable; break;
    default: break;
    }

    return flags;
}

bool ColorizerModel::insertRows(int position, int rows, const QModelIndex& parent)
{
    m_dirty = true;
    return TreeModel::insertRows(position, rows, parent);
}

bool ColorizerModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    m_dirty = true;
    return TreeModel::removeRows(position, rows, parent);
}

void ColorizerModel::clearData(const QItemSelectionModel* selectionModel, ModelType mt)
{
    if (selectionModel == nullptr)
        return;

    for (const auto& idx : selectionModel->selectedRows())
        getItem(idx)->clearData(mt);
}

inline const ColorizerItem* ColorizerModel::firstMatch(const QModelIndex& idx) const
{
    if (m_colorizeModel == nullptr)
        return nullptr;

    if (m_dirty)
        updateCache();

    // Performance: we are called repeatedly on the same index for different roles
    if (idx == m_lastMatchIdx)
        return m_lastMatchItem;

    m_lastMatchIdx = idx;

    for (const auto& item : m_cache.at(idx.column()))
        if (item->match(*m_colorizeModel, idx))
            return m_lastMatchItem = item;

    return m_lastMatchItem = nullptr;
}

// Possibly remove text.  Designed to be a fast passthrough if that isn't requested.
const QVariant& ColorizerModel::maybeUse(const QVariant& value, const QModelIndex& idx, int role) const
{
    static const QVariant empty;

    // If it isn't a role we'll hide, or there's no rule to do so, return what we were passed.
    if ((role != Qt::DisplayRole && role != Util::CopyRole) || !m_canHideText.at(idx.column()))
        return value;

    const ColorizerItem* item = firstMatch(idx);
    if (item == nullptr)
        return value;  // no match
    
    return item->m_hideText ? empty : value;
}

const ColorizerItem* ColorizerModel::find(const QVariant& data, ModelType mt) const
{
    if (m_dirty)
        updateCache();

    for (const auto& item : m_cache.at(mt))
        if (item->match(data))
            return item;

    return nullptr;
}

const ColorizerItem* ColorizerModel::find(const QModelIndex& idx) const
{
    return firstMatch(idx);
}

// Search for colorize data (FG color, BG color, icon) for the given idx in the
// m_colorizeModel, and return data if found.
QVariant ColorizerModel::colorize(const QModelIndex& idx, int role) const
{
    switch (role) {
    case Qt::ForegroundRole: [[fallthrough]];
    case Qt::BackgroundRole: [[fallthrough]];
    case Qt::DecorationRole: break;
    default:                 return QVariant();
    }

    const ColorizerItem* item = firstMatch(idx);
    if (item == nullptr)
        return QVariant();

    switch (role) {
    case Qt::ForegroundRole: return item->fgColor();
    case Qt::BackgroundRole: return item->bgColor();
    case Qt::DecorationRole: return item->icon();
    default:                 assert(0); return QVariant();
    }
}

QString ColorizerModel::mdName(ModelType mt)
{
    switch (mt) {
    case ColorizerModel::Column:    return QObject::tr("Column");
    case ColorizerModel::Query:     return QObject::tr("Query");
    case ColorizerModel::FgColor:   return QObject::tr("FgColor");
    case ColorizerModel::BgColor:   return QObject::tr("BgColor");
    case ColorizerModel::Icon:      return QObject::tr("Icon");
    case ColorizerModel::MatchCase: return QObject::tr("Match Case");
    case ColorizerModel::HideText:  return QObject::tr("Hide Text");
    case ColorizerModel::_Count:    break;
    }

    assert(0 && "Unknown ColorizerModel value");
    return "";
}

bool ColorizerModel::mdIsEditable(ModelType)
{
    return true;
}

// Return true if this data can be placed in a chart
bool ColorizerModel::mdIsChartable(ModelType)
{
    return false;
}

QString ColorizerModel::mdTooltip(ModelType mt)
{
    switch (mt) {
    case ColorizerModel::Column:    return QObject::tr("<i></i>Column to colorize.");
    case ColorizerModel::Query:     return QObject::tr("<i></i>Query text.");
    case ColorizerModel::FgColor:   return QObject::tr("<i></i>Foreground text color, if set.");
    case ColorizerModel::BgColor:   return QObject::tr("<i></i>Background text color, if set.");
    case ColorizerModel::Icon:      return QObject::tr("<i></i>Icon to display in column.");
    case ColorizerModel::MatchCase: return QObject::tr("<i></i>Enable for case sensitive matching.");
    case ColorizerModel::HideText:  return QObject::tr("<i></i>Whether to hide text (to display only icon).");
    case ColorizerModel::_Count:    break;
    }

    assert(0 && "Unknown ColorizerModel value");
    return "";
}

QString ColorizerModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment ColorizerModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

int ColorizerModel::mdDataRole(ModelType mt)
{
    switch (mt) {
    case ColorizerModel::FgColor:   [[fallthrough]];
    case ColorizerModel::BgColor:   return Qt::BackgroundRole;
    case ColorizerModel::Icon:      return Qt::DecorationRole;
    default:                        return Util::RawDataRole;
    }
}

void ColorizerModel::updateCache() const
{
    if (!m_dirty)
        return;

    m_cache.clear();
    m_canHideText.clear();
    m_lastMatchIdx  = QModelIndex();
    m_lastMatchItem = nullptr;

    if (m_colorizeModel == nullptr)
        return;

    m_cache.resize(m_colorizeModel->columnCount());
    m_canHideText.resize(m_colorizeModel->columnCount());
    m_canHideText.fill(false); // redundant, but here for clarity.

    Util::recurse(*this, [this](const QModelIndex& idx) {
        const ColorizerItem* item = getItem(idx);
        m_cache[item->m_column].append(item);
        m_canHideText[item->m_column] |= item->m_hideText;
    });

    m_dirty = false;
}

template <> Query::Context& ColorizerModel::queryCtx(const ColorizerItem&)
{
    return m_queryCtx;
}

class ColorizerEditor;
template <> Query::Context& ColorizerModel::queryCtx(const ColorizerEditor&)
{
    return m_queryCtx;
}

ColorizerModel& ColorizerModel::operator=(const ColorizerModel &rhs)
{
    TreeModel::operator=(rhs);
    m_dirty = true;
    m_colorizeModel = rhs.m_colorizeModel;

    updateCache();
    return *this;
}

void ColorizerModel::copyItem(const QModelIndex& dstIdx, const TreeModel& srcModel, const QModelIndex& srcIdx)
{
    TreeModel::copyItem(dstIdx, srcModel, srcIdx);

    ColorizerItem* dst       = getItem(dstIdx);
    const ColorizerItem* src = static_cast<const ColorizerModel&>(srcModel).getItem(srcIdx);

    if (dst == src) // short circuit self-copy
        return;

    *dst = *src;
}

void ColorizerModel::load(QSettings& settings)
{
    TreeModel::load(settings);

    m_dirty = true;
    updateCache();
}
