/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <src/util/roles.h>
#include <src/core/cfgdatabase.h>
#include "colorizermodel.h"
#include "colorizeritem.h"

ColorizerItem::ColorizerItem(ColorizerModel& model, const ItemData& data) :
    TreeItem(nullptr),
    m_column(ModelType(-1)),
    m_matchCase(false),
    m_hideText(false),
    m_queryRoot(new Query::All()),
    m_model(model)
{
    bool changed;
    for (int col = 0; col < data.size(); ++col)
        setData(col, data.at(col), Util::RawDataRole, changed);
}

ColorizerItem::ColorizerItem(ColorizerModel& model, const ColorizerItem& rhs) :
    TreeItem(nullptr),
    m_model(model)
{
    *this = rhs;
}

ColorizerItem::ColorizerItem(const ColorizerItem& rhs) :
    TreeItem(nullptr),
    m_model(rhs.m_model)
{
    *this = rhs;
}

void ColorizerItem::clear()
{
    for (int col = 0; col < ColorizerModel::_Count; ++col)
        clearData(col);
}

int ColorizerItem::columnCount() const
{
    return ColorizerModel::_Count;
}

QVariant ColorizerItem::data(const CfgDataBase& cfgData, ModelType mt, int role) const
{
    switch (role) {
    case Util::RawDataRole:
        switch (mt) {
        case ColorizerModel::Column: return m_column;
        default:                     break;
        }
        [[fallthrough]];
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch (mt) {
        case ColorizerModel::Column:
            if (m_column < 0 || m_model.colorizedModel() == nullptr)
                return QVariant();
            else
                return m_model.colorizedModel()->headerData(m_column, Qt::Horizontal);
        case ColorizerModel::Query:     return m_query;
        default:                        break;
        }
        break;

    case Qt::BackgroundRole:
        switch (mt) {
        case ColorizerModel::FgColor:   if (m_fgColor.isValid()) return m_fgColor; break;
        case ColorizerModel::BgColor:   if (m_bgColor.isValid()) return m_bgColor; break;
        default:                        break;
        }
        break;

    case Qt::CheckStateRole:
        switch (mt) {
        case ColorizerModel::MatchCase: return m_matchCase ? Qt::Checked : Qt::Unchecked;
        case ColorizerModel::HideText:  return m_hideText  ? Qt::Checked : Qt::Unchecked;
        default:                        break;
        }
        break;

    case Qt::DecorationRole:
        switch (mt) {
        case ColorizerModel::Icon:      return m_icon;
        case ColorizerModel::Query:     return m_query.isEmpty()      ? cfgData.filterEmptyIcon :
                                               m_queryRoot->isValid() ? cfgData.filterValidIcon :
                                                                        cfgData.filterInvalidIcon;

        default:                        break;
        }
        break;
    }

    return QVariant();
}

bool ColorizerItem::setData(ModelType mt, const QVariant& value, int role, bool& changed)
{
    changed = true; // TODO: we could be smarter about this.

    switch (role) {
    case Qt::DisplayRole:
    case Util::RawDataRole:
    case Util::IconNameRole:
    case Qt::EditRole:
    case Qt::BackgroundRole:
    case Qt::CheckStateRole:
        switch (mt) {
        case ColorizerModel::Column:
            if (value.type() == QVariant::Int)
                m_column = value.toInt();
            else if (m_model.colorizedModel() != nullptr)
                m_column = m_model.colorizedModel()->findHeaderColumn(value.toString());
            break;

        case ColorizerModel::Query:     updateQuery(value.toString());       break;
        case ColorizerModel::Icon:      setIcon(ColorizerModel::Icon, value.toString()); break;
        case ColorizerModel::FgColor:   m_fgColor   = value.value<QColor>(); break;
        case ColorizerModel::BgColor:   m_bgColor   = value.value<QColor>(); break;
        case ColorizerModel::MatchCase: m_matchCase = (value.toInt() == Qt::Checked); updateQuery(); break;
        case ColorizerModel::HideText:  m_hideText  = (value.toInt() == Qt::Checked); break;
        case ColorizerModel::_Count:
            assert(0 && "Unknown ColorizerModel value");
            return false;
        }
        return true;
    }

    return false;
}

bool ColorizerItem::setIcon(ModelType mt, const QString& iconFile)
{
    if (mt != ColorizerModel::Icon)
        return false;

    m_iconFile = iconFile;
    m_icon = iconFile.isEmpty() ? QIcon() : QIcon(m_iconFile);
    return true;
}

void ColorizerItem::clearData(ModelType mt)
{
    bool changed;

    switch (mt) {
    case ColorizerModel::Column:    setData(mt, -1, Util::RawDataRole, changed); break;
    case ColorizerModel::Query:     [[fallthrough]];
    case ColorizerModel::Icon:      setData(mt, "", Util::RawDataRole, changed); break;
    case ColorizerModel::FgColor:   [[fallthrough]];
    case ColorizerModel::BgColor:   setData(mt, QColor(), Util::RawDataRole, changed); break;
    case ColorizerModel::MatchCase: [[fallthrough]];
    case ColorizerModel::HideText:  setData(mt, false, Util::RawDataRole, changed); break;
    case ColorizerModel::_Count:    assert(0 && "Unknown ColorizerModel value"); break;
    }
}

void ColorizerItem::updateQuery(const QString& query)
{
    m_query = query;
    updateQuery();
}

void ColorizerItem::updateQuery()
{
    m_model.queryCtx(*this).setCaseSensitivity(m_matchCase ? Qt::CaseSensitive : Qt::CaseInsensitive);
    m_queryRoot.reset(m_model.queryCtx(*this).parse(m_query));
}

ColorizerItem& ColorizerItem::operator=(const ColorizerItem& rhs)
{
    m_column    = rhs.m_column;
    updateQuery(rhs.m_query);
    setIcon(ColorizerModel::Icon, rhs.m_iconFile);
    m_fgColor   = rhs.m_fgColor;
    m_bgColor   = rhs.m_bgColor;
    m_matchCase = rhs.m_matchCase;
    m_hideText  = rhs.m_hideText;

    return *this;
}

ColorizerItem* ColorizerItem::factory(const ColorizerItem::ItemData& data, [[maybe_unused]] TreeItem* parent)
{
    assert(dynamic_cast<ColorizerItem*>(parent) != nullptr);

    return new ColorizerItem(m_model, data);
}

void ColorizerItem::save(QSettings& settings) const
{
    TreeItem::save(settings);

    // m_icon restored from the filename
    MemberSave(settings, m_column);
    MemberSave(settings, m_query);
    MemberSave(settings, m_iconFile);
    MemberSave(settings, m_fgColor);
    MemberSave(settings, m_bgColor);
    MemberSave(settings, m_matchCase);
    MemberSave(settings, m_hideText);
}

void ColorizerItem::load(QSettings& settings)
{
    clear();

    TreeItem::load(settings);

    // m_icon restored from the filename
    MemberLoad(settings, m_column);
    MemberLoad(settings, m_query);
    MemberLoad(settings, m_iconFile);
    MemberLoad(settings, m_fgColor);
    MemberLoad(settings, m_bgColor);
    MemberLoad(settings, m_matchCase);
    MemberLoad(settings, m_hideText);

    setIcon(ColorizerModel::Icon, m_iconFile);
    updateQuery();
}
