/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PATTERN_H
#define PATTERN_H

#include <utility>
#include <tuple>
#include <QVector>
#include <QStringRef>
#include <QRegularExpression>

#include <src/util/roles.h>
#include <src/util/units.h>

class QAbstractItemModel;
class QModelIndex;

namespace Query {

class Context;

/*******************************************************************************
Pattern BNF:

<pattern> ::=
   <pattern> "|" <pattern> ...   |   ; match any
   <pattern> "&" <pattern> ...   |   ; match all
   <pattern> "," <pattern> ...   |   ; sequence of matches in increasing columns
   "!" <pattern>                 |   ; match negation
   <column> <cmp> <value> <unit> |   ; relational or regex comparison in given column
   <regex> <unit>                    ; PERL regular expression

<column> ::= "*"   |                 ; match in any column
             Name  |                 ; match in given column by name

<regex> ::= Perl Regular Expression from https://perldoc.perl.org/perlre.html

<cmp> ::= "<" | "<=" | ">" | ">=" | "==" | "!=" | "~="

<unit> ::= "" |
           SuffixName                ; unit suffix

Examples:
  Elevation > 1500
  Name : [A-M].*
  foo | bar
  foo & bar & blee
  col1data, col3data, col7data
  Name : foo | bar

*******************************************************************************/

// Single comparison, supports regexps or string/numeric relationals
class Base {
public:
    static const constexpr int AnyColumn = -1;
    static const constexpr int NoColumn = -2;

    virtual ~Base() { }

    virtual bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const = 0;
    virtual bool match(const QVariant&) const = 0;
    virtual void toString(const Context&, QString&) const = 0;
    virtual bool operator==(const Base& rhs) const = 0;
    virtual bool operator!=(const Base& rhs) const { return !(operator==(rhs)); }
    virtual bool isValid() const;

    inline bool matchIdx(const QAbstractItemModel& model, const QModelIndex& idx, int col = NoColumn) const {
        return match(model, model.parent(idx), idx.row(), col);
    }
};

// Error pattern: matches nothing
class None final : public Base {
public:
    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    bool operator==(const Base& rhs) const override;
    bool isValid() const override { return false; }
};

// Matches everything
class All final : public Base {
public:
    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    bool operator==(const Base& rhs) const override;
};

// Single relational comparison node
class Rel final : public Base {
public:
    enum class Cmp {
        EQ,      // ==
        NE,      // !=
        LT,      // <
        LE,      // <=
        GT,      // >
        GE,      // >=
        Regex,   // =~
        NoRegex, // !~
        None, // no comparison
    };

    // Create one, but return nullptr if it was an invalid pattern.
    static const Rel* make(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
                           Format format);

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    bool operator==(const Base& rhs) const override;
    bool isValid() const override;
    bool isRegex() const { return isRegex(cmpType); }

    // True if this is a regex comparison
    static bool isRegex(Cmp cmp) { return cmp == Cmp::Regex || cmp == Cmp::NoRegex; }

    static QString cmpToString(Cmp);
    static const QVector<Cmp>& comparisons();

private:
    Rel(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
        Format format) :
        context(ctx),
        pattern(pattern),
        cmpType(cmpType),
        caseSensitivity(caseSensitivity),
        column(column),
        role(role),
        format(format)
    {
        if (isRegex()) {
            regex.setPattern(pattern.toString());
            regex.setPatternOptions(caseSensitivity == Qt::CaseInsensitive ?
                                        QRegularExpression::CaseInsensitiveOption :
                                        QRegularExpression::NoPatternOption);
            regex.optimize();
        }
    }

    const Context&      context;
    QVariant            pattern;
    QRegularExpression  regex;
    Cmp                 cmpType;
    Qt::CaseSensitivity caseSensitivity;
    int                 column;
    int                 role;      // data role
    Format              format;    // unit format, for toString()
};

class Neg final : public Base {
public:
    Neg(const Base* node) :
        node(node)
    { }

    ~Neg() override { delete node; }

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    bool operator==(const Base& rhs) const override;

private:
    const Base* node;
};

class Seq final : public Base {
public:
    // These are in precision order.
    enum class Type {
        And,
        Xor,
        Or,
        Seq,  // sequence, N matches in non-monotonic increasing column order
        None,
    };

    Seq(Type cmpType, const Base* lhs = nullptr, const Base* rhs = nullptr) :
        cmpType(cmpType)
    {
        append(lhs);
        append(rhs);
    }

    Seq* append(const Base* node) {
        if (node != nullptr)
            cmpNode.append(node);
        return this;
    }

    ~Seq() override { qDeleteAll(cmpNode); }

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    bool operator==(const Base& rhs) const override;

private:
    QVector<const Base*> cmpNode;
    Type                 cmpType;
};


// Context data for parsing query language.  This can be set up once for a model and stored,
// to avoid some processing for each and every query parse.
class Context {
public:
    Context(int regexRole = Util::CopyRole,
            int relRole = Util::RawDataRole,
            int strRelRole = Util::CopyRole) :
        regexRole(regexRole),
        relRole(relRole),
        strRelRole(strRelRole)
    { }

    Context(const QAbstractItemModel* model, Qt::CaseSensitivity cs = Qt::CaseInsensitive,
            int regexRole = Util::CopyRole,
            int relRole = Util::RawDataRole,
            int strRelRole = Util::CopyRole) :
        Context(regexRole, relRole, strRelRole)
    { 
        setModel(model, cs);
    }

    const Base* parse(const QString& query) const;
    bool isValidQuery(const QString& query) const;
    QString toString(const Base*) const;

    // This can't be in the constructor: it's done during model setting
    void setModel(const QAbstractItemModel* model, Qt::CaseSensitivity cs);
    void setCaseSensitivity(Qt::CaseSensitivity cs) { caseSensitivity = cs; }

    static Rel::Cmp parseCompare(const QStringRef& token);
    int parseColumnName(const QStringRef& token) const;
    const QVector<QString>& headerNames() const { return headers; }
    const Units* units(int col) const { return columnUnits.at(col); }

private:
    enum class Status {
        Ok,
        Unrecognized,
        Error,
    };

    friend class Rel;
    friend class Regex;

    const Base* parseColumn() const;
    const Base* parseNeg() const;
    const Base* parseSeq(Seq::Type prec = Seq::Type::None, const Base* rhs = nullptr,
                         Seq::Type* nextType = nullptr) const;
    const Base* parseRegex() const;
    const Base* parsePattern() const;
    const Base* parseParen() const;

    auto parseUnitSuffix(int col) const;
    static Seq::Type parseSeqType(const QStringRef& token);
    inline QStringRef eat() const;
    inline QStringRef eat(int len) const;
    QStringRef nextToken() const;
    QStringRef token() const { return QStringRef(text.string(), tokenPos, tokenLen); }
    const Base* unwind(const QStringRef&, bool hasError) const;

    static std::pair<int, int> colBounds(int colCount, int column, int colOverride = Base::NoColumn);

    int                   columnCount;
    QMap<QString, int>    headerMap;
    QVector<QString>      headers;
    Qt::CaseSensitivity   caseSensitivity;
    QVector<const Units*> columnUnits;

    int                   regexRole;  // role for regex compares
    int                   relRole;    // role for numeric and date relative compares
    int                   strRelRole; // role for string relative compares

    mutable QStringRef    text;     // parse text
    mutable int           tokenPos; // end of next token
    mutable int           tokenLen; // current token length
    mutable bool          error;    // parse error
};

} // namespace Query

#endif // PATTERN_H
