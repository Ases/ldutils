/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>

#include <src/util/util.h>
#include <src/util/varianthash.h>

#include "contentaddrmodel.h"

ContentAddrModel::ContentAddrModel(TreeItem* root, int keyColumn, int keyRole, QObject *parent) :
    TreeModel(root, parent),
    m_keyColumn(keyColumn),
    m_keyRole(keyRole)
{
    setupSignals();
}

ContentAddrModel::~ContentAddrModel()
{
}

ContentAddrModel& ContentAddrModel::operator=(const ContentAddrModel& other)
{
    contentCache.clear();
    m_keyColumn  = other.m_keyColumn;
    m_keyRole    = other.m_keyRole;

    // We must do this after the assignments and cache clearing above, to get proper behavior
    // during the node copies.
    return static_cast<ContentAddrModel&>(TreeModel::operator=(other));
}

// Signals we react to for updating our cache.
void ContentAddrModel::setupSignals()
{
    connect(this, &ContentAddrModel::rowsAboutToBeRemoved, this, &ContentAddrModel::handleRowsAboutToBeRemoved);
    connect(this, &ContentAddrModel::rowsInserted, this, &ContentAddrModel::handleRowsInserted);
    connect(this, &ContentAddrModel::modelReset, this, &ContentAddrModel::handleModelReset);
}

QString ContentAddrModel::uniqueName(const QVariant& name) const
{
    int suffix = 0;

    if (name.type() != QVariant::String) // we only auto-modify strings.
        return QString();

    QString oldName = name.value<QString>();

    for (int pos = oldName.length() - 1; pos > 1 && suffix == 0; --pos) {
        if (oldName[pos] == '_') {
            suffix = oldName.mid(pos+1).toInt();
            oldName = oldName.mid(0, pos);
        }
    }

    for (suffix = std::max(suffix, 1); suffix < 1024; ++suffix) {
        const QString uniqueValue = oldName + "_" + QString::number(suffix);
        if (const auto it = contentCache.find(uniqueValue); it == contentCache.end())
            return uniqueValue;
    }

    return QString();
}

// Content cache based fetch: our whole reason to exist.
QVariant ContentAddrModel::value(const QVariant& key, int column, int role) const
{
    // If it's in the cache, use it.
    if (const auto it = contentCache.find(key); it != contentCache.end())
        return getItem(it.value())->data(column, role);

    // Didn't find it.
    return QVariant();
}

QModelIndex ContentAddrModel::keyIdx(const QVariant& key) const
{
    if (const auto it = contentCache.find(key); it != contentCache.end())
        return it.value();

    return QModelIndex();
}

bool ContentAddrModel::contains(const QVariant& key) const
{
    return contentCache.contains(key);
}

bool ContentAddrModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    if (idx.column() != m_keyColumn)
        return TreeModel::setData(idx, value, role);

    contentCache.remove(data(idx, role));

    const auto setNew = [this](const QModelIndex& idx, const QVariant& v, int role) {
        contentCache.insert(v, idx);
        return TreeModel::setData(idx, v, role);
    };

    // If the value isn't in the content cache, insert it, and set the model.
    if (const auto it = contentCache.find(value); it == contentCache.end())
        return setNew(idx, value, role);

    // Othewise uniquify the new value if possible.
    if (const QString newName = uniqueName(value); !newName.isEmpty())
        return setNew(idx, QVariant(newName), role);

    // If we get here, all attempts have failed.  Put old value back in cache and give up.
    contentCache.insert(value, idx);
    return false;
}

void ContentAddrModel::copyItem(const QModelIndex& dstIdx,
                                const TreeModel& srcModel, const QModelIndex& srcIdx)
{
    if (dstIdx == srcIdx && dstIdx.isValid()) // short circuit self-copy
        return;

    if (&srcModel == this) {
        // Intra-model copy
        // Fix content cache after copy, else we'd have two items with the same name.

        TreeModel::copyItem(dstIdx, srcModel, srcIdx);

        const QVariant value = data(m_keyColumn, srcIdx, m_keyRole);

        // Rename the source, to avoid conflict with the dst's copy of the value.
        if (const QString newName = uniqueName(value); !newName.isEmpty())
            setData(m_keyColumn, srcIdx, newName, m_keyRole);

        contentCache.insert(value, dstIdx);   // update cache to point to new.
    } else {
        const QVariant oldValue = data(m_keyColumn, dstIdx, m_keyRole);
        contentCache.remove(oldValue);

        TreeModel::copyItem(dstIdx, srcModel, srcIdx);
        const QVariant newValue = srcModel.data(m_keyColumn, srcIdx, m_keyRole);
        setData(dstIdx, newValue, m_keyRole);
    }
}

void ContentAddrModel::handleRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    for (int row = first; row <= last; ++row)
        contentCache.remove(data(index(row, m_keyColumn, parent), m_keyRole));
}

void ContentAddrModel::handleRowsInserted(const QModelIndex& parent, int first, int last)
{
    for (int row = first; row <= last; ++row) {
        const QModelIndex idx = index(row, m_keyColumn, parent);
        const QVariant value  = data(idx, m_keyRole);
         if (const auto it = contentCache.find(value); it == contentCache.end()) {
             // Not already in cache.
             contentCache.insert(data(idx, m_keyRole), idx);
         } else {
             // Already in cache.
             if (const QString newName = uniqueName(value); !newName.isEmpty()) {
                 contentCache.insert(newName, idx);
                 TreeModel::setData(idx, newName, m_keyRole);
             } else {
                 // TODO: ...
             }
         }
    }
}

void ContentAddrModel::handleModelReset()
{
    contentCache.clear();

    // Repopulate cache after a model reset.
    Util::recurse(*this, [this](const QModelIndex& idx) {
        contentCache.insert(data(m_keyColumn, idx, m_keyRole), idx);
    });
}
