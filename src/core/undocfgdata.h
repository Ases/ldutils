/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOCFGDATA_H
#define UNDOCFGDATA_H

#include <QByteArray>

#include <src/core/undobase.h>
#include <src/core/undomgr.h>

class MainWindowBase;
class CfgDataBase;

class UndoCfgData final : public UndoBase
{
public:
    UndoCfgData(MainWindowBase&, const QByteArray& before, const QByteArray& after);

    // Our own version of a scoped undo
    class ScopedUndo : public UndoMgr::ScopedUndo {
    public:
        ScopedUndo(MainWindowBase& mainWindow, UndoMgr& undoMgr, const QString& name,
                   const CfgDataBase& before, const CfgDataBase& after);

        ~ScopedUndo();
        bool hasDiffs() const { return m_beforeZ != m_afterZ; }
    private:
        MainWindowBase&  m_mainWindow;
        const QByteArray m_beforeZ;
        const QByteArray m_afterZ;
    };

protected:
    using UndoBase::size;

    bool undo() const override { return apply(m_beforeZ); }
    bool redo() const override { return apply(m_afterZ); }
    size_t size() const override;
    const char* className() const override { return "UndoCfgData"; }

private:
    static QByteArray read(const CfgDataBase&);
    bool apply(const QByteArray& stateZ) const;

    QByteArray      m_beforeZ;  // compressed byte array of configuration data
    QByteArray      m_afterZ;   // ...
    MainWindowBase& m_mainWindow;
};

#endif // UNDOCFGDATA_H
