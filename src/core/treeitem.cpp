/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <type_traits>
#include <QStringList>

#include <src/util/util.h>
#include "treemodel.h"
#include "treeitem.h"

TreeItem::TreeItem(const TreeItem::ItemData& data, TreeItem *parent, int role) :
    parentItem(parent)
{
    itemData[role] = data;
}

TreeItem::TreeItem(TreeItem *parent) :
    parentItem(parent)
{
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

TreeItem *TreeItem::child(int number)
{
    return childItems.value(number);
}

const TreeItem *TreeItem::child(int number) const
{
    return childItems.value(number);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::childNumber() const
{
    if (parentItem != nullptr)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

int TreeItem::columnCount() const
{
    int cc = 0;
    for (const auto& it : itemData)
        cc = std::max(cc, it.size());

    return cc;
}

QVariant TreeItem::data(int column, int role) const
{
    const auto it = itemData.find(role);

    if (it == itemData.end()) // role not present
        return QVariant();

    if (column >= it->size())
        return QVariant();

    return it->value(column);
}

bool TreeItem::insertChildren(int position, int count, int columns)
{
    const ItemData data(columns);
    return insertChildren(position, count, data);
}

bool TreeItem::insertChildren(int position, int count, const TreeItem::ItemData& data)
{
    if (position < 0 || position > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        TreeItem *item = factory(data, this);
        childItems.insert(position, item);
    }

    return true;
}

bool TreeItem::insertChildren(int position, const QVector<TreeItem::ItemData>& data)
{
    int itemNo = 0;
    for (const auto& item : data)
        if (!insertChildren(position + itemNo++, 1, item))
            return false;

    return true;
}

bool TreeItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > columnCount())
        return false;

    for (auto& roleData : itemData)
        for (int column = 0; column < columns; ++column)
            roleData.insert(position, QVariant());

    for (auto* child : childItems)
        child->insertColumns(position, columns);

    return true;
}

TreeItem *TreeItem::parent() const
{
    return parentItem;
}

bool TreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

bool TreeItem::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > columnCount())
        return false;

    for (int column = 0; column < columns; ++column)
        for (auto& roleData : itemData)
            roleData.remove(position);

    foreach (TreeItem *child, childItems)
        child->removeColumns(position, columns);

    return true;
}

bool TreeItem::setData(int column, const QVariant &value, int role, bool& changed)
{
    changed = false;

    if (column < 0)
        return false;

    auto it = itemData.find(role);

    if (it == itemData.end())
        it = itemData.insert(role, ItemData());

    if (column >= it->size())
        it->resize(column+1);

    // Better not to update identical data, to avoid data change events
    if ((*it)[column] != value) {
        (*it)[column] = value;
        changed = true;
    }

    return true;
}

bool TreeItem::setData(int column, const QVariant &value, int role)
{
    bool changed;
    return setData(column, value, role, changed);
}

TreeItem *TreeItem::factory(const TreeItem::ItemData& data, TreeItem *parent)
{
    return new TreeItem(data, parent);
}

// copy data, but not structure
void TreeItem::shallowCopy(const TreeItem* src)
{
    itemData = src->itemData;
}

bool TreeItem::moveRow(int srcRow, TreeItem *dstParent, int dstRow)
{
    if (dstParent == nullptr || srcRow < 0 || dstRow < 0 ||
        srcRow >= childCount() || dstRow >= (dstParent->childCount()+1))
        return false;

    if (dstParent == this) {
        if (srcRow == dstRow)  // identity move
            return true;

        // See note in docs for QAbstractItemModel::beginMoveRows about self-moves downward!
        // We'll be removing the source first, so if it's before the dest in the
        // same parent, our insertion point has moved up a slot.
        if (srcRow < dstRow)
            --dstRow;
    }

    dstParent->childItems.insert(dstRow, childItems.takeAt(srcRow));

    return true;
}

// Apply a new ordering given as a list of integers
void TreeItem::applyOrdering(const QVector<int>& newOrdering)
{
    decltype(childItems) reorderedChildren(childItems.size(), nullptr);

    for (int i = 0; i < newOrdering.size(); ++i)
        reorderedChildren[i] = childItems.at(newOrdering.at(i));

    // There must be no nullptrs in the resulting vector.  If any are, abort!
    if (std::any_of(reorderedChildren.begin(), reorderedChildren.end(),
                    [](const TreeItem* item) { return item == nullptr; })) {

        assert(0 && "Bad input ordering to TreeItem::applyOrdering");
        return;  // return without applying
    }

    childItems.swap(reorderedChildren);
}

void TreeItem::save(QSettings& settings) const
{
    settings.beginWriteArray("itemData"); {
        for (auto it = itemData.cbegin(); it != itemData.cend(); ++it) {
            if (saveRole(it.key()) && !it.value().empty()) { // only save if saveRole() says to.
                settings.setArrayIndex(it.key());
                SL::Save(settings, "data", it.value());
            }
        }
    } settings.endArray();

    settings.beginWriteArray("child"); {
        for (int i = 0; i < childItems.size(); ++i) {
            settings.setArrayIndex(i);
            child(i)->save(settings);
        }
    } settings.endArray();
}

Q_DECLARE_METATYPE(QVector<QVariant>)

// Fix up icons from icon names (e.g, to avoid storing cooked bitmap data for svgs).
void TreeItem::createIcons()
{
    if (const auto it = itemData.find(Util::IconNameRole); it != itemData.end()) {
        auto bgit = itemData.insert(Qt::DecorationRole, ItemData(it->size()));

        for (int c = 0; c < it->size(); ++c) {
            auto& decorationName = (*it)[c];
            auto& decorationVar = (*bgit)[c];
            if (!decorationName.isNull()) {
                QIcon decorationIcon(decorationName.toString());
                const bool broken = (decorationIcon.actualSize(QSize(16, 16)).width() <= 2);

                decorationVar = broken ? TreeModel::brokenIcon : decorationIcon;
            }
        }
    }
}

void TreeItem::load(QSettings& settings)
{
    itemData.clear();
    removeChildren(0, childCount());

    // TODO: make this more robust against column re-orgs by using the column headers
    // to discover the remapping, if any.
    const int count = settings.beginReadArray("itemData"); {
        for (int i = 0; i < count; ++i) {
            settings.setArrayIndex(i);
            if (saveRole(i) && settings.contains("data/size")) {
                ItemData val;
                SL::Load(settings, "data", val);
                itemData.insert(i, val);
            }
        }
    } settings.endArray();

    createIcons();

    const int childCount = settings.beginReadArray("child"); {
        for (int i = 0; i < childCount; ++i) {
            settings.setArrayIndex(i);
            insertChildren(i, 1, 0);
            child(i)->load(settings);
        }
    } settings.endArray();
}

QDataStream& TreeItem::save(QDataStream& stream, const TreeModel& model) const
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    // Data
    stream << guard << itemData;

    // Children:
    stream << childItems.size();
    for (const auto& ci : childItems)
        ci->save(stream, model);

    emit model.itemSaved(1);

    return stream;
}

QDataStream& TreeItem::load(QDataStream& stream, TreeModel& model)
{
    if (stream.status() != QDataStream::Ok)
        return stream;

    std::remove_const<decltype(guard)>::type readGuard;

    // Load data
    stream >> readGuard;
    if (readGuard != guard) {
        stream.setStatus(QDataStream::ReadCorruptData);
        return stream;
    }

    stream >> itemData;

    createIcons();

    // Load children
    int savedChildCount;
    stream >> savedChildCount;

    insertChildren(0, savedChildCount, 0);
    for (int i = 0; i < savedChildCount; ++i)
        child(i)->load(stream, model);

    if (stream.device() != nullptr)
        emit model.itemLoaded(stream.device()->pos());

    return stream;
}
