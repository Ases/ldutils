/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOBASE_H
#define UNDOBASE_H

#include <cstddef>
#include <functional>

#include <QByteArray>

class QSettings;
class QString;
class QStringList;
class QVariant;

// Base class for things which can be undone.
class UndoBase {
public:
    virtual ~UndoBase();
    virtual bool undo() const = 0;    // undo this change
    virtual bool redo() const = 0;    // redo this change
    virtual size_t size() const = 0;  // query total size in bytes for this change
    virtual const char* className() const = 0;

protected:
    // Estimate byte sizeo of various things.
    static size_t size(const QVariant&);
    static size_t size(const QString&);
    static size_t size(const QStringList&);
    static size_t size(const QByteArray&);
    template <typename T> static size_t size(const QVector<T>&);

    // Turn settings into compressed byte array
    static QByteArray readCfgZ(const std::function<void(QSettings&)>&);  // Return window config as a byte array
    // Apply settings from compressed byte array
    static bool applyCfgZ(const QByteArray& stateZ, const std::function<void(QSettings&)>&);
};

template <typename T> size_t UndoBase::size(const QVector<T>& v)
{
    return sizeof(v) + size_t(v.size()) * sizeof(T);
}

#endif // UNDOBASE_H
