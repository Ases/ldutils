/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "changetrackingmodel.h"
#include "undomodel.h"

#include "src/ui/windows/mainwindowbase.h"

UndoModel::UndoModel(ChangeTrackingModel& model) :
    m_mainWindow(model.undoMgr().mainWindow()),
    m_modelId(m_mainWindow.idForPersistentModel(model))
{
}

ChangeTrackingModel* UndoModel::findModel() const
{
    return m_mainWindow.persistentModelForId(m_modelId);
}

void UndoModel::saveData(QByteArray& dataZ, const QModelIndex& parent,
                         int start, int end) const
{
    if (!dataZ.isEmpty())
        return;

    QByteArray rawData;
    // We must save the removed data for restoration.
    QDataStream stream(&rawData, QIODevice::WriteOnly);

    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return;

    model->saveForUndo(stream, parent, start, end - start + 1);

    // store compressed form for space efficiency
    dataZ = qCompress(rawData, 9);
}

void UndoModel::restoreData(const QByteArray& dataZ, const QModelIndex& parent, int start) const
{
    QDataStream stream(qUncompress(dataZ));

    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return;

    model->loadForUndo(stream, parent, start);
}

UndoModelInsDel::UndoModelInsDel(ChangeTrackingModel& model, Mode mode,
                                 const QModelIndex& parent, int start, int end) :
    UndoModel(model), m_parent(Util::SaveIndex(parent)), m_start(start), m_end(end), m_mode(mode)
{
}

bool UndoModelInsDel::remove() const
{
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return false;

    ChangeTrackingModel::SignalBlocker block(*model);  // don't signal the undo

    const QModelIndex parent = Util::RestoreIndex(*model, m_parent);

    saveData(parent);

    if (m_mode == Mode::Row)
        return model->removeRows(m_start, m_end - m_start + 1, parent);
    else
        return model->removeColumns(m_start, m_end - m_start + 1, parent);
}

bool UndoModelInsDel::insert() const
{
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return false;

    ChangeTrackingModel::SignalBlocker block(*model);  // don't signal the redo

    const QModelIndex parent = Util::RestoreIndex(*model, m_parent);

    model->preUndoHook(parent, m_start, m_end);

    bool rc;
    if (m_mode == Mode::Row)
        rc = model->insertRows(m_start, m_end - m_start + 1, parent);
    else
        rc = model->insertColumns(m_start, m_end - m_start + 1, parent);

    restoreData(parent);
    model->postUndoHook(parent, m_start, m_end);

    return rc;
}

void UndoModelInsDel::saveData(const QModelIndex& parent) const
{
    saveData(m_savedDataZ, parent, m_start, m_end);
}

void UndoModelInsDel::restoreData(const QModelIndex& parent) const
{
    restoreData(m_savedDataZ, parent, m_start);
}

size_t UndoModelInsDel::size() const
{
    return sizeof(*this) + size(m_savedDataZ) + size(m_parent);
}

UndoModelInsert::UndoModelInsert(ChangeTrackingModel& model, Mode mode, const QModelIndex& parent, int start, int end) :
    UndoModelInsDel(model, mode, parent, start, end)
{
}

UndoModelRemove::UndoModelRemove(ChangeTrackingModel& model, Mode mode, const QModelIndex& parent, int start, int end) :
    UndoModelInsDel(model, mode, parent, start, end)
{
    saveData(parent);
}

UndoModelSetData::UndoModelSetData(ChangeTrackingModel& model, const QModelIndex& index, const QVariant& value, int role) :
    UndoModel(model),
    m_index(Util::SaveIndex(index)),
    m_before(model.data(index, role)),
    m_after(value),
    m_role(role)
{
}

bool UndoModelSetData::apply(const QVariant& data) const
{
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return false;

    ChangeTrackingModel::SignalBlocker block(*model);  // don't signal the undo

    const QModelIndex index = Util::RestoreIndex(*model, m_index);

    model->preUndoHook(index);
    const bool rc = model->setData(index, data, m_role);
    model->postUndoHook(index);

    return rc;
}

size_t UndoModelSetData::size() const
{
    return sizeof(*this) + size(m_index) + size(m_before) + size(m_after);
}

UndoModelData::UndoModelData(ChangeTrackingModel& model, const QModelIndex& parent,
                             int start, int end) :
    UndoModel(model), m_parent(Util::SaveIndex(parent)),
    m_start(start), m_end(end)
{
    // Save current data to apply on undo
    saveData(m_before, parent, m_start, m_end);
}

bool UndoModelData::apply(const QByteArray& dataZ) const
{
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return false;

    const QModelIndex parent = Util::RestoreIndex(*model, m_parent);

    // if we didn't already, save post-change data for redo
    saveData(m_after, parent, m_start, m_end);

    model->preUndoHook(parent, m_start, m_end);
    restoreData(dataZ, parent, m_start);
    model->postUndoHook(parent, m_start, m_end);

    return true;
}

size_t UndoModelData::size() const
{
    return sizeof(*this) + size(m_before) + size(m_after);
}
