/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/ui/panes/panebase.h>
#include <src/ui/panes/datacolumnpanebase.h>
#include <src/ui/windows/mainwindowbase.h>

#include "undopane.h"

UndoPaneBase::UndoPaneBase(PaneBase& pane) :
    m_mainWindow(pane.mainWindow()), m_paneId(pane.paneId())
{
}

UndoPaneBase::~UndoPaneBase()
{
}

template <typename T> T* UndoPaneBase::findPane() const
{
    return m_mainWindow.findPane<T>([this](T* p) { return p->paneId() == m_paneId; });
}

size_t UndoPaneState::size() const
{
    return sizeof(*this) + size(m_beforeZ) + size(m_afterZ);
}

QByteArray UndoPaneState::read(const PaneBase& pane)
{
    return readCfgZ([&pane](QSettings& settings) { pane.save(settings); });
}

bool UndoPaneState::apply(const QByteArray& stateZ) const
{
    if (PaneBase* pane = findPane<PaneBase>(); pane != nullptr) {
        return applyCfgZ(stateZ, [pane](QSettings& settings) { pane->load(settings); });
    }

    return false;
}

UndoPaneState::ScopedUndo::ScopedUndo(PaneBase& pane, const QString& name) :
    UndoMgr::ScopedUndo(pane.mainWindow().undoMgr(), name), m_pane(pane),
    m_beforeZ(nesting() == 1 ? read(pane) : QByteArray())
{
}

UndoPaneState::ScopedUndo::~ScopedUndo()
{
    if (nesting() == 1) {
        const QByteArray afterZ = read(m_pane);

        if (m_beforeZ != afterZ)
            m_undoMgr->add(new UndoPaneState(m_pane, m_beforeZ, afterZ));
    }
}

UndoPaneSectionMove::UndoPaneSectionMove(DataColumnPaneBase& pane, int from, int to) :
    UndoPaneBase(pane), m_from(from), m_to(to)
{
}

bool UndoPaneSectionMove::undo() const
{
    if (DataColumnPaneBase* pane = findPane<DataColumnPaneBase>(); pane != nullptr) {
        pane->moveSection(m_to, m_from);
        return true;
    }

    return false;
}

bool UndoPaneSectionMove::redo() const
{
    if (DataColumnPaneBase* pane = findPane<DataColumnPaneBase>(); pane != nullptr) {
        pane->moveSection(m_from, m_to);
        return true;
    }

    return false;
}

UndoPaneSetColumnHidden::UndoPaneSetColumnHidden(DataColumnPaneBase& pane, int column, bool hidden) :
    UndoPaneBase(pane), m_column(column), m_hidden(hidden)
{
}

bool UndoPaneSetColumnHidden::apply(bool hidden) const
{
    if (DataColumnPaneBase* pane = findPane<DataColumnPaneBase>(); pane != nullptr) {
        pane->setColumnHidden(m_column, hidden);
        return true;
    }

    return false;
}

UndoPaneSort::UndoPaneSort(DataColumnPaneBase& pane, int fromColumn, Qt::SortOrder fromOrder, int toColumn, Qt::SortOrder toOrder) :
    UndoPaneBase(pane), m_fromColumn(fromColumn), m_fromOrder(fromOrder),
    m_toColumn(toColumn), m_toOrder(toOrder)
{
}

bool UndoPaneSort::apply(int column, Qt::SortOrder order) const
{
    if (DataColumnPaneBase* pane = findPane<DataColumnPaneBase>(); pane != nullptr) {
        pane->setSort(column, order);
        return true;
    }

    return false;
}
