/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOMGR_H
#define UNDOMGR_H

#include <QObject>
#include <QList>
#include <QString>
#include <QSharedPointer>
#include <QMutex>
#include <QStack>
#include <QSet>

#include <tuple>

class QString;

class CfgDataBase;
class UndoBase;
class UndoableObject;
class MainWindowBase;

// Track undo/redo for models and maybe other things.
class UndoMgr final : public QObject
{   
    Q_OBJECT

public:
    enum class ModType { Modifying, NonModifying };

    // What to do in the event of new changes with a non-empty redo stack
    enum class MiddleMode {
        ClearRedo,    // clear the redos, and add new change ot undo stack
        XferAndSkip,  // transfer, but don't apply.  requires arbitrary order!
        XferAndApply, // apply all the pending redos
    };

    UndoMgr(MainWindowBase&, ModType modType = ModType::Modifying);
    UndoMgr(MainWindowBase&, int maxCount, size_t maxSize, ModType modType = ModType::Modifying);

    // scoped undo sets
    class ScopedUndo {
    public:
        // The enable flag is because we can't put one of these in its own if-scope, or
        // it will be popped off the stack before its intended use around the changes.
        ScopedUndo(UndoMgr* m, const QString& name, bool enable = true, ModType modType = ModType::Modifying) :
            m_undoMgr(m),
            m_enable(enable && m != nullptr), // only enable if we have a non-null UndoMgr
            m_modType(modType)
        {
            if (m_enable)
                m_undoMgr->beginUndo(name);
        }

        // Version from reference
        ScopedUndo(UndoMgr& m, const QString& name, bool enable = true, ModType modType = ModType::Modifying) :
            m_undoMgr(&m), m_enable(enable), m_modType(modType)
        {
            if (m_enable)
                m_undoMgr->beginUndo(name);
        }

        ~ScopedUndo() {
            if (m_enable)
                m_undoMgr->endUndo(m_modType);
        }

        bool enable() const { return m_enable; }
        int nesting() const { return (m_undoMgr != nullptr) ? m_undoMgr->m_nesting : -1; }
    protected:
        UndoMgr* m_undoMgr;
        bool     m_enable;
        ModType  m_modType;
    };

    void beginUndo(const QString& name); // start named undo set
    void add(const UndoBase*);           // add an undo to the pending set
    void endUndo(ModType modType);       // finish the undo set

    void setLimits(int maxCount, size_t maxSize);  // set limits for this manager
    void setLimits(const CfgDataBase&); // set limits from config data

    bool hasUndos()  const { return !m_undoStack.isEmpty(); }
    bool hasRedos()  const { return !m_redoStack.isEmpty(); }
    int  undoCount() const { return m_undoStack.size(); }
    int  redoCount() const { return m_redoStack.size(); }

    const QString& topUndoName() const;
    const QString& topRedoName() const;

    bool undo(); // undo from the top of the undo stack
    bool redo(); // redo from the redo stack

    // Build up a friendly name for undo entries.
    static QString genName(const QString& verb, int count, const QString& singular, const QString& plural);
    static QString genName(const QString& verb, int count, const std::tuple<const QString, const QString>&);
    static QString genNameX(const QString& verb, int count);

    // The first parameter is just a layer of protection so that only UndoableObjects call this.
    void emitDirtyStateChanged(bool dirty);  // for use by UndoableObjects

    // Nonsignalling: set all object's dirty state at once to a known state, e.g, on save.
    void setDirty(bool dirty);
    bool isDirty() const { return m_dirty; }

    // True if we've gotten back to the last save point.
    void markSavePoint();
    bool atSavePoint() const;

    void newConfig(const CfgDataBase&); // config changed

    // Set what to do in the event of undos with a non-empty redo stack
    void setMiddleMode(MiddleMode m) { m_middleMode = m; }

    // Reset it all, e.g, on session load.
    void clear();

    MainWindowBase& mainWindow() const { return m_mainWindow; }

signals:
    void dirtyStateChanged(bool); // dirt->clean or clean->dirty
    void undoAdded();             // sent when any undo is added to the manager
    void changeApplied();         // undo or redo applied
    void changeFailed();          // there was a problem.

private:
    friend class UndoableObject;
    friend class ScopedUndo;

    typedef QSharedPointer<const UndoBase> UndoRef;

    // A single undo set, potentially of multiple undos we'll apply as a named unit.  ScopedUndo
    // above can be nested to expose make-sense groupings of undos.
    class UndoSet {
    public:
        UndoSet() : m_name("no-op"), m_cachedSize(0) { }
        UndoSet(const QString& name) : m_name(name), m_cachedSize(0) { }

        void add(const UndoBase* undo); // add a new change into this set
        bool undo();                    // undo everything in this set.
        bool redo();                    // redo everything in this set.
        size_t size() const;            // estimate size of this set of undos

        bool isEmpty() const { return m_undos.isEmpty(); }

        const QString& name() const { return m_name; }
    private:
        QString         m_name;
        QList<UndoRef>  m_undos;
        mutable size_t  m_cachedSize; // cache size for efficiency
    };

    class UndoStack : public QStack<UndoSet> {
    public:
        size_t sizeEstimate() const;
    };

    // Apply: used internally by undo() and redo()
    bool apply(UndoStack& to, UndoStack& from, int direction);

    // Limit stacks to max size allowed
    void limitStacks();

    // For tracking all the undoable objects
    void registerUndoableObject(UndoableObject* obj);
    void unregisterUndoableObject(UndoableObject* obj);

    mutable QMutex        m_mutex;          // for thread safety
    MainWindowBase&       m_mainWindow;     // to find items from persistent IDs
    QSet<UndoableObject*> m_undoables;      // collect the whole set
    UndoStack             m_undoStack;      // stack of undo sets
    UndoStack             m_redoStack;      // stack of redo sets
    ModType               m_modType;        // modifying managers can send dirty signals
    MiddleMode            m_middleMode;     // mode for adding changes with non-empty redo stack
    int                   m_nesting;        // nesting level: only outer-most endUndo registers one.
    int                   m_unmodifiedBegin;// ID of last save point
    int                   m_unmodifiedEnd;  // end of unmodified IDs (e.g, view-only changes)
    int                   m_currentId;      // current ID state: if == m_unmodifiedId, we're at save point.
    int                   m_maxCount;       // max size of undo/redo stacks
    size_t                m_maxSize;        // max accumulated size of undo/redos
    bool                  m_dirty;          // master dirty flag
};

#endif // UNDOMGR_H
