/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOMODEL_H
#define UNDOMODEL_H

#include <QModelIndex>
#include <QVariant>

#include <src/util/util.h>
#include "undobase.h"

class MainWindowBase;
class ChangeTrackingModel;

// Base class for all model undos
class UndoModel : public UndoBase
{
protected:
    UndoModel(ChangeTrackingModel&);

    ChangeTrackingModel* findModel() const;

    MainWindowBase& m_mainWindow;
    int             m_modelId;

    // Save compressed data to byte arrays
    void saveData(QByteArray&, const QModelIndex& parent, int start, int end) const;
    void restoreData(const QByteArray&, const QModelIndex& parent, int start) const;
};

// Common baseclass for insertion/deletion of rows
class UndoModelInsDel : public UndoModel
{
public:
    enum class Mode { Row, Col };

    UndoModelInsDel(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    bool insert() const;
    bool remove() const;
    using UndoModel::saveData;
    using UndoModel::restoreData;
    void saveData(const QModelIndex& parent) const;
    void restoreData(const QModelIndex& parent) const;

    using UndoModel::size;
    size_t size() const override;

private:
    Util::SavableIndex m_parent;
    int                m_start;
    int                m_end;
    Mode               m_mode;
    mutable QByteArray m_savedDataZ; // compressed pre-removal data
};

// Undo for model row removal: adapt insertion and swap undo/redo.
class UndoModelRemove final : public UndoModelInsDel
{
public:
    UndoModelRemove(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    // Removal is almost the reverse of insertion.
    bool undo() const override { return insert(); }
    bool redo() const override { return remove(); }
    const char* className() const override { return "UndoModelRemove"; }
};

// Undo for model row removal: adapt insertion and swap undo/redo.
class UndoModelInsert final : public UndoModelInsDel
{
public:
    UndoModelInsert(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    // Insertion is almost the reverse of removal.
    bool undo() const override { return remove(); }
    bool redo() const override { return insert(); }
    const char* className() const override { return "UndoModelInsert"; }
};

// Undo for changing a given model index
class UndoModelSetData final : public UndoModel
{
public:
    UndoModelSetData(ChangeTrackingModel&, const QModelIndex&, const QVariant& value, int role);

protected:
    bool apply(const QVariant&) const;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }
    size_t size() const override;
    const char* className() const override { return "UndoModelSetData"; }

    using UndoModel::size;

private:
    Util::SavableIndex m_index;
    QVariant           m_before;
    QVariant           m_after;
    int                m_role;
};

// Undo that saves and restores model data for extra-model-API changes.
class UndoModelData final : public UndoModel
{
public:
    UndoModelData(ChangeTrackingModel&, const QModelIndex&, int start, int end);

protected:
    bool apply(const QByteArray&) const;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }

    using UndoModel::size;
    size_t size() const override;
    const char* className() const override { return "UndoModelData"; }

private:
    Util::SavableIndex m_parent; // parent of all
    int                m_start;
    int                m_end;
    mutable QByteArray m_before; // compressed byte array of model data
    mutable QByteArray m_after;  // ...
};

#endif // UNDOMODEL_H
