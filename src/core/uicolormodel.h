/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UICOLORMODEL_H
#define UICOLORMODEL_H

#include <src/core/colorlistmodel.h>

// UI messages
enum class UiType {
    _Begin,
    Normal = _Begin,
    Info,
    Success,
    Warning,
    Error,
    Critical,
    Cmdline,
    Stdout,
    Stderr,
    _Count,
};

class UiColorModel final : public ColorListModel
{
public:
    UiColorModel(QObject *parent = nullptr);

    QColor operator[](UiType row) const { return ColorListModel::operator[](int(row)); }

private:
    void addMissing() override;

    static QString name(UiType ml);   // name of level
    static QColor  color(UiType ml);  // default color
};

#endif // UICOLORMODEL_H
