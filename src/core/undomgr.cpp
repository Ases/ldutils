/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <numeric>

#include "src/ui/windows/mainwindowbase.h"
#include "src/util/reverseadapter.h"
#include "cfgdatabase.h"
#include "undomgr.h"
#include "undobase.h"
#include "undoableobject.h"

namespace {
    static const QString nullStr;
}

UndoMgr::UndoMgr(MainWindowBase& mainWindow, ModType modType) :
    UndoMgr(mainWindow, 25, 8L<<20, modType)
{ }

UndoMgr::UndoMgr(MainWindowBase& mainWindow, int maxCount, size_t maxSize, UndoMgr::ModType modType) :
    m_mutex(QMutex::Recursive),
    m_mainWindow(mainWindow),
    m_modType(modType),
    m_middleMode(MiddleMode::ClearRedo),
    m_dirty(false)
{
    clear();
    setLimits(maxCount, maxSize);
}

void UndoMgr::beginUndo(const QString& name)
{
    QMutexLocker lock(&m_mutex);
    if (m_nesting++ == 0)
        m_undoStack.append(UndoSet(name));
}

void UndoMgr::add(const UndoBase* undo)
{
    if (undo == nullptr)
        return;

    QMutexLocker lock(&m_mutex);
    assert(!m_undoStack.isEmpty());

    // Figure out what to do with any pending redos.
    if (hasRedos()) {
        switch (m_middleMode) {
        case MiddleMode::ClearRedo:
            m_redoStack.clear(); // a new addition clears the redo stack.
            break;
        case MiddleMode::XferAndSkip:
            while (hasRedos())
                m_undoStack.push(m_redoStack.pop());
            break;
        case MiddleMode::XferAndApply:
            while (hasRedos())
                redo();
            break;
        }
    }

    m_undoStack.top().add(undo);
}

// If modifying is false, then this change is not signaled as
// making the state dirty, and if the prior state was a saved state,
// then this is too.
void UndoMgr::endUndo(ModType modType)
{
    QMutexLocker lock(&m_mutex);
    if (--m_nesting > 0)
        return;

    if (m_undoStack.isEmpty())
        return;

    // If it's empty, don't use it.
    if (m_undoStack.top().isEmpty()) {
        m_undoStack.pop();
        return;
    }

    // If it's a non-modifying change, or the whole manager is marked as
    // non-modifying, don't send dirty signals.
    if (modType == ModType::Modifying && m_modType == ModType::Modifying) {
        emitDirtyStateChanged(true);
    } else {
        if (atSavePoint())
            ++m_unmodifiedEnd;
    }

    ++m_currentId;  // increment current ID

    limitStacks();
    emit undoAdded();
}

void UndoMgr::setLimits(int maxCount, size_t maxSize)
{
    QMutexLocker lock(&m_mutex);

    m_maxCount = maxCount;
    m_maxSize  = maxSize;

    limitStacks();
}

void UndoMgr::setLimits(const CfgDataBase& cfgData)
{
    setLimits(cfgData.maxUndoCount, size_t(cfgData.maxUndoSizeMiB * float(1L<<20)));
}

void UndoMgr::emitDirtyStateChanged(bool dirty)
{
    m_dirty = true;
    emit dirtyStateChanged(dirty);
}

void UndoMgr::setDirty(bool dirty)
{
    QMutexLocker lock(&m_mutex);

    for (auto* obj : m_undoables)
        obj->setDirty(dirty, false);  // don't signal

    // Track this save point.  -1 means empty stack.
    if (dirty == false)
        markSavePoint();

    m_dirty = dirty;
}

void UndoMgr::markSavePoint()
{
    QMutexLocker lock(&m_mutex);

    m_unmodifiedBegin = m_unmodifiedEnd = m_currentId;
}

bool UndoMgr::atSavePoint() const
{
    QMutexLocker lock(&m_mutex);

    return m_unmodifiedBegin <= m_currentId && m_currentId <= m_unmodifiedEnd;
}

void UndoMgr::newConfig(const CfgDataBase& cfgData)
{
    QMutexLocker lock(&m_mutex);

    setLimits(cfgData);
}

void UndoMgr::limitStacks()
{
    QMutexLocker lock(&m_mutex);

    const auto limiter = [this](UndoStack& stack, int& totCount, size_t& totSize) {
        int toRemove = 0;

        // Limit by count or by size, whichever is the most restrictive.
        while (toRemove < stack.size() && (totCount > m_maxCount || totSize > m_maxSize)) {
            totSize -= stack.at(toRemove++).size();
            totCount--;
        }

        stack.remove(0, toRemove);
    };

    int    totCount = m_undoStack.size() + m_redoStack.size();
    size_t totSize  = m_undoStack.sizeEstimate() + m_redoStack.sizeEstimate();

    limiter(m_redoStack, totCount, totSize);  // first remove redos
    limiter(m_undoStack, totCount, totSize);  // then undos

    assert(totCount >= 0 && totCount <= m_maxCount);
    assert(totSize <= m_maxSize);
}

void UndoMgr::registerUndoableObject(UndoableObject* obj)
{
    QMutexLocker lock(&m_mutex);
    m_undoables.insert(obj);
}

void UndoMgr::unregisterUndoableObject(UndoableObject* obj)
{
    QMutexLocker lock(&m_mutex);
    m_undoables.remove(obj);
}

const QString& UndoMgr::topUndoName() const
{
    QMutexLocker lock(&m_mutex);
    return hasUndos() ? m_undoStack.top().name() : nullStr;
}

const QString& UndoMgr::topRedoName() const
{
    QMutexLocker lock(&m_mutex);
    return hasRedos() ? m_redoStack.top().name() : nullStr;
}

bool UndoMgr::apply(UndoMgr::UndoStack& to, UndoMgr::UndoStack& from, int direction)
{
    QMutexLocker lock(&m_mutex);
    if (from.isEmpty())
        return false;

    m_currentId += direction;  // track where we are

    // Transfer from one stack to the other and apply it.
    to.push(from.pop());

    bool rc;

    if (direction < 1)
        rc = to.top().undo();
    else
        rc = to.top().redo();

    if (rc) {
        emit changeApplied();
        return true;
    }

    emit changeFailed();
    clear();
    return false;
}

void UndoMgr::clear()
{
    m_undoStack.clear();
    m_redoStack.clear();

    m_nesting = 0;
    m_unmodifiedBegin = 0;
    m_unmodifiedEnd = 0;
    m_currentId = 0;
}

bool UndoMgr::undo()
{
    return apply(m_redoStack, m_undoStack, -1);
}

bool UndoMgr::redo()
{
    return apply(m_undoStack, m_redoStack, 1);
}

// Build up a friendly name for undo entries.
QString UndoMgr::genName(const QString& verb, int count, const QString& singular, const QString& plural)
{
    return QString("%1 %2 %3")
            .arg(verb)
            .arg(count)
            .arg(count > 1 ? plural : singular);
}

// Similar to form above but with {singular, plural} in a tuple.
QString UndoMgr::genName(const QString& verb, int count, const std::tuple<const QString, const QString>& names)
{
    return genName(verb, count, std::get<0>(names), std::get<1>(names));
}

QString UndoMgr::genNameX(const QString& verb, int count)
{
    return QString("%1%2%3%4")
            .arg(verb)
            .arg(count > 1 ? " " : "")
            .arg(count > 1 ? QString::number(count) : "")
            .arg(count > 1 ? "X" : "");
}

void UndoMgr::UndoSet::add(const UndoBase* undo)
{
    m_undos.append(UndoRef(undo));
}

bool UndoMgr::UndoSet::undo()
{
    // We must undo in the reverse of the change order.
    for (auto undo : Util::reverse_adapter(m_undos))
        if (!undo->undo())
            return false;

    return true;
}

bool UndoMgr::UndoSet::redo()
{
    // Redo in original change order
    for (auto redo : m_undos)
        if (!redo->redo())
            return false;

    return true;
}

size_t UndoMgr::UndoSet::size() const
{
    if (m_cachedSize > 0)
        return m_cachedSize;

    return m_cachedSize =
            std::accumulate(m_undos.begin(), m_undos.end(), size_t(0), [](const size_t& size, const UndoRef& undo)
    {
        return size + undo->size();
    });
}

size_t UndoMgr::UndoStack::sizeEstimate() const
{
    return std::accumulate(begin(), end(), size_t(0), [](const size_t& size, const UndoSet& undoSet)
    {
        return size + undoSet.size();
    });
}
