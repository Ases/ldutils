/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELMETADATA_H
#define MODELMETADATA_H

#include <functional>
#include <QObject>
#include <QString>
#include <QDataStream>
#include <QVector>
#include <QComboBox>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QAbstractItemView>

#include <src/ui/widgets/checklistitemdelegate.h>

class QStandardItemModel;
class QComboBox;
class QStandardItem;
class CfgData;
class Units;

typedef int ModelType;

// Model metadata interface.
class ModelMetaData
{
public:
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static bool           mdIsRate(ModelType);
    static const QString& mdRateSuffix(ModelType);
    static const Units&   mdUnits(ModelType, const CfgData&);

    template <class MD>
    static QVariant headerData(ModelType section, Qt::Orientation orientation,
                    int role = Qt::DisplayRole);

    template <class MD>
    static Qt::ItemFlags flags(const QModelIndex&);

    static const std::function<QStandardItem*(ModelType, QStandardItem*)> mdIdentityItem;
    static const std::function<bool(ModelType)>                           mdAcceptAll;

    template <class MD>
    static ModelType      mdFind(const QString& n);

    template <class MD>
    static void setupComboBox(QComboBox& comboBox,
                              QStandardItemModel& comboModel,
                              QVector<ModelType>* entries = nullptr,
                              const decltype(mdIdentityItem)& = mdIdentityItem,
                              const decltype(mdAcceptAll)&    = mdAcceptAll);

    template <class MD> 
    static QStringList headersList();
};

template <class MD>
ModelType ModelMetaData::mdFind(const QString& n)
{
    for (ModelType d = MD::_First; d < MD::_Count; d++)
        if (MD::mdName(d) == n)
            return d;

    // failed to find it.. TODO: do something reasonable, such as complain to the programmer.
    return MD::_First;
}

// Set up a combo box & model to display TrackData entries
template <class MD>
void ModelMetaData::setupComboBox(QComboBox& comboBox, QStandardItemModel& comboModel,
                                  QVector<ModelType>* entries,
                                  const decltype(mdIdentityItem)& setupItem,
                                  const decltype(mdAcceptAll)& pred)
{
    if (entries != nullptr)
        entries->reserve(int(MD::_Count));

    for (ModelType td = MD::_First; td < MD::_Count; td++) {
        if (!pred(td))
            continue;

        if (entries != nullptr)
            entries->push_back(td);

        QStandardItem* item = new QStandardItem(MD::mdName(td));

        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        item->setData(MD::mdTooltip(td), Qt::ToolTipRole);
        item->setData(MD::mdWhatsthis(td), Qt::WhatsThisRole);

        comboModel.appendRow(setupItem(td, item));
    }

    comboBox.setModel(&comboModel);
    comboBox.setItemDelegate(new ChecklistItemDelegate(&comboBox));
    comboBox.setMaxVisibleItems(15);

    // Ensurce popup is sized to the maximum item width.
    const int minWidth = comboBox.minimumSizeHint().width() + 25; // TODO: kludge to account for checkbox
    comboBox.view()->setMinimumWidth(minWidth);
}

template <class MD>
QStringList ModelMetaData::headersList()
{
    QStringList headers;

    for (int md = MD::_First; md < MD::_Count; ++md)
        headers << MD::mdName(md);

    return headers;
}

template <class MD>
QVariant ModelMetaData::headerData(ModelType section, Qt::Orientation orientation, int role)
{
    if (orientation == Qt::Horizontal) {
        // Text alignment:
        if (role == Qt::TextAlignmentRole)
            return QVariant(MD::mdAlignment(section));

        // Handle column header tooltips.
        if (role == Qt::ToolTipRole)
            return MD::mdTooltip(section);

        if (role == Qt::WhatsThisRole)
            return MD::mdWhatsthis(section);
    }

    return QVariant();
}

template <class MD>
Qt::ItemFlags ModelMetaData::flags(const QModelIndex& idx)
{
    return MD::mdIsEditable(idx.column()) ? Qt::ItemIsEditable : Qt::NoItemFlags;
}

#endif // MODELMETADATA_H
