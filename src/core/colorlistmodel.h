/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORLISTMODEL_H
#define COLORLISTMODEL_H

#include <QModelIndex>
#include <QColor>
#include <QString>

#include <src/core/treemodel.h>

class ColorListModel : public TreeModel
{
    Q_OBJECT

public:
    enum {
        Name  = 0,
        Color,
        _Count,
    };

    ColorListModel(QObject *parent = nullptr);

    QColor operator[](int row) const;

    // *** begin Settings API
    using TreeModel::save;
    virtual void load(QSettings&) override;
    // *** end Settings API

protected:
    Qt::ItemFlags flags(const QModelIndex&) const override;
    int columnCount(const QModelIndex& = QModelIndex()) const override { return _Count; }
    virtual void addMissing() = 0;
};

#endif // COLORLISTMODEL_H
