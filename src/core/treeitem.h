/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREEITEM_H
#define TREEITEM_H

#include <QVariant>
#include <QVector>
#include <QMap>

#include <src/core/settings.h>

class TreeModel;

class TreeItem : public Settings
{
public:
    typedef QVector<QVariant> ItemData;

    explicit TreeItem(const ItemData& data, TreeItem *parent = nullptr, int role = Qt::DisplayRole);
    explicit TreeItem(TreeItem *parent = nullptr);

    virtual ~TreeItem() override;

    // *** begin Settings API
    virtual void save(QSettings&) const override;
    virtual void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    virtual QDataStream& save(QDataStream&, const TreeModel&) const;
    virtual QDataStream& load(QDataStream&, TreeModel&);
    // *** end Stream Save API

protected:
    virtual TreeItem* child(int number);
    virtual const TreeItem* child(int number) const;
    virtual int       childCount() const;
    virtual int       columnCount() const;
    virtual QVariant  data(int column, int role) const;
    virtual bool      insertChildren(int position, int count, const ItemData& data);
    virtual bool      insertChildren(int position, const QVector<ItemData>& data);
    virtual bool      insertChildren(int position, int count, int columns);
    virtual bool      insertColumns(int position, int columns);
    virtual TreeItem* parent() const;
    virtual bool      removeChildren(int position, int count);
    virtual bool      removeColumns(int position, int columns);
    virtual int       childNumber() const;
    virtual bool      setData(int column, const QVariant &value, int role, bool& changed);
    virtual bool      setData(int column, const QVariant &value, int role);
    virtual TreeItem *factory(const ItemData& data, TreeItem* parent);
    virtual bool      saveRole(int role) const { return role != Qt::DecorationRole; } // return true to save role.
    virtual bool      moveRow(int srcRow, TreeItem* dstParent, int dstRow);
    virtual void      applyOrdering(const QVector<int>&);

    virtual void shallowCopy(const TreeItem* src); // copy data, but not structure

    void createIcons(); // setup icons from paths

    friend class TreeModel;
    friend class ContentAddrModel;

    TreeItem& operator=(const TreeItem&) = delete;
    TreeItem(const TreeItem&)            = delete;

    static const constexpr unsigned int guard = 0x9381b9a0;  // binary save/load guard value

    QMap<int, ItemData>   itemData;
    QVector<TreeItem*>    childItems;
    TreeItem*             parentItem;
};

#endif // TREEITEM_H
