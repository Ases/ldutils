/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QDomDocument>
#include <QFile>
#include <QVariant>

#include <src/core/svgiconengine.h>
#include "svgcolorizer.h"

SvgColorizer::SvgColorizer()
{
}

bool SvgColorizer::setFill(QDomDocument& doc, const QString& val)
{
    const QString attr = "fill";
    const QString tagName = "g";
    const auto elements = doc.elementsByTagName(tagName);

    if (elements.size() != 1)
        return false;

    bool found = false;

    for (int c = 0; c < elements.size(); ++c) {
        const auto attrs = elements.at(c).attributes();
        for (int a = 0; a < attrs.size(); ++a) {
            if (attrs.contains(attr)) {
                attrs.namedItem(attr).setNodeValue(val);
                found = true;
            }
        }
    }

    return found;
}

bool SvgColorizer::isSvg(const QString &name)
{
    // TODO: also check for proper contents
    return name.endsWith(".svg", Qt::CaseInsensitive);
}

bool SvgColorizer::isColorizable(const QDomDocument& doc)
{
    const auto hasFill = [&doc](const QString& tagName) {
        const QString attr = "fill";
        const auto elements = doc.elementsByTagName(tagName);
        for (int c = 0; c < elements.size(); ++c) {
            const auto attrs = elements.at(c).attributes();
            for (int a = 0; a < attrs.size(); ++a)
                if (attrs.contains(attr))
                    return true;
        }

        return false;
    };

    // If there are gradients, don't do this.
    if (doc.elementsByTagName("linearGradient").size() > 0)
        return false;

    // If there are path or rect fills, don't do this.
    return !hasFill("path") && !hasFill("rect");
}

namespace {
template <typename C, typename KEY> const QIcon& FindOrInsert(C& c, const KEY& key, const QString& name)
{
    if (const auto it = c.find(key); it != c.end())
        return it.value();

    return c.insert(name, QIcon(name)).value();
}
} // anonymous namespace

const QIcon& SvgColorizer::operator()(const QString& name, const QVariant& color, bool colorize)
{
    return (*this)(name, color.value<QColor>(),
                   colorize && color.type() == QVariant::Color);
}

const QIcon& SvgColorizer::operator()(const QString& name, const QColor& color, bool colorize)
{
    return (*this)(name, color.rgb(), colorize);
}

void SvgColorizer::clear()
{
    iconMap.clear();
    uncoloredIcons.clear();
}

const QIcon& SvgColorizer::operator()(const QString& name, const QRgb& color, bool colorize)
{
    static const QIcon empty;
    if (name.isNull())
        return empty;

    if (!colorize)
        return FindOrInsert(uncoloredIcons, name, name);

    // If present in colorize cache, return it.
    if (const auto it = iconMap.find(qMakePair(name, color)); it != iconMap.end())
        return it.value().icon;

    // If present in uncolored cache, return it.  Means we were previously unable to colorize it.
    if (const auto it = uncoloredIcons.find(name); it != uncoloredIcons.end())
        return it.value();

    // If not colorizable, create and return icon directly.
    if (!isSvg(name))
        FindOrInsert(uncoloredIcons, name, name);

    // Else try to color it, and return that form.
    QFile file(name);
    if (!file.open(QFile::ReadOnly))
        return FindOrInsert(uncoloredIcons, name, name);

    QDomDocument doc;
    doc.setContent(file.readAll());

    // If not colorizable, create and return icon directly.
    if (!isColorizable(doc))
        FindOrInsert(uncoloredIcons, name, name);

    // set global stroke fill color
    if (!setFill(doc, QString::asprintf("#%02x%02x%02x", qRed(color), qGreen(color), qBlue(color))))
        return uncoloredIcons.insert(name, QIcon(name)).value();

    SvgIconEngine* engine = new SvgIconEngine(doc.toByteArray());

    // Insert modified icon in the map.
    return iconMap.insert(qMakePair(name, color),
                          IconData(QIcon(engine), engine)).value().icon;
}

const QString SvgColorizer::filename(const QString& name, const QColor& color, bool colorize)
{
    return filename(name, color.rgb(), colorize);
}

// Return a filename for the [name,color] tuple.  If we are colorizing it, we have to make
// up a new file, because the colorized data never had one before.
const QString SvgColorizer::filename(const QString& name, const QRgb& color, bool colorize)
{
    // if it's not to be colorized, just return the original resource.
    if (!colorize)
        return name;

    // Otherwise, we've got to invent a new filename for this resource.

    // Make sure it's in the map (in case nobody asked for it before)
    (*this)(name, color, colorize);

    if (const auto it = iconMap.find(qMakePair(name, color)); it != iconMap.end()) {
        it->file.reset(new QTemporaryFile());

        if (it->file->open()) {
            it->file->write(it->engine->data());
            it->file->close();

            return it->file->fileName();
        }
    }

    return name;  // something went wrong
}
