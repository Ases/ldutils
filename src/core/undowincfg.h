/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOWINCFG_H
#define UNDOWINCFG_H

#include <QByteArray>

#include "undobase.h"
#include "undomgr.h"

class MainWindowBase;
class UndoMgr;
class MainWindowBase;

// Undoer for window/tab configuration
class UndoWinCfg final : public UndoBase
{
public:
    UndoWinCfg(MainWindowBase&, const QByteArray& before, const QByteArray& after);

    static QByteArray read(MainWindowBase&);  // Return window config as a byte array

    // Our own version of a scoped undo
    class ScopedUndo : public UndoMgr::ScopedUndo {
    public:
        ScopedUndo(MainWindowBase& mainWindow, const QString& name);
        ~ScopedUndo();
    private:
        MainWindowBase&  m_mainWindow;
        const QByteArray m_before;
    };

protected:
    using UndoBase::size;

    bool undo() const override { return apply(m_beforeZ); }
    bool redo() const override { return apply(m_afterZ); }
    size_t size() const override;
    const char* className() const override { return "UndoWinCfg"; }

private:
    bool apply(const QByteArray& stateZ) const;

    MainWindowBase& m_mainWindow;
    QByteArray      m_beforeZ;     // compressed & serialized win/tab state
    QByteArray      m_afterZ;      // ...
};

#endif // UNDOWINCFG_H
