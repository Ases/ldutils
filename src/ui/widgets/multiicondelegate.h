/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MULTIICONDELEGATE_H
#define MULTIICONDELEGATE_H

#include <QIcon>
#include <QPen>
#include <src/util/roles.h>
#include <src/ui/widgets/delegatebase.h>

class MultiIconDelegate : public DelegateBase
{
public:
    enum OutlineMode {
        None,
        Box,
    };

    MultiIconDelegate(QObject* parent, const QString& winTitle, OutlineMode,
                      bool winBorders = true, int role = Util::RawDataRole);

    virtual void paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex &idx) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &idx) const override;

    static QPen outlinePen();  // return pen used to outline icons

protected:
    // Obtain the icon to draw for a given name
    virtual QIcon iconFor(const QString&) const = 0;
    virtual QSize iconSize() const = 0; // must by dynamic: cfgData can change at any time
    virtual int   maxIcons() const = 0; // ...

    void drawIcon(QPainter* painter, const QIcon& icon, const QSize requestSize,
                  QRect& rect, int fixedRight, const QStyleOptionViewItem& option,
                  Qt::Alignment align = Qt::AlignLeft | Qt::AlignVCenter) const;

    static const constexpr int ellipsisWidthRatio = 3;
    const QIcon        m_ellipsisIcon;  // the ellipsis icon
    const QPen         m_outlinePen;
    const OutlineMode  m_outlineMode;
};

#endif // MULTIICONDELEGATE_H
