/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QVBoxLayout>
#include <QAbstractItemModel>

#include <src/ui/dialogs/texteditor.h>
#include "texteditordelegate.h"

TextEditorDelegate::TextEditorDelegate(QObject* /*parent*/,
                                       const QString& winTitle, bool winBorders, int role) :
    DelegateBase(nullptr, true, winTitle, winBorders, role)
{
}

QWidget* TextEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& index) const
{
    TextEditor* editor = new TextEditor(parent);
    if (editor == nullptr)
        return nullptr;

    if (const QAbstractItemModel* model = dynamic_cast<const QAbstractItemModel*>(index.model()); model != nullptr) {
        setPopup(editor);
        editor->setHtml(model->data(index, role).value<QString>());
    }

    return editor;
}

void TextEditorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        TextEditor* dialog = dynamic_cast<TextEditor*>(editor);
        if (dialog == nullptr)
            return QString();

        accepted = (dialog->result() == QDialog::Accepted);
        return dialog->toHtml();
    });
}
