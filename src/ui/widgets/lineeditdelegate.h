/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LINEEDITDELEGATE_H
#define LINEEDITDELEGATE_H

#include <src/ui/widgets/delegatebase.h>

class LineEditDelegate : public DelegateBase
{
public:
    LineEditDelegate(QObject* parent = nullptr);

    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex&) const override;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex&) const override;
};

#endif // LINEEDITDELEGATE_H
