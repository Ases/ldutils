/*
    Copyright 2018-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMouseEvent>

#include "chartviewzoom.h"

void ChartViewZoom::mousePressEvent(QMouseEvent *event)
{

    if (chart() != nullptr && chart()->plotArea().contains(event->pos())) {
        m_pressed = true;
        m_panned = false;
        m_prevX = event->x();
        m_prevY = event->y();
        m_origX = m_prevX;
        m_origY = m_prevY;
    }

    emit mousePress(event);
    QtCharts::QChartView::mousePressEvent(event);
}

void ChartViewZoom::mouseReleaseEvent(QMouseEvent* event)
{
    if (m_pressed) {
        if (m_panned)
            emit mouseEndPan();
        else
            emit mouseRelease(event);
        m_pressed = false;
    }

    QtCharts::QChartView::mouseReleaseEvent(event);
}

void ChartViewZoom::mouseMoveEvent(QMouseEvent *event)
{
    if (m_pressed) {
        emit mousePan(event, event->x() - m_prevX, event->y() - m_prevY);

        m_prevX  = event->x();
        m_prevY  = event->y();
        m_panned = true;

        return;
    }

    // Non-drag event in absolute chart coordinates
    emit mouseMove(event);

    QtCharts::QChartView::mouseMoveEvent(event);
}

void ChartViewZoom::contextMenuEvent(QContextMenuEvent *event)
{
    emit customContextMenuRequested(event->pos());
}

bool ChartViewZoom::nonZeroPan(QMouseEvent* event) const
{
    return m_origX != event->x() || m_origY != event->y();
}
