/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <src/ui/widgets/delegatebase.h>

class ColorDelegate : public DelegateBase
{
public:
    ColorDelegate(QObject* parent = nullptr, bool showAlpha = false, const QString& winTitle = tr("Edit color"),
                  bool winBorders = true, int role = Qt::BackgroundRole);

    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex&) const override;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex&) const override;

    void paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex &idx) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &idx) const override;

    void setPadSize(QSize s) { pad = s; }
    void setPadSize(int w, int h) { pad = QSize(w, h); }
    void setMaxSize(QSize s) { maxSize = s; }
    void setMaxSize(int w, int h) { maxSize = QSize(w, h); }

private:
    QSize pad;
    QSize maxSize;
    bool  showAlpha;
};

#endif // COLORDELEGATE_H
