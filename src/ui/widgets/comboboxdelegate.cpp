/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "comboboxdelegate.h"

#include <QComboBox>
#include <QStringList>

ComboBoxDelegate::ComboBoxDelegate(QObject* parent, const QStringList& items) :
    DelegateBase(parent, false),
    items(items)
{
}

QWidget* ComboBoxDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    QComboBox* editor = new QComboBox(parent);
    if (editor == nullptr)
        return nullptr;

    if (const QAbstractItemModel* model = dynamic_cast<const QAbstractItemModel*>(idx.model()); model != nullptr) {
        editor->addItems(items);
        editor->setCurrentText(model->data(idx, role).toString());
        editor->setFrame(false);
        editor->setAutoFillBackground(true);  // otherwise it ends up transparent
    }

    return editor;
}

void ComboBoxDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        QComboBox* comboBox = dynamic_cast<QComboBox*>(editor);
        if (comboBox == nullptr)
            return QString();

        accepted = true;
        return comboBox->currentText();
    });
}

