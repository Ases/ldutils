/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DELEGATEBASE_H
#define DELEGATEBASE_H

#include <functional>

#include <QStyledItemDelegate>
#include <QDialog>

class QItemSelectionModel;

class UndoMgr;

class DelegateBase : public QStyledItemDelegate
{
public:
    DelegateBase(QObject* parent, bool m_popupEditor,
                 const QString& m_winTitle = "", bool m_winBorders = true, int role = Qt::EditRole);

    DelegateBase& setSelector(const QItemSelectionModel* s) { selector = s; return *this; }
    DelegateBase& setUndoMgr(UndoMgr& undoMgr) { m_undoMgr = &undoMgr; return *this; }

    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &idx) const override = 0;
    virtual void setEditorData(QWidget */*editor*/, const QModelIndex &/*idx*/) const override { }
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &idx) const override = 0;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &idx) const override;

protected:
    // Convenience function to set popup dialog flags
    QDialog* setPopup(QDialog* editor) const;

    // Convenience function to set given column of all selected rows
    virtual void updateResultFromEditor(QWidget* editor, QAbstractItemModel* model, const QModelIndex &idx,
                                        const std::function<QVariant(bool& accepted)>&) const;

    virtual void updateResultFromEditor(QWidget* editor, const QModelIndex &idx,
                                        const std::function<void(const QModelIndex& idx)>&) const;

    template <typename DIALOG>
    void updateResultsFromDialog(QWidget *editor, QAbstractItemModel *model, const QModelIndex &idx) const;

    template <typename WIDGET>
    void updateResultsFromWidget(QWidget *editor, QAbstractItemModel *model, const QModelIndex &idx) const;

protected:
    const QItemSelectionModel* selector;
    int                        role; // model role

private:
    static auto undoName(const QModelIndex& idx, int count);

    const bool     m_popupEditor;
    const QString  m_winTitle;   // window title
    UndoMgr*       m_undoMgr;    // pointer to undo mgr, or null
    bool           m_winBorders; // true to use window borders
};

template <typename DIALOG>
inline void DelegateBase::updateResultsFromDialog(QWidget *editor, QAbstractItemModel *model, const QModelIndex &idx) const
{
    const DIALOG* dialog = dynamic_cast<DIALOG*>(editor);

    if (editor == dialog)
        return;

    updateResultFromEditor(editor, model, idx, [dialog](bool& accepted) {
        accepted = dialog->result() == QDialog::Accepted;
        return dialog->value();
    });
}

template <typename WIDGET>
void DelegateBase::updateResultsFromWidget(QWidget *editor, QAbstractItemModel *model, const QModelIndex &idx) const
{
    const WIDGET* widget = dynamic_cast<WIDGET*>(editor);

    if (widget == nullptr)
        return;

    updateResultFromEditor(editor, model, idx, [widget](bool& accepted) {
        accepted = true;
        return widget->value();
    });
}


#endif // DELEGATEBASE_H
