/*
    Copyright 2018-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHARTVIEWZOOM_H
#define CHARTVIEWZOOM_H

#include <QtCharts/QChartView>

class QWheelEvent;
class QMouseEvent;

class ChartViewZoom : public QtCharts::QChartView
{
    Q_OBJECT
public:
    ChartViewZoom(QWidget *parent = nullptr) :
        QtCharts::QChartView(parent),
        m_origX(0), m_origY(0), m_prevX(0), m_prevY(0), m_pressed(false), m_panned(false)
    { }

signals:
    void mousePan(QMouseEvent*, int relX, int relY);  // LMB pan
    void mouseMove(QMouseEvent*);                     // normal mouse move
    void mouseEndPan();                               // end of panning
    void mousePress(QMouseEvent*);                    // for subclasses
    void mouseRelease(QMouseEvent*);                  // for subclasses

protected:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void contextMenuEvent(QContextMenuEvent* event) override;
    bool nonZeroPan(QMouseEvent* event) const;  // true if panned beyond original click location

    int  m_origX;    // begin pan location
    int  m_origY;    // end pan location
    int  m_prevX;    // for generation of relative motion
    int  m_prevY;    // ...
    bool m_pressed;  // set in mouse press event, to test for panning
    bool m_panned;   // true if there were any pan events
};

#endif // CHARTVIEWZOOM_H
