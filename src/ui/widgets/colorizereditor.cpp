/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "colorizereditor.h"
#include "ui_colorizereditor.h"

#include "src/util/icons.h"
#include "src/core/colorizermodel.h"

ColorizerEditor::ColorizerEditor(const CfgDataBase& cfgData, ColorizerModel& model,
                                 const QStringList& headerLabels, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ColorizerEditor),
    model(model),
    headerView(Qt::Horizontal, this),
    columnDelegate(this, headerLabels),
    colorDelegate(this),
    iconDelegate(":art/tags", true),
    queryDelegate(cfgData, model.queryCtx(*this), this)
{
    ui->setupUi(this);

    setupActionIcons();
    setupView();
    setupDelegates();
    setupMenu();
    setupSignals();
}

ColorizerEditor::~ColorizerEditor()
{
    delete ui;
}

void ColorizerEditor::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Unset_Icon,             "edit-clear");
    Icons::defaultIcon(ui->action_Unset_Background_Color, "edit-clear");
    Icons::defaultIcon(ui->action_Unset_Foreground_Color, "edit-clear");
    Icons::defaultIcon(ui->action_Remove,                 "edit-delete");
    Icons::defaultIcon(ui->action_Add,                    "list-add");
}

void ColorizerEditor::setupSignals()
{
    // Selection changes
    connect(ui->colorizerView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ColorizerEditor::enableMenus);
}

void ColorizerEditor::setupView()
{
    QTreeView& view = *ui->colorizerView;

    view.setModel(&model);
    view.setHeader(&headerView);

    headerView.setSectionResizeMode(QHeaderView::Interactive);
    headerView.setSectionsMovable(true);

    enableMenus();
}

void ColorizerEditor::defaultColumnWidths()
{
    QTreeView& view = *ui->colorizerView;
    Util::ResizeColumns(view, { 15, 45, 5, 5, 5, 5, 5 });
}

QTreeView* ColorizerEditor::treeView() const
{
    return (ui == nullptr) ? nullptr : ui->colorizerView;
}

void ColorizerEditor::setVisible(bool visible)
{
    QWidget::setVisible(visible);

    if (visible)
        defaultColumnWidths();
}

void ColorizerEditor::setupDelegates()
{
    QTreeView& view = *ui->colorizerView;

    columnDelegate.setSelector(view.selectionModel());
    colorDelegate.setSelector(view.selectionModel());
    iconDelegate.setSelector(view.selectionModel());
    queryDelegate.setSelector(view.selectionModel());

    view.setItemDelegateForColumn(ColorizerModel::Column,  &columnDelegate);
    view.setItemDelegateForColumn(ColorizerModel::Query,   &queryDelegate);
    view.setItemDelegateForColumn(ColorizerModel::Icon,    &iconDelegate);
    view.setItemDelegateForColumn(ColorizerModel::FgColor, &colorDelegate);
    view.setItemDelegateForColumn(ColorizerModel::BgColor, &colorDelegate);
}

void ColorizerEditor::setupMenu()
{
    contextMenu.addActions({ ui->action_Add,
                             ui->action_Remove });
    contextMenu.addSeparator();
    contextMenu.addActions({ ui->action_Unset_Icon,
                             ui->action_Unset_Foreground_Color,
                             ui->action_Unset_Background_Color });

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &ColorizerEditor::customContextMenuRequested, this, &ColorizerEditor::showContextMenu);
}

void ColorizerEditor::enableMenus()
{
    // Enable menus as appropriate
    const bool hasSelection = ui->colorizerView->selectionModel()->hasSelection();

    ui->action_Remove->setEnabled(model.rowCount() > 0);
    ui->action_Unset_Background_Color->setEnabled(hasSelection);
    ui->action_Unset_Foreground_Color->setEnabled(hasSelection);
    ui->action_Unset_Icon->setEnabled(hasSelection);

    ui->buttonRemoveItem->setEnabled(ui->action_Remove->isEnabled());
    ui->buttonResetBg->setEnabled(ui->action_Unset_Background_Color->isEnabled());
    ui->buttonResetFg->setEnabled(ui->action_Unset_Foreground_Color->isEnabled());
    ui->buttonResetIcon->setEnabled(ui->action_Unset_Icon->isEnabled());
}

void ColorizerEditor::showContextMenu(const QPoint& pos)
{
    contextMenu.exec(mapToGlobal(pos));
}

void ColorizerEditor::on_action_Unset_Icon_triggered()
{
    model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::Icon);
}

void ColorizerEditor::on_action_Unset_Background_Color_triggered()
{
    model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::BgColor);
}

void ColorizerEditor::on_action_Unset_Foreground_Color_triggered()
{
    model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::FgColor);
}

void ColorizerEditor::on_action_Remove_triggered()
{
    model.removeRows(ui->colorizerView->selectionModel(), nullptr);
    enableMenus();
}

void ColorizerEditor::on_action_Add_triggered()
{
    int insertRow = ui->colorizerView->selectionModel()->currentIndex().row();
    if (insertRow < 0)
        insertRow = model.rowCount();

    model.insertRows(insertRow, 1);
    ui->colorizerView->scrollTo(model.index(insertRow, 0));
    enableMenus();
}
