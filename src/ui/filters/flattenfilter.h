/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FLATTENFILTER_H
#define FLATTENFILTER_H

#include <cassert>
#include <functional>

#include <QAbstractProxyModel>
#include <QModelIndexList>
#include <QModelIndex>
#include <QHash>
#include <QPersistentModelIndex>

// TODO: this should be in ldutils.a, but it causes unknown linking problems.
class FlattenFilter final : public QAbstractProxyModel
{
    Q_OBJECT

public:
    typedef std::function<bool(const QAbstractItemModel* model, const QModelIndex&)> PredFn;

    FlattenFilter(QObject *parent = nullptr, PredFn pred = nullptr);
    ~FlattenFilter() override;

    int         rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int         columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;
    QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;

    QVariant    headerData(int section, Qt::Orientation orientation, int role) const override;

    bool        hasChildren(const QModelIndex &parent = QModelIndex()) const override;
    void        setSourceModel(QAbstractItemModel *sourceModel) override;

    void        setPredicate(const PredFn pred);

    bool        setDynamic(bool dynamic);  // returns old value
    void        invalidateFilter();

private slots:
    void processDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);
    void processRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
    void processRowsInserted(const QModelIndex &parent, int first, int last);
    void processRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void processRowsRemoved(const QModelIndex &parent, int first, int last);

    void processRowsAboutToBeMoved(const QModelIndex &, int, int, const QModelIndex &, int);
    void processRowsMoved(const QModelIndex &, int, int, const QModelIndex &, int);
    void processColumnsAboutToBeInserted(const QModelIndex &, int, int);
    void processColumnsInserted(const QModelIndex &, int, int);
    void processColumnsAboutToBeRemoved(const QModelIndex &, int, int);
    void processColumnsRemoved(const QModelIndex &, int, int);
    void processColumnsAboutToBeMoved(const QModelIndex &, int, int, const QModelIndex &, int);
    void processColumnsMoved(const QModelIndex &, int, int, const QModelIndex &, int);

    void processLayoutAboutToBeChanged(const QList<QPersistentModelIndex> &parents, QAbstractItemModel::LayoutChangeHint);
    void processLayoutChanged(const QList<QPersistentModelIndex> &parents, QAbstractItemModel::LayoutChangeHint hint);

    void processModelAboutToBeReset();
    void processModelReset();

private:
    QModelIndex insert(const QModelIndex& sourceIndex);
    bool acceptedRow(const QModelIndex& sourceIndex) const;

    bool                              m_dynamic;
    PredFn                            predicate;

    QVector<QPersistentModelIndex>    proxyToSource;
    QHash<QPersistentModelIndex, int> sourceToProxy;

    static const constexpr bool useProcessRowsInserted = false;
    static const constexpr bool useProcessRowsRemoved  = false;

    inline const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        assert(idx.row() == -1 || idx.row() < proxyToSource.size());
        assert(sourceToProxy.size() == proxyToSource.size());
        return idx;
    }

    void clear();
    void rebuild();
};

#endif
