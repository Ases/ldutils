/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SUBTREEFILTER_H
#define SUBTREEFILTER_H

#include <functional>
#include <QSortFilterProxyModel>
#include <QModelIndexList>
#include <QScopedPointer>

#include <src/core/query.h>

class QTreeView;
class QItemSelectionModel;
class CfgData;
class QRegExp;

// Filter, accepting rows who have sub-items matching filter
class SubTreeFilter final : public QSortFilterProxyModel
{
private:
    typedef std::function<bool(const QModelIndex&)> ModelIndexPred;

public:
    using QSortFilterProxyModel::QSortFilterProxyModel;

    SubTreeFilter(QObject *parent = nullptr);
    ~SubTreeFilter() override { }

    bool filterAcceptsRow(int row, const QModelIndex& parent) const override;

    // filter-space sort of selection, so clipboard copy ends up in right order
    QModelIndexList sortedSelection(const QItemSelectionModel* selector,
                                    const QModelIndex& parent = QModelIndex()) const;

    // First form uses provided row indexes.  Second for uses view selection.
    void copyToClipboard(const QTreeView& view, const QModelIndexList& filterRows,
                         const QString& rowSep, const QString& colSep,
                         int role,
                         const ModelIndexPred& pred = [](const QModelIndex&) { return true; } ) const;

    void setSourceModel(QAbstractItemModel* sourceModel) override;
    void setCaseSensitivity(Qt::CaseSensitivity cs);

    bool isValid() const { return m_queryRoot->isValid(); }
    bool isEmpty() const { return dynamic_cast<const Query::All*>(m_queryRoot.data()) != nullptr; }

    const Query::Context& queryCtx() const { return m_queryCtx; }

public slots:
    void setQueryString(const QString&);

private:
    bool lessThan(const QModelIndex& left, const QModelIndex& right) const;

    bool anyChildMatches(const QModelIndex& parent) const;

    void setFilterRegExp(const QString&) = delete;
    void setFilterRegExp(const QRegExp&) = delete;

    Query::Context                    m_queryCtx;  // for query language
    QScopedPointer<const Query::Base> m_queryRoot; // root of node filter tree
};

#endif // SUBTREEFILTER_H
