/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTENTFILTER_H
#define CONTENTFILTER_H

#include <QSortFilterProxyModel>

class ContentAddrModel;

// Filters out items available in some given ContentAddrModel
class ContentFilter : public QSortFilterProxyModel
{
public:
    ContentFilter(ContentAddrModel& search,
                  int srcColumn, int srcRole,
                  QObject *parent = nullptr);

    virtual bool filterAcceptsRow(int srcRow, const QModelIndex &srcParent) const override;

private:
    ContentAddrModel& search;            // filter out items available in this model.
    int               srcColumn;         // column to search
    int               srcRole;           // role to search
};

#endif // CONTENTFILTER_H
