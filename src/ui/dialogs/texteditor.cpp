/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QTextCharFormat>
#include <QClipboard>
#include <QMimeData>
#include <QColorDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QTextCursor>
#include <QTextList>

#include "src/util/icons.h"
#include "texteditor.h"
#include "ui_texteditor.h"

// Credit: Some of this code borrows heavily from the Qt "textedit" example shipped with qtcreator.
TextEditor::TextEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEditor),
    editorMenu(this)
{
    ui->setupUi(this);

    setupActionIcons();
    setupActions();
    setupFontSizes();
    setupSignals();
    setupUi();
    setupMenus();
}

TextEditor::~TextEditor()
{
    delete ui;
}

void TextEditor::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Bold,                  "format-text-bold");
    Icons::defaultIcon(ui->action_Italic,                "format-text-italic");
    Icons::defaultIcon(ui->action_Underline,             "format-text-underline");
    Icons::defaultIcon(ui->action_Align_Left,            "format-justify-left");
    Icons::defaultIcon(ui->action_Align_Right,           "format-justify-right");
    Icons::defaultIcon(ui->action_Align_Center,          "format-justify-center");
    Icons::defaultIcon(ui->action_Cut,                   "edit-cut");
    Icons::defaultIcon(ui->action_Copy,                  "edit-copy");
    Icons::defaultIcon(ui->action_Paste,                 "edit-paste");
    Icons::defaultIcon(ui->action_Undo,                  "edit-undo");
    Icons::defaultIcon(ui->action_Redo,                  "edit-redo");
    Icons::defaultIcon(ui->action_Align_Fill,            "format-justify-fill");
    Icons::defaultIcon(ui->action_Text_Color,            "format-text-color");
    Icons::defaultIcon(ui->action_Insert_Link,           "insert-link");
    Icons::defaultIcon(ui->action_Insert_Image,          "insert-image");
    Icons::defaultIcon(ui->action_Format_Ordered_List,   "format-list-ordered");
    Icons::defaultIcon(ui->action_Format_Unordered_List, "format-list-unordered");
}

void TextEditor::setupSignals()
{
    if (ui == nullptr)
        return;

    // Cursor position
    connect(ui->textEdit, &QTextEdit::cursorPositionChanged, this, &TextEditor::cursorPositionChanged);

    // Character format
    connect(ui->textEdit, &QTextEdit::currentCharFormatChanged, this, &TextEditor::currentCharFormatChanged);

    // Font family
    connect(ui->fontFamily, static_cast<void (QComboBox::*)(int)> (&QComboBox::activated), this, &TextEditor::textFamily);

    // Font size
    connect(ui->fontSize, static_cast<void (QComboBox::*)(int)> (&QComboBox::activated), this, &TextEditor::textSize);

    // Undo/redo:
    connect(ui->textEdit->document(), &QTextDocument::undoAvailable, ui->action_Undo, &QAction::setEnabled);
    connect(ui->textEdit->document(), &QTextDocument::redoAvailable, ui->action_Redo, &QAction::setEnabled);

    connect(QApplication::clipboard(), &QClipboard::dataChanged, this, &TextEditor::clipboardDataChanged);

    // Reset
    QPushButton* resetButton = ui->buttonBox->button(QDialogButtonBox::Reset);
    connect(resetButton, &QPushButton::clicked, this, &TextEditor::resetText);

    // Menu
    connect(this, &TextEditor::customContextMenuRequested, this, &TextEditor::showContextMenu);
}

void TextEditor::setupActions()
{
    if (ui == nullptr)
        return;

    ui->formatBold->setDefaultAction(ui->action_Bold);
    ui->formatItalic->setDefaultAction(ui->action_Italic);
    ui->formatUnderline->setDefaultAction(ui->action_Underline);
    ui->textColor->setDefaultAction(ui->action_Text_Color);
    ui->formatLeft->setDefaultAction(ui->action_Align_Left);
    ui->formatCenter->setDefaultAction(ui->action_Align_Center);
    ui->formatRight->setDefaultAction(ui->action_Align_Right);
    ui->formatFill->setDefaultAction(ui->action_Align_Fill);
    ui->editCut->setDefaultAction(ui->action_Cut);
    ui->editCopy->setDefaultAction(ui->action_Copy);
    ui->editPaste->setDefaultAction(ui->action_Paste);
    ui->editUndo->setDefaultAction(ui->action_Undo);
    ui->editRedo->setDefaultAction(ui->action_Redo);
    ui->insertLink->setDefaultAction(ui->action_Insert_Link);
    ui->insertImage->setDefaultAction(ui->action_Insert_Image);
    ui->formatListOrdered->setDefaultAction(ui->action_Format_Ordered_List);
    ui->formatListUnordered->setDefaultAction(ui->action_Format_Unordered_List);
}

void TextEditor::setupUi()
{
    ui->action_Undo->setEnabled(ui->textEdit->document()->isUndoAvailable());
    ui->action_Redo->setEnabled(ui->textEdit->document()->isRedoAvailable());

    // default font
    ui->textEdit->setFont(QApplication::font());

    alignmentChanged();
    colorChanged();
    fontChanged();
    styleChanged();
    clipboardDataChanged();

    ui->textEdit->setFocus();

    ui->textEdit->setTextInteractionFlags(ui->textEdit->textInteractionFlags() | Qt::LinksAccessibleByMouse);
}

void TextEditor::setupMenus()
{
    setContextMenuPolicy(Qt::CustomContextMenu);

    QMenu* formatMenu = editorMenu.addMenu("Character Format");
    QMenu* alignMenu = editorMenu.addMenu("Alignment");
    QMenu* insertMenu = editorMenu.addMenu("Insert");

    formatMenu->addAction(ui->action_Bold);
    formatMenu->addAction(ui->action_Italic);
    formatMenu->addAction(ui->action_Underline);
    formatMenu->addAction(ui->action_Text_Color);

    alignMenu->addAction(ui->action_Align_Left);
    alignMenu->addAction(ui->action_Align_Center);
    alignMenu->addAction(ui->action_Align_Right);
    alignMenu->addAction(ui->action_Align_Fill);
    alignMenu->addSeparator();
    alignMenu->addAction(ui->action_Format_Ordered_List);
    alignMenu->addAction(ui->action_Format_Unordered_List);

    insertMenu->addAction(ui->action_Insert_Link);
    insertMenu->addAction(ui->action_Insert_Image);

    editorMenu.addAction(ui->action_Undo);
    editorMenu.addAction(ui->action_Redo);
}

void TextEditor::setupFontSizes()
{
    const QList<int> standardSizes = QFontDatabase::standardSizes();
    for (int size : standardSizes)
        ui->fontSize->addItem(QString::number(size));

    ui->fontSize->setCurrentIndex(standardSizes.indexOf(QApplication::font().pointSize()));
}

void TextEditor::showContextMenu(const QPoint& pos)
{
    editorMenu.exec(mapToGlobal(pos));
}

void TextEditor::setHtml(const QString& s)
{
    if (ui != nullptr)
        ui->textEdit->setHtml(s);

    original = s;
}

QString TextEditor::toHtml() const
{
    if (ui == nullptr)
        return QString();

    return ui->textEdit->toHtml();
}

void TextEditor::cursorPositionChanged()
{
    alignmentChanged();
    styleChanged();
}

void TextEditor::clipboardDataChanged()
{
    if (const QMimeData* md = QApplication::clipboard()->mimeData())
        ui->action_Paste->setEnabled(md->hasText());
}

void TextEditor::textFamily(int index)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(ui->fontFamily->itemText(index));
    mergeFormatOnSelection(fmt);
}

void TextEditor::textSize(int index)
{
    const qreal pointSize = qreal(ui->fontSize->itemText(index).toFloat());

    if (pointSize > 0.0f) {
        QTextCharFormat fmt;
        fmt.setFontPointSize(pointSize);
        mergeFormatOnSelection(fmt);
    }
}

void TextEditor::textColor()
{
    const QColor col = QColorDialog::getColor(ui->textEdit->textColor(), this);
    if (!col.isValid())
        return;
    QTextCharFormat fmt;
    fmt.setForeground(col);
    mergeFormatOnSelection(fmt);
    colorChanged();
}

void TextEditor::resetText()
{
    const QMessageBox::StandardButton result =
            QMessageBox::warning(this, tr("Reset?"),
                                 tr("Reset text?  This will lose recent changes."),
                                 QMessageBox::Ok | QMessageBox::Cancel,
                                 QMessageBox::Cancel);

    if (result != QMessageBox::Ok)
        return;

    ui->textEdit->setHtml(original);
}

void TextEditor::alignmentChanged()
{
    const Qt::Alignment a = ui->textEdit->alignment();
    ui->action_Align_Left->setChecked((a & Qt::AlignLeft) ? true : false);
    ui->action_Align_Center->setChecked((a & Qt::AlignHCenter) ? true : false);
    ui->action_Align_Right->setChecked((a & Qt::AlignRight) ? true : false);
    ui->action_Align_Fill->setChecked((a & Qt::AlignJustify) ? true : false);
}

void TextEditor::colorChanged()
{
    const QColor& color = ui->textEdit->textColor();

    QPixmap p(ui->textColor->size());
    p.fill(color);
    ui->action_Text_Color->setIcon(p);
}

void TextEditor::fontChanged()
{
    const QFont& font = ui->textEdit->font();
    ui->fontFamily->setCurrentIndex(ui->fontFamily->findText(QFontInfo(font).family()));
    ui->fontSize->setCurrentIndex(ui->fontSize->findText(QString::number(font.pointSize())));
}

void TextEditor::styleChanged()
{
    if (QTextList* list = ui->textEdit->textCursor().currentList(); list != nullptr) {
        bool ordered;
        switch (list->format().style()) {
        case QTextListFormat::ListDisc:
        case QTextListFormat::ListCircle:
        case QTextListFormat::ListSquare:
            ordered = false;
            break;
        default:
            ordered = true;
            break;
        }

        ui->action_Format_Ordered_List->setChecked(ordered);
        ui->action_Format_Unordered_List->setChecked(!ordered);
    } else {
        ui->action_Format_Ordered_List->setChecked(false);
        ui->action_Format_Unordered_List->setChecked(false);
    }
}

void TextEditor::currentCharFormatChanged(const QTextCharFormat& format)
{
    ui->action_Bold->setChecked(format.fontWeight() == QFont::Bold);
    ui->action_Italic->setChecked(format.fontItalic());
    ui->action_Underline->setChecked(format.fontUnderline());
    colorChanged();
}

void TextEditor::setListFormat(bool apply, QTextListFormat::Style style)
{
    QTextCursor cursor = ui->textEdit->textCursor();

    if (apply) {
        cursor.beginEditBlock();

        QTextBlockFormat blockFmt = cursor.blockFormat();
        QTextListFormat listFmt;

        if (cursor.currentList() != nullptr) {
            listFmt = cursor.currentList()->format();
        } else {
            listFmt.setIndent(blockFmt.indent() + 1);
            blockFmt.setIndent(0);
            cursor.setBlockFormat(blockFmt);
        }

        listFmt.setStyle(style);
        cursor.createList(listFmt);
        cursor.endEditBlock();
    } else {
        if (QTextList* list = cursor.currentList(); list != nullptr) {
            list->remove(cursor.block());
            QTextBlockFormat blockFmt = cursor.blockFormat();
            blockFmt.setIndent(0);
            cursor.setBlockFormat(blockFmt);
        }
    }

    styleChanged();
}

void TextEditor::mergeFormatOnSelection(const QTextCharFormat& format)
{
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.mergeCharFormat(format);
    ui->textEdit->mergeCurrentCharFormat(format);
}

void TextEditor::on_action_Bold_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(ui->action_Bold->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Italic_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(ui->action_Italic->isChecked());
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Underline_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(ui->action_Underline->isChecked());
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Text_Color_triggered()
{
    textColor();
}

void TextEditor::on_action_Align_Left_triggered()
{
   ui->textEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
   alignmentChanged();
}

void TextEditor::on_action_Align_Right_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
    alignmentChanged();
}

void TextEditor::on_action_Align_Center_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignHCenter);
    alignmentChanged();
}

void TextEditor::on_action_Align_Fill_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignJustify);
    alignmentChanged();
}

void TextEditor::on_action_Cut_triggered()
{
    ui->textEdit->cut();
}

void TextEditor::on_action_Copy_triggered()
{
    ui->textEdit->copy();
}

void TextEditor::on_action_Paste_triggered()
{
    ui->textEdit->paste();
}

void TextEditor::on_action_Undo_triggered()
{
    ui->textEdit->undo();
}

void TextEditor::on_action_Redo_triggered()
{
    ui->textEdit->redo();
}

void TextEditor::on_action_Insert_Link_triggered()
{
    linkDialog.setText(ui->textEdit->textCursor().selectedText());
    linkDialog.setFocus();

    if (linkDialog.exec() != QDialog::Accepted)
        return;

    const QString link = "<a href=\"" + linkDialog.url() + "\">" + linkDialog.text() + "</a> ";
    ui->textEdit->textCursor().insertHtml(link);
}

void TextEditor::on_action_Insert_Image_triggered()
{
    QMessageBox::warning(this, "Unimplemented",
                         "This feature is currently unimplemented.  Check back later!",
                         QMessageBox::Ok);
    return;

    imageLinkDialog.setFocus();

    if (imageLinkDialog.exec() != QDialog::Accepted)
        return;

    ui->textEdit->textCursor().insertImage(linkDialog.url());
}

void TextEditor::on_action_Format_Ordered_List_triggered(bool checked)
{
    setListFormat(checked, QTextListFormat::ListDecimal);
}

void TextEditor::on_action_Format_Unordered_List_triggered(bool checked)
{
    setListFormat(checked, QTextListFormat::ListDisc);
}
