/*
    Copyright 2018 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DOCDIALOGBASE_H
#define DOCDIALOGBASE_H

#include <QDialog>
#include <QVector>
#include <QStandardItemModel>

namespace Ui {
class DocDialogBase;
}

class QModelIndex;
class TextBrowser;

class DocDialogBase : public QDialog
{
    Q_OBJECT

public:
    explicit DocDialogBase(QWidget *parent = nullptr);
    ~DocDialogBase();

protected slots:
    virtual void changePage(const QModelIndex&);

protected:
    void setupTOC();
    void setupSignals();
    void setupSearchPaths();
    void setupTextBrowser();

    Ui::DocDialogBase* ui;
    QStandardItemModel tocModel;

    struct PageInfo {
        int indent;
        const char* heading;
        const char* resource;
    };

    TextBrowser*      textBrowser;
    QVector<PageInfo> pageInfo;
    bool              searchError; // true if we failed to find the documentation
};

#endif // DOCDIALOGBASE_H
