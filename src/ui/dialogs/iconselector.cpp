/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDirIterator>
#include <QIcon>
#include <QFileDialog>
#include <QImageReader>

#include <src/util/util.h>
#include "iconselector.h"
#include "ui_iconselector.h"

IconSelector::IconSelector(const QString& root, bool showNames, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IconSelector),
    iconSortModel(this)
{
    ui->setupUi(this);

    setupModel(root, showNames);

    iconSortModel.setSourceModel(&iconFs);
    ui->iconView->sortByColumn(IconAndName, Qt::AscendingOrder);

    ui->iconView->setModel(&iconSortModel);
    ui->iconView->setWindowTitle(tr("Select Icon"));
    ui->iconView->setColumnHidden(Path, true);  // hide pathnames
}

IconSelector::~IconSelector()
{
    delete ui;
}

void IconSelector::setCurrentPath(const QString& path)
{
    Util::OpenToMatch(iconSortModel, *ui->iconView, [&path](const QModelIndex& idx) {
        return idx.model() != nullptr &&
                idx.model()->data(idx.model()->sibling(idx.row(), Path, idx), Qt::DisplayRole).toString() == path;
    });
}

void IconSelector::setupModel(const QString& root, bool showNames)
{
    // QFilesystemModel doesn't work on resources: https://bugreports.qt.io/browse/QTBUG-7010
    // Instead, we build a simple model ourselves.

    QDirIterator it(root, QDirIterator::Subdirectories);
    QStandardItem* parent = iconFs.invisibleRootItem();

    iconFs.setColumnCount(_Count);

    int depth = -1;

    while (it.hasNext()) {
        const QString& file = it.next();
        const int newDepth = file.count('/');

        while (depth >= 0 && newDepth < depth) {
            parent = parent->parent() == nullptr ? iconFs.invisibleRootItem() : parent->parent();
            --depth;
        }

        if (it.fileInfo().isDir()) {
            QStandardItem* dir = new QStandardItem(QFileInfo(file).fileName());
            parent->appendRow(dir);
            parent = dir;
        }

        if (it.fileInfo().isFile()) {
            parent->appendRow({ new QStandardItem(QIcon(file), showNames ? QFileInfo(file).baseName() : ""),
                                new QStandardItem(file)
                              });

            const QStandardItem* iconItem = parent->child(parent->rowCount() - 1, IconAndName);

            // TODO: we should get the size from somewhere.
            static const QSize iconPad(5, 5);
            const QSize querySize = ui->iconView->iconSize();
            const QSize iconSize = iconItem->data(Qt::DecorationRole).value<QIcon>().actualSize(querySize);

            parent->child(parent->rowCount() - 1, IconAndName)->setData(iconSize + iconPad, Qt::SizeHintRole);
        }

        depth = newDepth;
    }
}

void IconSelector::showEvent(QShowEvent* event)
{
    QDialog::showEvent(event);
    m_icon = QIcon();
    m_iconFile = QString();
}

void IconSelector::on_fromFile_pressed()
{
   const auto formats = QImageReader::supportedImageFormats();

   QString filter = "Icons (";
   for (const auto& f : formats)
       filter.append("*." + f + " ");
   filter.append(");;All (*)");

   // TODO: get import dir from main window or somewhere
   const QString file =
           QFileDialog::getOpenFileName(this, tr("Select icon file"), "~", filter,
                                        nullptr, QFileDialog::ReadOnly);

   if (file.isEmpty())
       return;

   m_iconFile = file;
   m_icon = QIcon(file);
   accept();
}

QIcon IconSelector::icon() const
{
    if (!m_icon.isNull()) // from file selection
        return m_icon;

    if (const QModelIndex idx = ui->iconView->currentIndex(); idx.isValid())
        return iconSortModel.data(idx, Qt::DecorationRole).value<QIcon>();
    return QIcon();
}

QString IconSelector::iconFile() const
{
    if (!m_iconFile.isNull()) // from file selection
        return m_iconFile;

    if (const QModelIndex idx = ui->iconView->currentIndex(); idx.isValid())
        return iconSortModel.data(iconSortModel.sibling(idx.row(), Path, idx), Qt::DisplayRole).value<QString>();

    return QString();
}
