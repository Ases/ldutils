/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QUrl>

#include "linkdialog.h"
#include "ui_linkdialog.h"

LinkDialog::LinkDialog(QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::LinkDialog)
{
    ui->setupUi(this);
}

LinkDialog::~LinkDialog()
{
    delete ui;
}

void LinkDialog::setUrl(const QString& url)
{
    ui->urlText->setText(url);
}

void LinkDialog::setText(const QString& text)
{
    ui->linkText->setText(text);
}

QString LinkDialog::url() const
{
    return ui->urlText->text();
}

QString LinkDialog::text() const
{
    return ui->linkText->text();
}

void LinkDialog::on_filesystemLink_clicked()
{
    const QString localFile =
            QFileDialog::getOpenFileName(this, tr("Select file"), "~", "",
                                         nullptr, QFileDialog::ReadOnly);

    if (localFile.isEmpty())
        return;

    ui->urlText->setText(QUrl::fromLocalFile(localFile).url());
}
