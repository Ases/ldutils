/*
    Copyright 2018 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QUrl>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDir>

#include <src/ui/widgets/textbrowser.h>
#include <src/util/ui.h>

#include "docdialogbase.h"
#include "ui_docdialogbase.h"

DocDialogBase::DocDialogBase(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DocDialogBase),
    tocModel(this),
    textBrowser(new TextBrowser(this)),
    searchError(false)
{
    ui->setupUi(this);

    setupTextBrowser();
    setupSearchPaths();
}

DocDialogBase::~DocDialogBase()
{
    delete ui;
}

void DocDialogBase::setupTextBrowser()
{
    ui->horizontalLayout->addWidget(textBrowser);
    ui->horizontalLayout->setStretch(0, 15);
    ui->horizontalLayout->setStretch(1, 75);

    ui->toc->setFocus(Qt::OtherFocusReason);
}

// Doc search locations.
void DocDialogBase::setupSearchPaths()
{
    QStringList paths;

    for (QString dir : { Util::IsLightTheme() ? "docs/light" : "docs/dark", "docs" }) {
        paths.append(QStandardPaths::locateAll(QStandardPaths::AppDataLocation, dir, QStandardPaths::LocateDirectory));

        // Also search application directory
        if (const QDir appDir(QApplication::applicationDirPath() + QDir::separator() + dir); appDir.exists())
            paths.append(appDir.path());
    }

    textBrowser->setSearchPaths(paths);
}

void DocDialogBase::setupTOC()
{
    tocModel.setHorizontalHeaderLabels({ "Section" });

    QStandardItem* item = nullptr;
    QVector<QStandardItem*> root = { tocModel.invisibleRootItem() };
    int prevIndent = 0;
    int entry = 0;  // index into pageInfo vector

    for (const auto& page : pageInfo) {
        if (page.indent > prevIndent)
            root.append(item);
        else if (page.indent < prevIndent)
            root.removeLast();

        prevIndent = page.indent;

        item = new QStandardItem(page.heading);
        item->setData(entry++, Qt::UserRole);

        root.back()->appendRow(item);
    }

    ui->toc->setModel(&tocModel);
    ui->toc->expandAll();

    setupSignals();
}

void DocDialogBase::setupSignals()
{
    connect(ui->toc->selectionModel(), &QItemSelectionModel::currentChanged, this, &DocDialogBase::changePage);
    connect(ui->toc, &QTreeView::clicked, this, &DocDialogBase::changePage);
}

void DocDialogBase::changePage(const QModelIndex& idx)
{
    if (searchError) // don't spam the error message.
        return;

    if (const int page = tocModel.data(idx, Qt::UserRole).toInt(); page >= 0 && page < pageInfo.size()) {
        if (const char* resource = pageInfo.at(page).resource; resource != nullptr)
            textBrowser->setSource(QUrl(resource));

        if (textBrowser->document()->isEmpty()) {
            QMessageBox::critical(this, tr("Document Error"),
                                  tr("Unable to find documentation files.  Please verify the installation "
                                     "and restart the program.  The current documentation search path is:\n") +
                                  "\n   " +
                                  QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).join("\n   "),
                                  QMessageBox::Ok);

            searchError = true;
        }
    }
}
