/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONSELECTOR_H
#define ICONSELECTOR_H

#include <QDialog>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

namespace Ui {
class IconSelector;
}

class IconSelector final : public QDialog
{
    Q_OBJECT

public:
    explicit IconSelector(const QString& root, bool showNames = true, QWidget *parent = nullptr);
    ~IconSelector() override;

    void setCurrentPath(const QString& path); // initially selected item

    QIcon icon() const;
    QString iconFile() const;

private slots:
    void on_fromFile_pressed();

private:
    enum {
        IconAndName = 0,
        Path,
        _Count,
    };

    void setupModel(const QString& root, bool showNames);
    void showEvent(QShowEvent *event) override;

    // QFilesystemModel doesn't work on resources: https://bugreports.qt.io/browse/QTBUG-7010
    Ui::IconSelector*     ui;
    QStandardItemModel    iconFs;
    QSortFilterProxyModel iconSortModel;
    QIcon                 m_icon;
    QString               m_iconFile;
};

#endif // ICONSELECTOR_H
