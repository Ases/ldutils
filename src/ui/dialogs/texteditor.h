/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QDialog>
#include <QMenu>
#include <QTextListFormat>
#include <src/ui/dialogs/linkdialog.h>
#include <src/ui/dialogs/imagelinkdialog.h>

class QTextCharFormat;

namespace Ui {
class TextEditor;
}

// Credit: Some of this code borrows heavily from the Qt "textedit" example shipped with qtcreator.
class TextEditor : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditor(QWidget *parent = nullptr);
    ~TextEditor();

    void setHtml(const QString& s);
    QString toHtml() const;

private slots:
    void showContextMenu(const QPoint&);
    void currentCharFormatChanged(const QTextCharFormat &format);
    void cursorPositionChanged();
    void clipboardDataChanged();
    void textFamily(int index);
    void textSize(int index);
    void textColor();
    void resetText();

    void on_action_Bold_triggered();
    void on_action_Italic_triggered();
    void on_action_Underline_triggered();
    void on_action_Text_Color_triggered();
    void on_action_Align_Left_triggered();
    void on_action_Align_Right_triggered();
    void on_action_Align_Center_triggered();
    void on_action_Align_Fill_triggered();
    void on_action_Cut_triggered();
    void on_action_Copy_triggered();
    void on_action_Paste_triggered();
    void on_action_Undo_triggered();
    void on_action_Redo_triggered();
    void on_action_Insert_Link_triggered();
    void on_action_Insert_Image_triggered();
    void on_action_Format_Ordered_List_triggered(bool checked);
    void on_action_Format_Unordered_List_triggered(bool checked);

private:
    void setupActionIcons();
    void setupSignals();
    void setupActions();
    void setupFontSizes();
    void setupUi();
    void setupMenus();

    void mergeFormatOnSelection(const QTextCharFormat &format);
    void alignmentChanged();
    void colorChanged();
    void fontChanged();
    void styleChanged();
    void setListFormat(bool apply, QTextListFormat::Style);

    Ui::TextEditor *ui;
    QString         original;
    LinkDialog      linkDialog;
    ImageLinkDialog imageLinkDialog;
    QMenu           editorMenu;
};

#endif // TEXTEDITOR_H
