/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "aboutbase.h"

#include <cassert>
#include <QTabWidget>
#include <QAction>
#include <QTextBrowser>

#include <src/util/icons.h>
#include <src/util/ui.h>

void AboutBase::setup(QTabWidget* tabWidget, QAction* next, QAction* prev)
{
    tabs = tabWidget;
    setupActionIcons(next, prev);

    addAction(next);
    addAction(prev);

    connect(next, &QAction::triggered, this, &AboutBase::nextTab);
    connect(prev, &QAction::triggered, this, &AboutBase::prevTab);

    // Open external links in  QDesktopServices::openUrl()
    for (auto* browser : tabWidget->findChildren<QTextBrowser*>(QString(), Qt::FindChildrenRecursively))
        browser->setOpenExternalLinks(true);
}

void AboutBase::setupActionIcons(QAction* next, QAction* prev)
{
    Icons::defaultIcon(next, "go-next");
    Icons::defaultIcon(prev, "go-previous");

    int tab = 0;
    Icons::defaultIcon(tabs, tab++, "help-about");
    Icons::defaultIcon(tabs, tab++, "author");
    Icons::defaultIcon(tabs, tab++, "file-library-symbolic");
    Icons::defaultIcon(tabs, tab++, "license");
    Icons::defaultIcon(tabs, tab++, "network-disconnect");
    Icons::defaultIcon(tabs, tab++, "help-donate");
}

void AboutBase::prevTab()
{
    Util::PrevTab(tabs);
}

void AboutBase::nextTab()
{
    Util::NextTab(tabs);
}

void AboutBase::showTab(Tab tab)
{
    assert(int(tab) < tabs->count());

    show();
    tabs->setCurrentIndex(int(tab));
}
