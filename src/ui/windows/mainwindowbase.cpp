/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QInputDialog>
#include <QStatusBar>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QStandardPaths>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/ui/panes/panebase.h>
#include <src/ui/widgets/tabwidget.h>
#include <src/core/cfgdatabase.h>
#include <src/core/undowincfg.h>

#include "mainwindowbase.h"

MainWindowBase::MainWindowBase(const char* apptitle, const char* initialSettingsFile, QWidget *parent) :
    QMainWindow(parent),
    apptitle(apptitle),
    saveOnExit(true),
    firstExecution(true),
    startupFontSize(QApplication::font().pointSize()),
    m_undoMgr(*this),
    uiTabs(nullptr),
    postLoadFocus(nullptr),
    errorDialog(this),
    lastSettingsFile(initialSettingsFile),
    defaultSettingsDir(QDir::home().absolutePath()),
    postLoadTimer(this),
    m_nextPaneId(0),
    modFlag(false),
    loadError(false)
{
    setupDefaults();
    setupTimers();
    restoreRecentSessions();  // load recent sessions, if available
}

MainWindowBase::~MainWindowBase()
{
}

void MainWindowBase::cleanup()
{
    // Avoid attempting to reset focus on the way out.
    disconnect(reinterpret_cast<QGuiApplication*>(QGuiApplication::instance()),
               &QGuiApplication::focusObjectChanged,
               this, &MainWindowBase::newFocus);

    // Delete the tabs BEFORE we tear down the MainWindow, so destructors can use some of
    // our data on their way out.  Otherwise, the QWidget does this, by which time we're gone.
    if (auto* tabs = mainWindowTabs())
        tabs->deleteTabs(false);
}

void MainWindowBase::setupSignals()
{
    // notify of focus changes
    connect(reinterpret_cast<QGuiApplication*>(QGuiApplication::instance()),
            &QGuiApplication::focusObjectChanged,
            this, &MainWindowBase::newFocus);
}

void MainWindowBase::setupDefaults()
{
    errorDialog.resize(500, 250);  // default size for error dialog
}

void MainWindowBase::setupAppConfig()
{
    QApplication::setApplicationDisplayName(apptitle);

    errorDialog.setWindowTitle("Error");
}

void MainWindowBase::setupActionTooltips()
{
    Util::setupActionTooltips(this);
}

void MainWindowBase::setupTimers()
{
    postLoadTimer.setSingleShot(true);
    connect(&postLoadTimer, &QTimer::timeout, this, &MainWindowBase::postLoadHook, Qt::QueuedConnection);
}

void MainWindowBase::firstRunHook()
{
    firstExecution = false;
}

// The UI is not displayed yet on first load.  This is a workaround: a hook to be
// called after it is, to set up e.g, menu check states depending on some UI visibility.
void MainWindowBase::postLoadHook()
{
    if (!isVisible())  // in some environments, this happens before the main window is loaded
        return;

    if (firstExecution)
        firstRunHook();  // potentially show first-execution intro

    activateWindow();
    raise();

    // Focus can't be set during UI construction.
    if (postLoadFocus != nullptr)
        postLoadFocus->setFocus();

    // Let panes run their own post load hook
    runOnPanels([](PaneBase* pane) { pane->postLoadHook(); });
}

// track newly focused pane in the prevFocus deque
PaneBase* MainWindowBase::trackFocus(PaneBase* newFocus)
{
    static const int maxFocusDequeSize = 8;

    if (newFocus == nullptr)
        return nullptr;

    // Push it on the focus deque, unless it's already at the end.
    if (prevFocus.empty() || prevFocus.back() != newFocus)
        prevFocus.push_back(newFocus);

    while (prevFocus.size() > maxFocusDequeSize)
        prevFocus.pop_front();

    return newFocus;
}

PaneBase* MainWindowBase::focusedPane(QObject* focused) const
{
    // If not given an explicit object, use the prior focus
    if (focused == nullptr && !prevFocus.empty())
        return prevFocus.back();

    // look up the chain until we run out, or find a class we understand.
    while (focused != nullptr) {
        PaneBase* panel = dynamic_cast<PaneBase*>(focused);
        if (panel != nullptr)
            return panel;

        focused = focused->parent();
    }

    return nullptr;
}

PaneBase* MainWindowBase::focusedPane(PaneBase* pane) const
{
    if (pane != nullptr)
        return pane;

    return focusedPane();
}

PaneBase* MainWindowBase::focusedPaneWarn(QObject *focused) const
{
    if (PaneBase* pane = focusedPane(focused))
        return pane;

    statusMessage(UiType::Info, tr("No container pane focused. Please click on one and try again."));

    return nullptr;
}

PaneBase* MainWindowBase::focusedPaneWarn(PaneBase* pane) const
{
    if (pane != nullptr)
        return pane;

    return focusedPaneWarn();
}

void MainWindowBase::setFontSize(int newSize)
{
    QFont font = QApplication::font();

    font.setPointSize(Util::Clamp(newSize, 5, 35));
    QApplication::setFont(font);

    // TODO: This is a workaround for an unknown problem where the main window won't
    // repaint the font changes unless something triggers a relayout(?).
    QFrame(this).show();

    // Fiddle with columns so they aren't truncated.
    resizeColumnsAllPanes();
}

void MainWindowBase::changeFontSize(float mul)
{
    QFont font = QApplication::font();

    int newSize = int(float(QApplication::font().pointSizeF()) * mul);

    if (newSize == font.pointSize())
        newSize += (mul > 1.0f) ? 1 : -1;

    setFontSize(newSize);
}

// We use the TabWidget as our secondary top level window class
TabWidget* MainWindowBase::newWindow()
{
    TabWidget* tabBar = new TabWidget(*this);
    tabBar->setWindowFlags(Qt::Window);
    tabBar->setAttribute(Qt::WA_DeleteOnClose);
    tabBar->show();

    return tabBar;
}

// For interactive (menu) use: create new window and give it default contents.
void MainWindowBase::newWindowInteractive(PaneBase* pane)
{
    TabWidget* tabBar = newWindow();

    if (pane == nullptr)
        pane = dynamic_cast<PaneBase*>(paneFactory());

    if (pane != nullptr) {
        tabBar->addTab("Main", pane);
        pane->setFocus();
    }

    tabBar->resize(512, 256);
}

// Clean up UI pane structure (remove empty groups, etc)
void MainWindowBase::cleanStructure()
{
    for (auto& row : findChildren<PaneBase::Container*>(PaneBase::containerName)) {
        if (row->count() == 0) {  // Remove empty groups
            row->deleteLater();
            break;
        } else {
            if (row->count() == 1) {
                QWidget* child = row->widget(0);
                if (child == nullptr)
                    continue;

                // If a group has one child, replace group in its own parent with that child.
                if (PaneBase::Container* gp = dynamic_cast<PaneBase::Container*>(row->parent()); gp != nullptr) {
                    if (const int index = gp->indexOf(row); index >= 0) {
                        gp->replaceWidget(index, child);
                        child->show();
                        row->deleteLater();
                    }
                } else if (PaneBase::Container* cc = dynamic_cast<PaneBase::Container*>(child); cc != nullptr) {
                    // special case: if this is already the root node, there's no more parent,
                    // so add child's children to ourselves and remove that child.
                    cc->deleteLater();       // remove ourselves
                    cc->setParent(nullptr); // remove us from parent group (for size preservation)
                    moveChildren(row, cc);
                }
            }
        }
    }
}

// Move child items of a pane container to a differnent container, cloning the properties
// of the original (sizes and orientation).
void MainWindowBase::moveChildren(PaneBase::Container* moveTo, PaneBase::Container* moveFrom)
{
    const auto oldSizes = moveFrom->sizes(); // grab sizes before removing children

    // Move childen to new container, preserving order.
    while (moveFrom->count() > 0) {
        if (QWidget* child = moveFrom->widget(0); child != nullptr) {
            child->setParent(moveTo);
            child->show();
        }
    }

    moveTo->setOrientation(moveFrom->orientation()); // clone orientation of parent
    moveTo->setSizes(oldSizes);
}

TabWidget* MainWindowBase::mainWindowTabs()
{
    if (uiTabs != nullptr)  // return cached one
        return uiTabs;

    if (auto tabs = findChildren<TabWidget*>(); !tabs.isEmpty())
        return uiTabs = tabs.front();

    return nullptr;
}

const TabWidget* MainWindowBase::mainWindowTabs() const
{
    return const_cast<const TabWidget*>(const_cast<MainWindowBase*>(this)->mainWindowTabs());
}

// Update focus on pane removal.
void MainWindowBase::paneRefocus(PaneBase* deletedPane, PaneBase* newFocus)
{
    prevFocus.removeAll(deletedPane); // remove from prior focus

    // If no explicit new focus, get from prev focus list, if possible.
    if (newFocus == nullptr)
        newFocus = findPane<PaneBase>();

    // Focus the new pane
    if (newFocus != nullptr)
        newFocus->setFocus();

    // If we landed on the new-tab tab, pick another one.
    if (auto* tabs = mainWindowTabs()) {
        const int currentTab = tabs->currentIndex();
        if (currentTab == (tabs->count()-1))
            if (currentTab > 0)
                tabs->setCurrentIndex(currentTab - 1);
    }
}

PaneBase* MainWindowBase::replacePane(PaneBase* newPane, PaneBase* oldPane)
{
    if (oldPane == nullptr) {
        delete newPane;
        return nullptr;
    }

    PaneBase::Container* row = dynamic_cast<PaneBase::Container*>(oldPane->parent());

    if (row == nullptr) {
        delete newPane;
        return nullptr;
    }

    const int colIndex = row->indexOf(oldPane);

    row->replaceWidget(colIndex, newPane);

    paneRefocus(oldPane, newPane);

    oldPane->deleteLater();

    statusMessage(UiType::Info, tr("Replaced pane: ") + oldPane->objectName() + " -> " +
                  newPane->objectName());

    return newPane;
}

void MainWindowBase::removePane(PaneBase* pane)
{
    if (pane == nullptr)
        return;

    paneRefocus(pane);

    pane->setParent(nullptr);
    pane->deleteLater();

    statusMessage(UiType::Info, tr("Removed pane: ") + pane->objectName());

    cleanStructure();
}

// This can move either panes or containers, so it accepts a QWidget;
void MainWindowBase::movePane(QWidget* pane, int relPos)
{
    if (pane == nullptr || relPos == 0)
        return;

    PaneBase::Container* row = dynamic_cast<PaneBase::Container*>(pane->parent());

    if (row == nullptr)
        return;

    const int idx = row->indexOf(pane);

    // Can't move off ends
    if (idx < 0 || (idx == 0 && relPos < 0) || (idx >= (row->count()-1) && relPos > 0))
        return;

    pane->setParent(nullptr);       // remove from where it was

    // New index
    row->insertWidget(idx + relPos, pane); // add to where it goes

    // Refocus it (it became unfocued on removal)
    if (dynamic_cast<PaneBase*>(pane) != nullptr)
        pane->setFocus();
}

void MainWindowBase::movePaneParent(PaneBase* pane, int relPos)
{
    if (pane == nullptr)
        return;

    PaneBase::Container* row = dynamic_cast<PaneBase::Container*>(pane->parent());
    if (row == nullptr)
        return;

    movePane(row, relPos);
}

void MainWindowBase::resizeColumnsAllPanes()
{
    runOnPanels([](PaneBase* pane) {
        pane->resizeToFit();
    });
}

void MainWindowBase::changeStatusBarColor(const QColor &color) const
{
    statusBar()->setStyleSheet("QStatusBar { color: " + color.name() + ";}");
}

void MainWindowBase::changeStatusBarColor(QPalette::ColorRole role) const
{
    changeStatusBarColor(QGuiApplication::palette().color(role));
}

void MainWindowBase::balanceSiblingsInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Balance Siblings"));

    if (PaneBase* focused = focusedPaneWarn())
        if (PaneBase::Container* row = dynamic_cast<PaneBase::Container*>(focused->parent()))
            TabWidget::balanceChildren(row);
}

void MainWindowBase::newFocus(QObject* focus)
{
    if (PaneBase* focusPane = focusedPane(focus); focusPane != nullptr) {
        if (!prevFocus.empty()) {
            if (focusPane == prevFocus.back())  // spam prevention.
                return;

            prevFocus.back()->focusOut();
        }

        focusPane->focusIn();

        // Need to change code just below if Pane::Superclass changes
        // Damnit, we want a static_assert here, but typeid is not constexpr.
        assert(typeid(PaneBase::Superclass) == typeid(QGroupBox));

        // Set previous focus item title to non-bold
        if (!prevFocus.empty())
            if (PaneBase::Superclass* gb = dynamic_cast<PaneBase::Superclass*>(prevFocus.back()))
                gb->setStyleSheet("QGroupBox { font-weight: normal; } ");

        // Set current focus item title to bold
        if (focus) {
            if (PaneBase::Superclass* gb = dynamic_cast<PaneBase::Superclass*>(focusPane))
                gb->setStyleSheet("QGroupBox { font-weight: bold; } ");
        }

        trackFocus(focusPane);

        if (focusPane->selectionModel() != nullptr) {
            currentChanged(focusPane->selectionModel()->currentIndex());
            selectionChanged(focusPane->selectionModel(),
                             focusPane->selectionModel()->selection(), QItemSelection());
        }
    } else {
        currentChanged(QModelIndex());
        selectionChanged(nullptr, QItemSelection(), QItemSelection());
    }
}

// Backup file in fname.  Returns true on success.  Keeps "count" numbered backups as [1..count]
bool MainWindowBase::backupFile(const QString& fname, int count)
{
    const auto backup = [&fname](int n) {
        return (n == 0) ? QFile(fname) : QFile(fname + ".~" + QString::number(n) + "~");
    };

    if (count == 0 || !backup(0).exists())
        return true;

    // If there's an open slot, use it (fill out the count)
    for (int i = 1; i <= count; ++i)
        if (!backup(i).exists())
            return backup(0).rename(backup(i).fileName());

    // Too many: we have to remove one.
    // Keep N numbered backups as [1..N], with most recent as biggest N.
    // Move old [2..N] to be new [1..N-1].
    for (int i = 1; i < count; ++i) {
        if (backup(i+1).exists()) {
            if (backup(i+0).exists() && !backup(i+0).remove())
                return false;
            if (!backup(i+1).rename(backup(i+0).fileName()))
                return false;
        }
    }

    return backup(0).rename(backup(count).fileName());
}

QString MainWindowBase::currentSettingsDirectory() const
{
    return QFileInfo(lastSettingsFile).dir().path();
}

QString MainWindowBase::currentSettingsFile() const
{
    return lastSettingsFile;
}

QString MainWindowBase::appConfigDir() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
}

QString MainWindowBase::appDataDir() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString MainWindowBase::recentSessionsFile() const
{
    return appConfigDir() + QDir::separator() + "RecentSessions.conf";
}

void MainWindowBase::restoreRecentSessions()
{
    // Restore list
    QSettings settings(recentSessionsFile(), QSettings::IniFormat, this);
    MemberLoad(settings, recentSessions);

    // If no settings supplied externally, use most recent one from recentSessions
    if (!hasSettingsFile() && !recentSessions.isEmpty())
        lastSettingsFile = recentSessions.front();

    // If none from recentSessions, use default as last resort
    if (!hasSettingsFile())
        lastSettingsFile = appDataDir() + QDir::separator() + QApplication::applicationName() + ".conf";

    addSession(lastSettingsFile, false);

    recentSessionsChanged();  // let subclasses update
}

void MainWindowBase::loadRecentSession(QAction* action)
{
    // Short-circuit if no session change to avoid reload.
    if (currentSettingsFile() == action->iconText())
        return;

    uiLoad(action->iconText());
}

void MainWindowBase::saveRecentSessions()
{
    QSettings settings(recentSessionsFile(), QSettings::IniFormat, this);
    MemberSave(settings, recentSessions);

    if (settings.status() != QSettings::NoError)
        error(tr("Unable to save recent sesssions"), tr("Save Sessions"));
}

void MainWindowBase::recentSessionsChanged(QMenu* recentSessionsMenu)
{
    if (recentSessionsMenu == nullptr)
        return;

    recentSessionsMenu->clear();

    // Only add entries that exist on the filesystem
    for (const auto& session : recentSessions)
        if (QFile(session).exists())
            if (QAction* action = recentSessionsMenu->addAction(Icons::get("document-open"), session); action != nullptr)
                action->setIconText(session);
}

void MainWindowBase::addSession(const QString& file, bool save)
{
    static const int recentSessionCount = 5;

    defaultSettingsDir = QFileInfo(file).path();
    lastSettingsFile   = QDir::toNativeSeparators(file);

    if (int index = recentSessions.indexOf(file); index >= 0) {
        // If recentSessions already contains the session file, bump it to the top.
        if (index == 0)  // list wouldn't change
            return;

        recentSessions.move(index, 0);
    } else {
        // Add new item at beginning, and trim list size.
        recentSessions.prepend(file);

        while (recentSessions.size() > recentSessionCount)
            recentSessions.pop_back();
    }

    if (save) {
        saveRecentSessions();
        recentSessionsChanged(); // let subclasses update
    }
}

void MainWindowBase::removeSession(const QString& file, bool save)
{
    if (lastSettingsFile == file)
        lastSettingsFile.clear();

    if (recentSessions.removeAll(file) == 0)
        return;

    if (save) {
        saveRecentSessions();
        recentSessionsChanged();
    }
}

void MainWindowBase::sessionSave()
{
    if (!hasSettingsFile())
        return;

    // Ensure directory containing lastSettingsFile exists.
    const QString dirname = currentSettingsDirectory();

    if (!QDir::root().mkpath(dirname)) {
        QMessageBox::critical(this, tr("Save Error"),
                              tr("Error creating directory for settings file: ") + dirname,
                              QMessageBox::Ok);

        return;
    }

    // Backup old one
    backupFile(currentSettingsFile(), cfgData().backupUICount);

    QSettings settings(currentSettingsFile(), QSettings::IniFormat, this);
    save(settings);

    if (settings.status() != QSettings::NoError) {
        QMessageBox::critical(this, tr("Save Error"),
                              tr("Error saving settings: ") + currentSettingsFile(),
                              QMessageBox::Ok);
    }
}

void MainWindowBase::sessionRestore()
{
    QSettings settings(currentSettingsFile(), QSettings::IniFormat, this);
    setLoadedFile(currentSettingsFile());
    load(settings);
}

void MainWindowBase::closeEvent(QCloseEvent* event)
{
    if (cfgData().warnOnExit) {
        if (warningDialog(tr("Quit") + apptitle, tr("Quit application?")) == QMessageBox::Cancel) {
            event->ignore();
            return;
        }
    }

    if (saveOnExit)
        sessionSave();

    closeSecondaryWindows();
    QApplication::closeAllWindows();

    // For an unknown reason, on certain distros such as OpenSUSE, QGuiApplication::quitOnLastWindowClosed
    // is not causing the app to exit after the last window has been closed.  This forces the issue.
    // TODO: figure out why the normal quitOnLastWindowClosed handling isn't working.
    qApp->quit();
}

void MainWindowBase::showEvent(QShowEvent* event)
{
    QMainWindow::showEvent(event);

    // In some environments, the window isn't displayed even after the call to QMainWindow::showEvent(),
    // so we do this on a small timer so the window is displayed.
    postLoadTimer.start(500);
}

void MainWindowBase::statusMessage(UiType ml, const QString& txt) const
{
    if (ml < UiType::_Begin || ml >= UiType::_Count)
        return;

    int timeout;

    // default timeouts per message level
    switch (ml) {
    case UiType::Warning:  timeout =  5000; break;
    case UiType::Error:    timeout = 10000; break;
    case UiType::Critical: timeout = 15000; break;
    default:               timeout =  3000; break;
    }

    statusBar()->showMessage(txt, timeout);
    changeStatusBarColor(cfgData().uiColor[ml]);
}

void MainWindowBase::error(const QString& message, const QString& type) const
{
    errorDialog.showMessage(message, type);
    statusMessage(UiType::Error, message);
}

// Display unimplemented warning
void MainWindowBase::unimplemented()
{
    QMessageBox::warning(this, "Unimplemented",
                         "This feature is currently unimplemented.  Check back later!",
                         QMessageBox::Ok);
}

PaneBase::Container* MainWindowBase::addPane(PaneBase::Container* toAdd, PaneBase::Container* row, int before)
{
    if (toAdd == nullptr || row == nullptr)
        return nullptr;

    row->insertWidget(before, toAdd);

    return toAdd;
}

PaneBase* MainWindowBase::addPane(PaneBase* toAdd, PaneBase::Container* row, bool before, PaneBase* refPane)
{
    if (toAdd == nullptr)
        return nullptr;

    if (row == nullptr && refPane != nullptr)
        row = dynamic_cast<PaneBase::Container*>(refPane->parent());

    if (row == nullptr) {
        delete toAdd;  // because we adopt it
        return nullptr;
    }

    int colIndex;

    if (refPane == nullptr) {
        colIndex = before ? 0 : row->count();
    } else {
        colIndex = row->indexOf(refPane);
    }

    if (!before)
        ++colIndex;

    row->insertWidget(colIndex, toAdd);

    return toAdd;
}
void MainWindowBase::splitPaneInteractive(PaneBase* pane, Qt::Orientation orientation)
{
    if (pane == nullptr)
        return;

    PaneBase::Container* parent = dynamic_cast<PaneBase::Container*>(pane->parent());
    if (parent == nullptr)
        return;

    PaneBase* newPane = pane->clone();
    if (newPane == nullptr)
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Split Pane") + " " +
                                         (orientation == Qt::Horizontal ? tr("Horizontally") : tr("Vertically")));

    // Same orientation: Just add to that group
    if (parent->orientation() == orientation) {
        addPane(newPane, parent, true, pane);
        return;
    }

    // If we get here, we are going to split in the other orientation (H in a V group,
    // or V in an H group).
    auto* newContainer = containerFactory();

    if (newContainer == nullptr) {
        delete newPane;
        return;
    }

    newContainer->setOrientation(orientation);

    const int location = parent->indexOf(pane);

    parent->replaceWidget(location, newContainer);
    addPane(pane, newContainer);
    addPane(newPane, newContainer);

    statusMessage(UiType::Info, tr("Split pane: ") + pane->objectName());
}

PaneBase* MainWindowBase::addGroupSibling(PaneBase* toAdd, PaneBase::Container* parent,
                                          bool before, PaneBase* refPane)
{
    if (toAdd == nullptr)
        return nullptr;

    if (parent == nullptr && refPane != nullptr)
        parent = dynamic_cast<PaneBase::Container*>(refPane->parent());

    if (parent == nullptr) {
        delete toAdd;  // because we adopt it
        return nullptr;
    }

    PaneBase::Container* grandparent = dynamic_cast<PaneBase::Container*>(parent->parent());

    // Grandparent may be a group, or a tab
    if (grandparent == nullptr) {
        // grandparent is tab: change its group to other orientation and add its children to a new group.
        PaneBase::Container* newGroup = containerFactory();
        if (newGroup == nullptr) {
            delete toAdd;
            return nullptr;
        }

        moveChildren(newGroup, parent);

        // Add the cloned group, and the new widget.
        if (before)  parent->addWidget(toAdd);
        parent->addWidget(newGroup);
        if (!before) parent->addWidget(toAdd);

        // Reverse orientation of parent.
        parent->setOrientation(parent->orientation() == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);
    } else {
        // grandparent is a group: just add to that.
        addPane(toAdd, grandparent, before, refPane);
    }

    return toAdd;
}

void MainWindowBase::addPaneAction(QAction *action)
{
    PaneBase* refPane = focusedPane();
    PaneBase::Container* row = nullptr;

    if (refPane == nullptr) { // No reference. Find a row to add to.
        row = findChild<PaneBase::Container*>();
        if (row == nullptr) {
            statusMessage(UiType::Warning, tr("No active pane found"));
            return;
        }
    }

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Add New Pane"));

    const int idx = paneAddActions.indexOf(action);

    // Create and focus a new pane
    if (PaneBase* newPane = addPane(dynamic_cast<PaneBase*>(paneFactory(idx)), row, true, refPane))
        newPane->setFocus();
}

void MainWindowBase::addGroupAction(QAction *action)
{
    PaneBase* refPane = focusedPaneWarn();
    if (refPane == nullptr)
        return;

    PaneBase::Container* row = dynamic_cast<PaneBase::Container*>(refPane->parent());
    if (row == nullptr)  // unable to find parent container
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Add Group Sibling"));

    const int idx = paneClassGrpActions().indexOf(action);

    // Add group sibling and focus it.
    if (PaneBase* newPane = addGroupSibling(dynamic_cast<PaneBase*>(paneFactory(idx)), row, false, refPane))
        newPane->setFocus();
}

void MainWindowBase::replacePaneAction(QAction *action)
{
    PaneBase* refPane = focusedPaneWarn();
    if (refPane == nullptr)
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Replace Pane"));

    const int idx = paneClassRepActions().indexOf(action);

    // Create and focus a new pane
    if (PaneBase* newPane = replacePane(dynamic_cast<PaneBase*>(paneFactory(idx)), refPane))
        newPane->setFocus();
}

void MainWindowBase::paneInWindowAction(QAction *action)
{
    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Open Pane in New Window"));

    const int idx = paneClassWinActions().indexOf(action);

    newWindowInteractive(dynamic_cast<PaneBase*>(paneFactory(idx)));
}

void MainWindowBase::closeSecondaryWindows()
{
    // Close and delete current secondary windows
    for (auto* window : QApplication::topLevelWidgets()) {
        if (TabWidget* tabWidget = dynamic_cast<TabWidget*>(window)) {
            tabWidget->hide();
            tabWidget->deleteLater();
        }
    }
}

void MainWindowBase::saveWinConfig(QSettings& settings) const
{
    if (const auto* tabs = mainWindowTabs())
        tabs->save(settings);

    // Save secondary windows
    settings.beginWriteArray("windows"); {
        int winId = 0;
        for (const auto* window : QApplication::topLevelWidgets()) {
            if (const TabWidget* tabWidget = dynamic_cast<const TabWidget*>(window)) {
                settings.setArrayIndex(winId++);
                tabWidget->save(settings);
            }
        }
    } settings.endArray();
}

void MainWindowBase::saveUiConfig(QSettings& settings, const std::function<void(void)>& saver) const
{
    // Clear out old settings
    settings.clear();

    // Save window size/position
    settings.beginGroup("MainWindow"); {
        SL::Save(settings, versionKey, version());
        SL::Save(settings, "geometry", saveGeometry());
        SL::Save(settings, "state",    saveState());
        SL::Save(settings, "cfgData",  cfgData());
        MemberSave(settings, defaultSettingsDir);
        MemberSave(settings, firstExecution);

        saver(); // save derived settings if any
        saveWinConfig(settings); // load tab/window layout

        // Save font size
        SL::Save(settings, "fontSize", QApplication::font().pointSize());
    } settings.endGroup();
}

void MainWindowBase::loadWinConfig(QSettings& settings)
{
    // We can't foocus during UI construction, so this is deferred to the postLoadHook
    setPostLoadFocus(nullptr);

    if (auto* tabs = mainWindowTabs())
        tabs->load(settings);

    // Load new secondary windows (after deleting any currently open ones)
    closeSecondaryWindows();
    const int winCount = settings.beginReadArray("windows"); {
        for (int winId = 0; winId < winCount; ++winId) {
            settings.setArrayIndex(winId);
            (newWindow())->load(settings);
        }
    } settings.endArray();

    cleanStructure();
}

void MainWindowBase::loadUiConfig(QSettings& settings, const std::function<void(void)>& loader)
{
    undoMgr().clear(); // undo/redo no longer apply in this new world.

    loadError = false;

    // Load window size/position
    settings.beginGroup("MainWindow"); {
        // only load compatible versions
        if (SL::Load(settings, versionKey, 0) == Settings::version()) {
            if (settings.contains("geometry"))
                restoreGeometry(settings.value("geometry").toByteArray());
            if (settings.contains("state"))
                restoreState(settings.value("state").toByteArray());

            SL::Load(settings, "cfgData", cfgData());
            MemberLoad(settings, defaultSettingsDir);
            MemberLoad(settings, firstExecution);

            loader(); // load derived settings, if any
            loadWinConfig(settings); // load tab/window layout

            // Font size
            if (settings.contains("fontSize"))
                setFontSize(SL::Load(settings, "fontSize", 10));
        } else {
            loadError = true;
        }
    } settings.endGroup();

    paneRefocus();
    newConfig(); // refresh everything from just-loaded config
    postLoadTimer.start(0); // hook to do things after the UI is finally displayed.
}

void MainWindowBase::runOnFocusPane(const std::function<void(PaneBase*)>& fn)
{
    if (PaneBase* pane = focusedPaneWarn())
        fn(pane);
}

void MainWindowBase::uiSave(const QString& file)
{
    if (file.isEmpty()) {
        error(tr("Error: No save file."), tr("Save UI"));
        return;
    }

    addSession(file);

    if (!backupFile(currentSettingsFile(), cfgData().backupUICount))
        error(tr("Error creating settings file backup:<p>") + currentSettingsFile(), tr("Save UI"));

    QSettings settings(file, QSettings::IniFormat, this);
    save(settings);

    if (settings.status() == QSettings::NoError)
        statusMessage(UiType::Info, tr("Saved: ") + file);
    else
        error(tr("Error saving settings:<p>") + file, tr("Save UI"));

    setLoadedFile(currentSettingsFile());
}

void MainWindowBase::uiLoad(const QString& file)
{
    addSession(file);

    QSettings settings(file, QSettings::IniFormat, this);
    load(settings);

    if (settings.status() == QSettings::NoError && !loadError) {
        statusMessage(UiType::Info, tr("Loaded: ") + file);
    } else {
        error(tr("Error loading settings:<p>") + file, tr("Load UI"));
        removeSession(file);
    }

    setLoadedFile(currentSettingsFile());
}

void MainWindowBase::saveSettingsAs()
{
    const QString settingsFile =
            QFileDialog::getSaveFileName(this, tr("Save Configuration"), defaultSettingsDir,
                                         confFileFilter, nullptr);

    if (settingsFile.isEmpty()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    uiSave(settingsFile);
}

void MainWindowBase::saveSettings()
{
    if (!hasSettingsFile() || !QFileInfo(currentSettingsFile()).isWritable())
        return saveSettingsAs();

    uiSave(currentSettingsFile());
}

void MainWindowBase::openSettings()
{
    const QString settingsFile =
            QFileDialog::getOpenFileName(this, tr("Open Configuration"), defaultSettingsDir,
                                         confFileFilter,  nullptr, QFileDialog::ReadOnly);

    if (settingsFile.isEmpty()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    uiLoad(settingsFile);
}

void MainWindowBase::revertSettings()
{
    if (!hasSettingsFile()) {
        statusMessage(UiType::Error, tr("No settings file to revert from"));
        return;
    }

    if (cfgData().warnOnRevert)
        if (warningDialog(tr("Revert settings"),
                          tr("Revert settings from file?") + "\n   " + currentSettingsFile()) != QMessageBox::Ok)
            return;

    const QFileInfo stat(currentSettingsFile());

    if (stat.isReadable())
        uiLoad(currentSettingsFile());
    else
        statusMessage(UiType::Error, tr("Unable to read save file"));
}

int MainWindowBase::warningDialog(const QString& winTitle, const QString& msg)
{
    return Util::WarningDialog(*this, winTitle, msg);
}

void MainWindowBase::setLoadedFile(const QString& file)
{
    const QFileInfo fileInfo(file);

    setWindowTitle(fileInfo.baseName() + "[*]");
}

void MainWindowBase::markModified(bool dirty, bool force)
{
    if (dirty == modFlag && !force)
        return;

    modFlag = dirty;

    setWindowModified(dirty);

    if (force || undoMgr().isDirty() != dirty)
        undoMgr().setDirty(dirty);
}

void MainWindowBase::updateUndoActions(const UndoMgr* mgrToUse, QAction* undo, QAction* redo, QAction* clear)
{
    const int undoCount = (mgrToUse != nullptr) ? mgrToUse->undoCount() : 0;
    const int redoCount = (mgrToUse != nullptr) ? mgrToUse->redoCount() : 0;

    const QString topUndoName = (mgrToUse != nullptr) ? mgrToUse->topUndoName() : "";
    const QString topRedoName = (mgrToUse != nullptr) ? mgrToUse->topRedoName() : "";

    const auto undoTxt = [](const QString& type, const QString& name, int count) {
        return type + " " + name +
                (count > 1 ? QString(tr(" (... %1 more)")).arg(count - 1) : tr(""));
    };

    const auto setUndoName = [](QAction* undo, int count, const QString& name) {
        if (undo != nullptr) {
            undo->setEnabled(count > 0);
            undo->setText(name);      // action text
            undo->setToolTip(name);   // for the toolbar
            undo->setWhatsThis(name);
            undo->setStatusTip(name);
        }
    };

    const QString undoName = undoTxt(tr("Undo"), topUndoName, undoCount);
    const QString redoName = undoTxt(tr("Redo"), topRedoName, redoCount);

    if (clear != nullptr)
        clear->setEnabled(undoCount > 0 || redoCount > 0);

    setUndoName(undo, undoCount, undoName);
    setUndoName(redo, redoCount, redoName);
}
