/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOWBASE_H
#define MAINWINDOWBASE_H

#include <functional>

#include <QMainWindow>
#include <QErrorMessage>
#include <QProgressBar>
#include <QCursor>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QApplication>
#include <src/core/settings.h>
#include <src/core/uicolormodel.h>
#include <src/core/undomgr.h>
#include <src/ui/panes/panebase.h>
#include <src/ui/widgets/tabwidget.h>

class CfgDataBase;
class QCloseEvent;
class QSettings;
class ChangeTrackingModel;

class MainWindowBase : public QMainWindow, public Settings
{
    Q_OBJECT

public:
    explicit MainWindowBase(const char* apptitle, const char* initialSettingsFile, QWidget *parent = nullptr);
    virtual ~MainWindowBase() override;

    virtual const CfgDataBase& cfgData() const = 0;
    virtual UndoMgr& undoMgr() { return m_undoMgr; }

    const QList<QAction*>& paneClassAddActions() const { return paneAddActions; }
    const QList<QAction*>& paneClassGrpActions() const { return paneGrpActions; }
    const QList<QAction*>& paneClassRepActions() const { return paneRepActions; }
    const QList<QAction*>& paneClassWinActions() const { return paneWinActions; }

    virtual QAction* getPaneAction(PaneAction cc) const = 0;

    // Find pane matching given class P, and the predicate if supplied.
    template <typename P> P* findPane(const std::function<bool(P*)>& = [](P*) { return true; }) const; // search prior focus list first, then all.

    // Factory for UI panes
    virtual QWidget* paneFactory(int paneClass = -1) const = 0;
    virtual PaneBase::Container* containerFactory() const = 0;

    // Push this on the stack to save and restore the cursor.
    class SaveCursor {
    public:
        SaveCursor(MainWindowBase& mainWindow, Qt::CursorShape shape) :
            mainWindow(mainWindow),
            oldCursor(mainWindow.cursor()) { mainWindow.setCursor(shape); }
        ~SaveCursor() { mainWindow.setCursor(oldCursor); }
    public:
        MainWindowBase& mainWindow;
        QCursor         oldCursor;
    };

    QString currentSettingsDirectory() const;
    QString currentSettingsFile() const;

    // We can't focus widgets during UI construction, so we defer it to a post-load hook.
    void setPostLoadFocus(QWidget* w) { postLoadFocus = w; }

    // Get persistent model ID for the given change tracking model
    virtual ChangeTrackingModel* persistentModelForId(int /*modelId*/) { return nullptr; }
    virtual int idForPersistentModel(ChangeTrackingModel&) const { return -1; }

public slots:
    virtual void newConfig() = 0;
    virtual void selectionChanged(const QItemSelectionModel*,
                                  const QItemSelection& selected, const QItemSelection& deselected) = 0;
    virtual void currentChanged(const QModelIndex &current) = 0;
    virtual void statusMessage(UiType, const QString&) const; // status bar message
    virtual void addPaneAction(QAction*);
    virtual void addGroupAction(QAction*);
    virtual void replacePaneAction(QAction*);
    virtual void paneInWindowAction(QAction*);
    virtual void dirtyStateChanged(bool) { }  // sent by UndoMgr

protected slots:
    void loadRecentSession(QAction*);
    virtual void newFocus(QObject*);
    virtual void postLoadHook(); // see comment in C++

protected:
    friend class UndoWinCfg;
    friend class TabWidget; // TODO: Temporary
    friend class PaneBase;
    friend class UndoCfgData;

    template <typename... P>
    void runOnFocusPane(void (PaneBase::*method)(P...), const P&... p);
    void runOnFocusPane(const std::function<void(PaneBase*)>& fn);

    // *** begin Settings API
    void saveUiConfig(QSettings& settings, const std::function<void(void)>& = [](){} ) const;
    void loadUiConfig(QSettings& settings, const std::function<void(void)>& = [](){});
    // *** end Settings API

    virtual CfgDataBase& cfgData() = 0;
    PaneBase* addPane(PaneBase* toAdd, PaneBase::Container* row, bool before = false,
                      PaneBase* refPane = nullptr);
    PaneBase::Container* addPane(PaneBase::Container* toAdd, PaneBase::Container* row, int before = 0);
    void      splitPaneInteractive(PaneBase* pane, Qt::Orientation orientation);

    PaneBase* addGroupSibling(PaneBase* toAdd, PaneBase::Container* parent,
                              bool before = false, PaneBase* refPane = nullptr);

    void      setupSignals();         // focus changes, etc
    void      setupDefaults();        // dialog sizes
    void      setupTimers();          // post load timers
    void      setupDefaultSettings();
    void      setupAppConfig();       // setup app title, etc
    void      setupActionTooltips();  // set WhatsThis to match ToolTip
    void      cleanup();              // things to do before deleting the UI
    void      changeFontSize(float mul);
    void      setFontSize(int newSize);
    void      closeEvent(QCloseEvent*) override;
    void      showEvent(QShowEvent*) override;
    void      error(const QString& message, const QString& type) const;
    void      unimplemented();
    void      uiSave(const QString& file);
    void      uiLoad(const QString& file);
    void      saveSettingsAs();
    void      saveSettings();
    void      openSettings();
    void      revertSettings();
    int       warningDialog(const QString& winTitle, const QString& msg);
    void      setLoadedFile(const QString& file);

    // load config data from these settings
    virtual void loadCfgData(QSettings&) = 0;

    // If force is true, then reset all the UndoableObjects's dirty states as well, and
    // force-set the window title no matter its old state (used on save/load).
    virtual void markModified(bool modified, bool force = false);

    // Helper to avoid duplication of this effort in derived clases
    virtual void updateUndoActions(const UndoMgr*, QAction* undo, QAction* redo, QAction* clear = nullptr);

    void      changeStatusBarColor(const QColor& color) const;
    void      changeStatusBarColor(QPalette::ColorRole) const;

    PaneBase* trackFocus(PaneBase*);  // track newly focused pane in the prevFocus deque
    PaneBase* focusedPane(QObject* focused = nullptr) const;
    PaneBase* focusedPane(PaneBase* pane) const;
    PaneBase* focusedPaneWarn(QObject* focused = nullptr) const;
    PaneBase* focusedPaneWarn(PaneBase* pane) const;
    void      paneRefocus(PaneBase* deletedPane = nullptr, PaneBase* newFocus = nullptr);
    PaneBase* replacePane(PaneBase* newPane, PaneBase* oldPane);
    void      removePane(PaneBase* pane);
    void      movePane(QWidget* pane, int relPos); // relpos can be - or +
    void      newWindowInteractive(PaneBase* pane = nullptr);
    void      closeSecondaryWindows();
    TabWidget* newWindow();
    void      balanceSiblingsInteractive();

    void      cleanStructure();
    static void moveChildren(PaneBase::Container* moveTo, PaneBase::Container* moveFrom);
    void      movePaneParent(PaneBase* pane, int relPos); // relpos can be - or +

    static bool backupFile(const QString& file, int count);

    virtual void resizeColumnsAllPanes();

    virtual void sessionSave();
    void         sessionRestore();
    virtual void addSession(const QString& file, bool save = true); // add settings file to recent session list
    virtual void removeSession(const QString& file, bool save = true); // remove session file
    virtual void restoreRecentSessions();
    virtual void firstRunHook();
    QString      recentSessionsFile() const;
    QString      appConfigDir() const;
    QString      appDataDir() const;
    virtual void recentSessionsChanged() { }
    void         recentSessionsChanged(QMenu*);
    bool         hasSettingsFile() const { return !currentSettingsFile().isEmpty(); }
    TabWidget*   mainWindowTabs();
    const TabWidget* mainWindowTabs() const;
    int          nextPaneId() { return m_nextPaneId++; }

    // run on all panels if className == nullptr
    template <typename P = PaneBase, typename FN = void> void runOnPanels(const FN& fn) const;

    static const constexpr char* confFileFilter  = "Configuration (*.ini *.conf);;Text (*.txt);;Backups(*.conf.~*~);;All (*)";

    const char*        apptitle;           // application title

    bool               saveOnExit;         // true to autosave configuration on exit
    bool               firstExecution;     // whether to auto-show intro dialog
    int                startupFontSize;    // for resetting font

    QList<QAction*>    paneAddActions;     // pane add menu (dynamic)
    QList<QAction*>    paneGrpActions;     // add group sibling menu (dynamic)
    QList<QAction*>    paneRepActions;     // pane replace menu (dynamic)
    QList<QAction*>    paneWinActions;     // open pane in new window

    QProgressBar       progressBar;        // bottom progress bar
    QStringList        recentSessions;     // recent sessions list

private:
    void saveWinConfig(QSettings&) const;
    void loadWinConfig(QSettings&);
    void saveRecentSessions();

    UndoMgr            m_undoMgr;          // must be set up before models which use it.

    TabWidget*         uiTabs;             // main window tab widget
    QWidget*           postLoadFocus;      // we can't focus during load, because the UI doesn't exist yet.

    mutable QErrorMessage errorDialog;     // When things are not going well. Mutable so we can call from const methods.

    QString            lastSettingsFile;   // last file saved or loaded
    QString            defaultSettingsDir; // default save location

    QList<PaneBase*>   prevFocus;          // deque of prior focused panels
    QTimer             postLoadTimer;      // see comment in C++

    int                m_nextPaneId;       // next unique paneId
    bool               modFlag;            // modification flag
    bool               loadError;          // extra checking on top of QSettings::status()
};

template <typename P, typename FN>
void MainWindowBase::runOnPanels(const FN& fn) const
{
    for (auto* window : QApplication::topLevelWidgets())
        for (auto& obj : window->findChildren<P*>())
            fn(static_cast<P*>(obj));
}

// search prior focus list first, then all.
template <typename P> P* MainWindowBase::findPane(const std::function<bool(P*)>& pred) const
{
    // Search through prior focus list first, most recent first
    for (auto it = prevFocus.crbegin(); it != prevFocus.crend(); ++it)
        if (P* pane = dynamic_cast<P*>(*it); pane != nullptr)
            if (pred(pane))
                return pane;

    // It was never focused.  Last ditch attempt: dig through everything visible.
    for (auto* window : QApplication::topLevelWidgets())
        for (auto* tabs : window->findChildren<TabWidget*>())
            if (auto* tab = tabs->currentTab())
                if (P* pane = tab->findChild<P*>())
                    if (pred(pane))
                        return pane;

    return nullptr;
}

template <typename... P>
void MainWindowBase::runOnFocusPane(void (PaneBase::*method)(P...), const P&... p)
{
    if (PaneBase* pane = focusedPaneWarn())
        (pane->*method)(p...);
}

#endif // MAINWINDOWBASE_H
