/*
    Copyright 2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QString>
#include <QPalette>

#include "launchsplash.h"
#include "ui_launchsplash.h"

LaunchSplash::LaunchSplash(int count, const QString& logoHtml) :
    QFrame(nullptr),
    m_current(0),
    ui(new Ui::LaunchSplash)
{
    ui->setupUi(this);
    setWindowFlags(Qt::SplashScreen | Qt::FramelessWindowHint);

    ui->appLogo->setText(logoHtml);

    setBackgroundRole(QPalette::Midlight);

    const QString appTitleText =
            QString("<html><head/><body><p align=center><span style=\" font-size:25pt; font-style:italic;\">") +
            QApplication::applicationDisplayName() + " " + QApplication::applicationVersion() +
            "</span></p></body></html>";

    ui->appTitle->setText(appTitleText);
    ui->progressBar->setMaximum(count);

    show();
    qApp->processEvents();  // let event loop run to display this window.
}

LaunchSplash::~LaunchSplash()
{
    delete ui;
}

void LaunchSplash::setStatus(const QString& text)
{
    ui->statusText->setText(text);
    ui->progressBar->setValue(++m_current);

    update();
}

void LaunchSplash::finished()
{
    setStatus(tr("Done."));
    ui->progressBar->setValue(ui->progressBar->maximum());

    hide();
    // Don't execute processEvents() here. We'll hide when object is deleted and normal event loop runs.
}
