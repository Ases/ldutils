/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStandardItem>
#include <QTreeView>
#include <QLineEdit>
#include <QComboBox>
#include <QItemSelectionModel>
#include <QLabel>

#include "datacolumnpanebase.h"

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/roles.h>
#include <src/core/cfgdatabase.h>
#include <src/core/undopane.h>
#include <src/ui/windows/mainwindowbase.h>
#include <src/ui/misc/querycompleter.h>

const QString DataColumnPaneBase::filterAllHeader = QObject::tr("All");
int DataColumnPaneBase::SignalBlocker::nest = 0;

DataColumnPaneBase::DataColumnPaneBase(MainWindowBase& mainWindow, int paneClass, QWidget *parent, bool useFlattener) :
    PaneBase(mainWindow, paneClass, parent),
    m_treeView(nullptr),
    m_subtreeFilter(this),
    m_flattenFilter(this),
    m_headerView(Qt::Horizontal, this),
    m_filterText(nullptr),
    m_filterValid(nullptr),
    m_filterColumn(nullptr),
    m_showColumn(nullptr),
    m_showColumnModel(this),
    m_filterColumnModel(this),
    m_baseModel(nullptr),
    m_headerMenu(tr("Column Actions"), this),
    m_headerMenuIndex(-1),
    m_resizeTimer(this),
    m_dataAddedTimer(this),
    m_useFlattener(useFlattener),
    m_filterDirty(false)
{
    setupHeaderMenus();
    setupTimers();
    saveSortData();
}

// This is needed to make the MOC hapy
DataColumnPaneBase::~DataColumnPaneBase()
{
}

void DataColumnPaneBase::setupHeaderMenus()
{
    // Header context menus
    m_headerMenu.addAction(Icons::get("view-restore"), tr("Show All Columns"), this, &DataColumnPaneBase::showAllColumnsInteractive);
    m_headerMenu.addAction(Icons::get("project-defaults"), tr("Show Default Columns"), this,
                           static_cast<void(DataColumnPaneBase::*)()>(&DataColumnPaneBase::showDefaultColumnsInteractive));
    m_headerMenu.addAction(Icons::get("edit-table-delete-column"), tr("Hide Column"), this,
                           &DataColumnPaneBase::hideHeaderColumnInteractive);
    m_headerMenu.addAction(Icons::get("edit-table-delete-column"), tr("Hide Other Columns"), this,
                           &DataColumnPaneBase::hideOtherColumnsInteractive);

    m_headerView.setContextMenuPolicy(Qt::CustomContextMenu);
    connect(&m_headerView, &QHeaderView::customContextMenuRequested, this, &DataColumnPaneBase::showHeaderContextMenu);
}

void DataColumnPaneBase::setupCompleter()
{
    if (m_filterText == nullptr || m_baseModel == nullptr) {
        if (m_filterText != nullptr)
            m_filterText->setCompleter(nullptr);

        return;
    }

    // the QLineEdit widget takes ownership of the completer: we don't delete or keep track of it.
    QueryCompleter* completer = new QueryCompleter(cfgData(), m_subtreeFilter.queryCtx(), m_filterText);
    m_filterText->setCompleter(completer);
}

void DataColumnPaneBase::setupLineEdit()
{
    if (m_filterText == nullptr)
        return;

    connect(m_filterText, &QLineEdit::textChanged, this, &DataColumnPaneBase::filterTextChanged);
}

void DataColumnPaneBase::setupFilterStatusIcons()
{
    if (m_filterValid != nullptr) {
        const QSize iconSize = m_filterValid->minimumSize();
        const CfgDataBase& cfgData = constMainWindow().cfgData();

        m_filterEmptyIcon   = cfgData.filterEmptyIcon.pixmap(iconSize);
        m_filterValidIcon   = cfgData.filterValidIcon.pixmap(iconSize);
        m_filterInvalidIcon = cfgData.filterInvalidIcon.pixmap(iconSize);
    }
}

void DataColumnPaneBase::setupTimers()
{
    // Avoid resize spam
    m_resizeTimer.setSingleShot(true);
    connect(&m_resizeTimer, &QTimer::timeout, this, &DataColumnPaneBase::resizeDeferredHook);

    m_dataAddedTimer.setSingleShot(true);
    connect(&m_dataAddedTimer, &QTimer::timeout, this, &DataColumnPaneBase::dataAddedDeferredHook);
}

bool DataColumnPaneBase::isQueryValid() const
{
    return m_subtreeFilter.isValid();
}

void DataColumnPaneBase::sort()
{
    m_subtreeFilter.sort(m_subtreeFilter.sortColumn(), m_subtreeFilter.sortOrder());
}

// Select given indices
void DataColumnPaneBase::select(const QModelIndexList& selection,
                                QItemSelectionModel::SelectionFlags flags)
{
    if (selectionModel() == nullptr)
        return;

    if (flags & QItemSelectionModel::Clear) {
        selectionModel()->clearSelection();
        flags &= ~QItemSelectionModel::Clear;
    }

    for (const auto& idx : Util::MapUp(&topFilter(), selection))
        selectionModel()->select(idx, flags);
}

bool DataColumnPaneBase::select(const QModelIndex& modelIdx,
                                QItemSelectionModel::SelectionFlags flags)
{
    if (selectionModel() == nullptr)
        return false;

    const QModelIndex proxyIdx = Util::MapUp(&topFilter(), modelIdx);
    if (!proxyIdx.isValid())
        return false;

    selectionModel()->select(proxyIdx, flags);

    if (flags & QItemSelectionModel::Current) {
        m_treeView->setCurrentIndex(proxyIdx);
        m_treeView->scrollTo(proxyIdx);
    }

    return true;
}

void DataColumnPaneBase::setupView(QTreeView *view, QAbstractItemModel* model)
{
    if (view == nullptr)
        return;

    m_treeView  = view;
    m_baseModel = model;
    setupCompleter();

    m_subtreeFilter.setSourceModel(model);
    m_subtreeFilter.setSortCaseSensitivity(cfgData().getSortCaseSensitivity());
    m_subtreeFilter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());
    m_subtreeFilter.setDynamicSortFilter(false);

    view->setModel(&topFilter());
    view->setHeader(&m_headerView);
    view->sortByColumn(0, Qt::AscendingOrder);

    // These are adopted from the form editor, so don't mess with them here.  Code for clarity.
    static const bool overrideFormEditor = false;
    if (overrideFormEditor) {
        view->setRootIsDecorated(false);
        view->setSortingEnabled(true);
        view->setAlternatingRowColors(true);
        view->setSelectionMode(QAbstractItemView::ExtendedSelection);
        view->setSelectionBehavior(QAbstractItemView::SelectRows);
        view->setEditTriggers(QAbstractItemView::EditKeyPressed | QAbstractItemView::DoubleClicked);
    }

    m_headerView.setSectionResizeMode(QHeaderView::Interactive);
    m_headerView.setDefaultAlignment(Qt::AlignLeft);
    m_headerView.setSectionsMovable(true);  // allow column reordering

    m_subtreeFilter.setFilterRole(Util::CopyRole);  // from copy-formatted data

    setFocusProxy(view);
}

void DataColumnPaneBase::setupSectionMoveSignal(bool install)
{
    if (install) {
        // Manage undos for column reordering
        connect(&m_headerView, &QHeaderView::sectionMoved, this, &DataColumnPaneBase::handleSectionMoved, Qt::UniqueConnection);
    } else {
        disconnect(&m_headerView, &QHeaderView::sectionMoved, this, &DataColumnPaneBase::handleSectionMoved);
    }
}

void DataColumnPaneBase::setupSectionSortSignal(bool install)
{
    if (install) {
        // Manage undos for column reordering
        connect(&m_headerView, &QHeaderView::sortIndicatorChanged, this, &DataColumnPaneBase::handleSortChanged, Qt::UniqueConnection);
    } else {
        disconnect(&m_headerView, &QHeaderView::sortIndicatorChanged, this, &DataColumnPaneBase::handleSortChanged);
    }
}

void DataColumnPaneBase::setupSignals()
{
    // Selection changes
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, [this](const QItemSelection &selected, const QItemSelection &deselected) {
        mainWindow().selectionChanged(selectionModel(), selected, deselected); });

    // And current object changes
    connect(selectionModel(), &QItemSelectionModel::currentChanged, &mainWindow(), &MainWindowBase::currentChanged);

    setupSectionMoveSignal(true);
    setupSectionSortSignal(true);

    // Data inserted
    connect(m_baseModel, &QAbstractItemModel::rowsInserted, this, &DataColumnPaneBase::handleRowsInserted);
}

void DataColumnPaneBase::handleSectionMoved(int logicalIndex, int oldVisualIndex, int newVisualIndex)
{
    if (oldVisualIndex == newVisualIndex)
        return;

    UndoMgr::ScopedUndo undoSet(mainWindow().undoMgr(), tr("Move Header: ") + columnName(logicalIndex));

    mainWindow().undoMgr().add(new UndoPaneSectionMove(*this, oldVisualIndex, newVisualIndex));
}

void DataColumnPaneBase::handleSortChanged(int logicalIndex, Qt::SortOrder order)
{
    if (logicalIndex == m_prevSortColumn && order == m_prevSortOrder)
        return;

    UndoMgr::ScopedUndo undoSet(mainWindow().undoMgr(), tr("Change Sort"));

    mainWindow().undoMgr().add(new UndoPaneSort(*this, m_prevSortColumn, m_prevSortOrder,
                                                logicalIndex, order));

    saveSortData(); // save whatever sort data now exists
}

// Re-run filter after rows are inserted, in case new items are in the filtered set.
void DataColumnPaneBase::handleRowsInserted(const QModelIndex&, int, int)
{
    m_filterDirty = true;
    m_dataAddedTimer.start(500);
}

void DataColumnPaneBase::showHeaderContextMenu(const QPoint &pos)
{
    m_headerMenuIndex = m_headerView.logicalIndexAt(pos);

    m_headerMenu.exec(m_headerView.mapToGlobal(pos));
}

void DataColumnPaneBase::reenableChildren()
{
    PaneBase::reenableChildren();
    m_headerMenu.setEnabled(true);
}

bool DataColumnPaneBase::validHeaderIndex(int pos) const
{
    return pos >= 0 && pos < columnCount();
}

void DataColumnPaneBase::toggleColumnInteractive(QStandardItem* item)
{
    setColumnHiddenInteractive(item->index().row() - 1,  // -1 to bypass the descriptive header
                               item->checkState() != Qt::Checked);
}

void DataColumnPaneBase::showFilterStatusIcon() const
{
    if (m_filterValid != nullptr)
        m_filterValid->setPixmap(m_subtreeFilter.isEmpty() ? m_filterEmptyIcon :
                               m_subtreeFilter.isValid() ? m_filterValidIcon : m_filterInvalidIcon);
}

void DataColumnPaneBase::filterTextChanged(const QString& query)
{
    m_subtreeFilter.setQueryString(query);
    showFilterStatusIcon();
    m_filterDirty = false;

    if (query.size() == 0)  // if filter is cleared, re-show everything.  TODO: Restore prior show/hide status.
        showAll();
    else
        expandAll();
}

// Since the QHeaderView::sectionMoved signal fires for both interactive and API driven
// events, we can't know whether to create an undo event.  This method is for
// non-interactive use, and avoids sending the signal for those cases.
void DataColumnPaneBase::moveSection(int from, int to)
{
    const SignalBlocker block(*this);
    m_headerView.moveSection(from, to);
}

void DataColumnPaneBase::setSort(int logicalIndex, Qt::SortOrder order)
{
    const SignalBlocker block(*this);
    m_headerView.setSortIndicator(logicalIndex, order);
    saveSortData();
}

int DataColumnPaneBase::visualIndex(int logicalIndex) const
{
    return m_headerView.visualIndex(logicalIndex);
}

// Upon load of an old conf format, if we've inserted a data column in the
// new save format we should adjust the hidden columns to reflect this.
void DataColumnPaneBase::insertColumnOnLoad(uint32_t addedInVersion, int col, int count)
{
    if (cfgData().priorCfgDataVersion >= addedInVersion)
        return;

    // Update column positions
    for (int c = count - 1; c >= col; --c) {
        setColumnHidden(c, m_treeView->isColumnHidden(c - 1));
        moveSection(visualIndex(c), visualIndex(c-1));
    }

    // Update sort column
    if (int sortCol = m_headerView.sortIndicatorSection(); sortCol >= col)
        m_headerView.setSortIndicator(sortCol + 1, m_headerView.sortIndicatorOrder());
}

void DataColumnPaneBase::hideHeaderColumnInteractive()
{
    setColumnHiddenInteractive(m_headerMenuIndex, true);
    m_headerMenuIndex = -1;
}

QItemSelectionModel* DataColumnPaneBase::selectionModel() const
{
    if (m_treeView == nullptr)
        return nullptr;

    return m_treeView->selectionModel();
}

int DataColumnPaneBase::columnCount() const
{
    if (m_treeView == nullptr || m_treeView->model() == nullptr)
        return 0;

    return m_treeView->model()->columnCount();
}

void DataColumnPaneBase::setColumnHidden(int column, bool hidden)
{
    if (m_treeView == nullptr || !validHeaderIndex(column))
        return;   

    m_treeView->setColumnHidden(column, hidden);

     // +1 to bypass header
    if (m_showColumnModel.rowCount() > 0) {
        QSignalBlocker block(m_showColumnModel);
        m_showColumnModel.item(column+1)->setCheckState(hidden ? Qt::Unchecked : Qt::Checked);
    }

    resizeToFit(10);
}

// For interactive use: creates an undo point
void DataColumnPaneBase::setColumnHiddenInteractive(int column, bool hidden)
{
    const QString undoName = (hidden ? tr("Hide") : tr("Show")) + " " +
            tr("Column") + ": " + columnName(column);

    const UndoPaneState::ScopedUndo undoSet(*this, undoName);
    mainWindow().undoMgr().add(new UndoPaneSetColumnHidden(*this, column, hidden));

    setColumnHidden(column, hidden);
}

QModelIndexList DataColumnPaneBase::getSelections() const
{
    if (selectionModel() == nullptr)
        return QModelIndexList();

    QModelIndexList selection = selectionModel()->selectedRows();
    return Util::MapDown(selection);
}

bool DataColumnPaneBase::hasSelection() const
{
    if (selectionModel() == nullptr)
        return false;

    return selectionModel()->hasSelection();
}

bool DataColumnPaneBase::hasItems() const
{
    return topFilter().rowCount() > 0;
}

bool DataColumnPaneBase::isSelected(const QModelIndex &idx) const
{
    if (selectionModel() == nullptr)
        return false;

    return selectionModel()->isSelected(Util::MapUp(&topFilter(), idx));
}

void DataColumnPaneBase::showAllColumnsInteractive()
{
    const UndoPaneState::ScopedUndo undoSet(*this, tr("Show All Columns"));

    showAllColumns();
}

// For use from context menu
void DataColumnPaneBase::showAllColumns()
{
    for (ModelType h = 0; h < columnCount(); ++h)
        setColumnHidden(h, false);

    resizeToFit();
}


// For use from context menu
void DataColumnPaneBase::hideOtherColumns()
{
    if (!validHeaderIndex(m_headerMenuIndex))
        return;

    for (ModelType h = 0; h < columnCount(); ++h)
        setColumnHidden(h, h != m_headerMenuIndex);
}

void DataColumnPaneBase::hideOtherColumnsInteractive()
{
    const UndoPaneState::ScopedUndo undoSet(*this, tr("Hide Other Columns"));

    hideOtherColumns();
}

// Create from initializer list
DataColumnPaneBase::DefColumns::DefColumns(std::initializer_list<QPair<ModelType, bool>> args)
{
    reserve(int(args.size()));
    for (const auto& arg : args)
        append(arg);
}

// Linear search, but we don't do this very much and not from perf sensitive code.
int DataColumnPaneBase::DefColumns::findData(ModelType md) const
{
    int pos = 0;
    for (const auto& pair : *this) {
        if (pair.first == md)
            return pos;
        ++pos;
    }
    return -1;
}

// Linear search, but we don't do this very much and not from perf sensitive code.
bool DataColumnPaneBase::DefColumns::defaultShown(ModelType md) const
{
    for (const auto& pair : *this)
        if (pair.first == md)
            return pair.second;
    return false;
}

void DataColumnPaneBase::showDefaultColumns(const DefColumns& defCols)
{
    // First add the ones we are explicitly given an order for
    int pos = 0;

    for (const auto& it : defCols)
        moveSection(m_headerView.visualIndex(it.first), pos++);

    // The others go after that in the enum order
    for (ModelType mt = 0; mt < columnCount(); ++mt)
        if (defCols.findData(mt) < 0)
            moveSection(m_headerView.visualIndex(mt), pos++);

    // Show or hide.
    for (ModelType mt = 0; mt < columnCount(); ++mt)
        setColumnHidden(mt, !defCols.defaultShown(mt));

    resizeToFit();
}

void DataColumnPaneBase::saveSortData()
{
    m_prevSortOrder  = m_headerView.sortIndicatorOrder();
    m_prevSortColumn = m_headerView.sortIndicatorSection();
}

void DataColumnPaneBase::showDefaultColumns()
{
    showDefaultColumns(defColumnView());
}

void DataColumnPaneBase::showDefaultColumnsInteractive()
{
    const UndoPaneState::ScopedUndo undoSet(*this, tr("Show Default Columns"));

    showDefaultColumns();
}

void DataColumnPaneBase::refreshHiddenColumns()
{
    if (m_treeView == nullptr)
        return;

    // Update rest of UI, e.g, combo box, to reflect hidden columns.
    for (int c = 0; c < columnCount(); ++c)
        setColumnHidden(c, m_treeView->isColumnHidden(c));
}

void DataColumnPaneBase::showAll()
{
    if (m_filterText == nullptr)
        return;

    m_filterText->clear();
    expandAll();
    resizeToFit();
}

void DataColumnPaneBase::expandAll()
{
    if (m_treeView == nullptr)
        return;

    m_treeView->expandAll();
}

void DataColumnPaneBase::collapseAll()
{
    if (m_treeView == nullptr)
        return;

    m_treeView->collapseAll();
}

void DataColumnPaneBase::selectAll()
{
    if (m_treeView == nullptr)
        return;

    m_treeView->selectAll();
}

void DataColumnPaneBase::selectNone()
{
    if (m_treeView == nullptr)
        return;

    m_treeView->clearSelection();
}

void DataColumnPaneBase::resizeDeferredHook()
{
    if (m_treeView == nullptr)
        return;

    Util::ResizeViewForData(*m_treeView);
}

void DataColumnPaneBase::dataAddedDeferredHook()
{
    if (!m_filterDirty)
        return;

    // Don't use setQuery(); we must re-run even if query is the same, because this
    // triggers on the addition of new data.
    filterTextChanged(getQuery());
}

void DataColumnPaneBase::resizeToFit(int defer)
{
    if (defer < 0)
        resizeDeferredHook();
    else
        m_resizeTimer.start(defer);
}

void DataColumnPaneBase::copySelected() const
{
    if (m_treeView == nullptr)
        return;

    // TODO: should probably use topFilter(), but that returns an abstract filter
    //       which doesn't have the clipboard methods.
    m_subtreeFilter.copyToClipboard(*m_treeView,
                                  m_subtreeFilter.sortedSelection(selectionModel()),
                                  cfgData().rowSeparator, cfgData().colSeparator,
                                  Util::CopyRole);
}

void DataColumnPaneBase::newConfig()
{
    m_subtreeFilter.setCaseSensitivity(cfgData().getFilterCaseSensitivity());
    m_subtreeFilter.setSortCaseSensitivity(cfgData().getSortCaseSensitivity());

    setupCompleter();
    setupFilterStatusIcons();
}

void DataColumnPaneBase::viewAsTree(bool asTree)
{
    if (!m_useFlattener)
        return;

    if (asTree) {
        m_subtreeFilter.setSourceModel(m_baseModel);
        m_flattenFilter.setSourceModel(nullptr);
    } else {
        m_flattenFilter.setSourceModel(m_baseModel);
        m_subtreeFilter.setSourceModel(&m_flattenFilter);
    }
}

bool DataColumnPaneBase::viewIsTree() const
{
    return m_subtreeFilter.sourceModel() != &m_flattenFilter;
}

// We can't directly set the column in the filter view, since we have an "All" header.
void DataColumnPaneBase::setFilterColumn(int column)
{
    if (m_filterColumn == nullptr)
        return;

    m_filterColumn->setCurrentIndex(column);

    // -1 to account for "All" header. Setting -1 in the filter searches all columns.
    m_subtreeFilter.setFilterKeyColumn(column - 1);
}

void DataColumnPaneBase::setQuery(const QString& query)
{
    if (m_filterText == nullptr)
        return;

    m_filterText->setText(query);
}

QString DataColumnPaneBase::getQuery() const
{
    if (m_filterText == nullptr)
        return PaneBase::getQuery();

    return m_filterText->text();
}

QModelIndex DataColumnPaneBase::clickPosIndex(const QPoint& pos) const
{
    return Util::clickPosIndex(m_treeView, m_headerView, pos);
}

void DataColumnPaneBase::setSourceModel(QAbstractItemModel* model)
{
    m_baseModel = model;
    setupCompleter();

    if (viewIsTree() || !m_useFlattener) {
        m_subtreeFilter.setSourceModel(model);
    } else {
        m_flattenFilter.setSourceModel(model);
    }
}

void DataColumnPaneBase::setFlattenPredicate(FlattenFilter::PredFn pred)
{
    m_flattenFilter.setPredicate(pred);
}

void DataColumnPaneBase::save(QSettings& settings) const
{
    PaneBase::save(settings);

    SL::Save(settings, "filterText",   m_filterText);
    SL::Save(settings, "filterColumn", m_filterColumn);
    SL::Save(settings, "headerView",   m_headerView);

    // Save current index in a form we can restore.
    if (m_treeView != nullptr)
        SL::Save(settings, "currentIndex", Util::SaveIndex(Util::MapDown(m_treeView->currentIndex())));
}

void DataColumnPaneBase::load(QSettings& settings)
{
    const SignalBlocker block(*this);
    PaneBase::load(settings);

    SL::Load(settings, "filterText",    m_filterText);
    SL::Load(settings, "filterColumn",  m_filterColumn);
    SL::Load(settings, "headerView",    m_headerView);

    // Restore current index
    if (m_treeView != nullptr) {
        SL::Load(settings, "currentIndex", m_savedCurrentIndex);
    }

    // Update rest of UI, e.g, combo box, to reflect hidden columns.
    refreshHiddenColumns();

    saveSortData(); // save whatever sort data now exists
}

void DataColumnPaneBase::postLoadHook()
{
    PaneBase::postLoadHook();

    if (m_treeView == nullptr || m_baseModel == nullptr)
        return;

    m_treeView->setCurrentIndex(Util::MapUp(&topFilter(), Util::RestoreIndex(*m_baseModel, m_savedCurrentIndex)));
    m_savedCurrentIndex.clear();
}

DataColumnPaneBase::SaveColumnWidths::SaveColumnWidths(DataColumnPaneBase* dp) :
    dp(dp)
{
    state = dp->m_headerView.saveState();
}

DataColumnPaneBase::SaveColumnWidths::~SaveColumnWidths()
{
    dp->m_headerView.restoreState(state);
}

