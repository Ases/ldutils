/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATACOLUMNPANEBASE_H
#define DATACOLUMNPANEBASE_H

#include <QHeaderView>
#include <QMenu>
#include <QStandardItemModel>
#include <QTreeView>
#include <QSet>
#include <QList>
#include <QPoint>
#include <QTimer>
#include <QToolButton>
#include <QPixmap>
#include <QPair>

#include <src/core/modelmetadata.h>
#include <src/ui/filters/subtreefilter.h>
#include <src/ui/filters/flattenfilter.h>
#include "src/core/modelmetadata.h"
#include "panebase.h"

// Intermediate class for panes which show columns of data.  Provides methods to
// hide/sort/etc columns.  Has no UI of its own.

class QItemSelectionModel;
class QStandardItem;
class QLineEdit;
class QComboBox;
class QLabel;
class MainWindowBase;

class DataColumnPaneBase : public PaneBase
{
    Q_OBJECT

public:
    DataColumnPaneBase(MainWindowBase& mainWindow, int paneClass, QWidget *parent = nullptr, bool m_useFlattener = true);
    ~DataColumnPaneBase() override;

    bool hasSelection() const override;
    bool isSelected(const QModelIndex& idx) const override;
    bool hasItems() const override;
    QModelIndexList getSelections() const override;
    virtual void sort() override;
    virtual QItemSelectionModel* selectionModel() const override;
    virtual void select(const QModelIndexList&, QItemSelectionModel::SelectionFlags);
    virtual bool select(const QModelIndex& modelIdx, QItemSelectionModel::SelectionFlags =
            QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);

    // Put this on the stack to disable sorting, and re-enable on its destruction.
    class SortDisabler {
    public:
        SortDisabler(DataColumnPaneBase* dcp) : dcp(dcp), sorted(dcp->m_treeView->isSortingEnabled()) { }
        ~SortDisabler() { dcp->m_treeView->setSortingEnabled(sorted); }
    private:
        DataColumnPaneBase* dcp;
        bool                sorted;
    };

    template <class P> static auto DisableSort(const QObject&);

    virtual void showAll() override;
    virtual void expandAll() override;
    virtual void collapseAll() override;
    virtual void selectAll() override;
    virtual void selectNone() override;
    virtual void resizeToFit(int defer = -1) override;
    virtual void copySelected() const override;
    virtual void newConfig() override;
    virtual void viewAsTree(bool) override;
    virtual bool viewIsTree() const override;
    virtual void setQuery(const QString& query) override;
    virtual QString getQuery() const override;
    virtual void showAllColumns();

protected:
    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    virtual void postLoadHook() override; // things we can only do after the model data is loaded.

protected slots:
    virtual void setFilterColumn(int);
    virtual void filterTextChanged(const QString&);
    virtual void toggleColumnInteractive(QStandardItem*);
    virtual void hideHeaderColumnInteractive();
    virtual void hideOtherColumnsInteractive();
    virtual void showDefaultColumnsInteractive();
    virtual void showHeaderContextMenu(const QPoint&);
    virtual void showAllColumnsInteractive();
    virtual void handleRowsInserted(const QModelIndex &parent, int first, int last);

protected:
    // This object can be pushed on the stack to remember column widths across some operation
    // that will otherwise change them, such as a model reset.
    class SaveColumnWidths {
    public:
        SaveColumnWidths(DataColumnPaneBase* dp);
        ~SaveColumnWidths();
    private:
        DataColumnPaneBase* dp;
        // QVector<std::tuple<int, bool>> state;
        QByteArray state;
    };

    // Map columns to an order, and shown state. This must be a vector, because order matters.
    struct DefColumns : public QVector<QPair<ModelType, bool>> {
        DefColumns(std::initializer_list<QPair<ModelType, bool>> args);

        int  findData(ModelType) const; // returns -1 if not found, else position.
        bool defaultShown(ModelType) const;
    };

    virtual const DefColumns& defColumnView() const = 0;   // return default column visual order
    void setupView(QTreeView* view, QAbstractItemModel*);
    template <class MD> void setWidgets(const DefColumns&, QLineEdit* ft,
                                        QComboBox* fc, QComboBox* sc, QWidget* fb = nullptr,
                                        QLabel* fv = nullptr);

    virtual void setupSignals();
    virtual int  columnCount() const;
    virtual void setColumnHidden(int column, bool hidden);
    virtual void setColumnHiddenInteractive(int column, bool hidden);  // creates undo pt
    virtual void reenableChildren() override;
    virtual void hideOtherColumns();
    virtual void showDefaultColumns();
    virtual void moveSection(int from, int to); // see c++ comment
    virtual void setSort(int logicalIndex, Qt::SortOrder); // see c++ comment
    virtual int  visualIndex(int logicalIndex) const;
    virtual void insertColumnOnLoad(uint32_t addedInVersion, int col, int count); // see c++ comment

    // Ease of access to column name
    virtual QString columnName(int column) const {
        return m_baseModel->headerData(column, Qt::Horizontal).toString();
    }

    void refreshHiddenColumns();
    QModelIndex clickPosIndex(const QPoint& pos) const;
    void showFilterStatusIcon() const;

    QSortFilterProxyModel& topFilter() { return m_subtreeFilter; }
    const QSortFilterProxyModel& topFilter() const { return m_subtreeFilter; }
    void setSourceModel(QAbstractItemModel*);
    void setFlattenPredicate(FlattenFilter::PredFn pred);

    bool isQueryValid() const;

    static const QString filterAllHeader;   // text for filter-all

    QTreeView*          m_treeView;         // primary treeview

private slots:
    void handleSectionMoved(int logicalIndex, int oldVisualIndex, int newVisualIndex);
    void handleSortChanged(int logicalIndex, Qt::SortOrder);

private:
    // Smaller hammer than the Qt one.
    struct SignalBlocker {
        SignalBlocker(DataColumnPaneBase& p) : p(p) {
            if (nest++ == 0) {
                p.setupSectionMoveSignal(false);
                p.setupSectionSortSignal(false);
            }
        }
        ~SignalBlocker() {
            if (--nest == 0) {
                p.setupSectionMoveSignal(true);
                p.setupSectionSortSignal(true);
            }
        }
        DataColumnPaneBase& p;
        static int nest;
    };

    friend class UndoPaneSectionMove;
    friend class UndoPaneSetColumnHidden;
    friend class UndoPaneSort;

    void setupTimers();
    void setupHeaderMenus();
    void setupLineEdit();
    void setupCompleter();
    void setupSectionMoveSignal(bool install);
    void setupSectionSortSignal(bool install);
    void setupFilterStatusIcons();
    bool validHeaderIndex(int pos) const;
    void resizeDeferredHook();
    void dataAddedDeferredHook();
    void showDefaultColumns(const DefColumns&);  // initialize default columns from given set
    void saveSortData();

    template <class MD> void setupShowColumnComboBox(const DefColumns&);
    template <class MD> void setupFilterColumn();

    SubTreeFilter       m_subtreeFilter;     // filter with subtree searching
    FlattenFilter       m_flattenFilter;     // flatten tree views
    QHeaderView         m_headerView;        // header view
    QLineEdit*          m_filterText;        // filter text gadget, or nullptr if none
    QLabel*             m_filterValid;       // filter validity indicator
    QComboBox*          m_filterColumn;      // filter column, or nullptr if none
    QComboBox*          m_showColumn;        // shown column combobox, or nullptr if none
    QStandardItemModel  m_showColumnModel;   // for show-column combo box
    QStandardItemModel  m_filterColumnModel; // for fiilter-column combo box
    QAbstractItemModel* m_baseModel;         // underlying model
    QMenu               m_headerMenu;        // header context menu
    int                 m_headerMenuIndex;   // index of header column right-clicked
    QTimer              m_resizeTimer;       // avoid resize spam
    QTimer              m_dataAddedTimer;    // triggers after new data is added to base model
    bool                m_useFlattener;      // false to disable flatten filter
    bool                m_filterDirty;       // need to re-run filter (e.g, on data addition)
    QPixmap             m_filterEmptyIcon;   // emblem for empty filter
    QPixmap             m_filterValidIcon;   // emblem for valid filter
    QPixmap             m_filterInvalidIcon; // emblem for invalid filter
    QVector<int>        m_savedCurrentIndex; // for reenabling active item after load
    Qt::SortOrder       m_prevSortOrder;     // for creating undos
    int                 m_prevSortColumn;    // ...
};

template <class MD> void DataColumnPaneBase::setupShowColumnComboBox(const DefColumns& defColumns)
{
    if (m_treeView == nullptr || m_showColumn == nullptr)
        return;

    // Populate combobox for column selection
    m_showColumnModel.appendRow(new QStandardItem(tr("Show Columns")));

    ModelMetaData::setupComboBox<MD>(*m_showColumn, m_showColumnModel, nullptr,
                                     [this, &defColumns](ModelType mt, QStandardItem* item) {
        const bool show = defColumns.defaultShown(mt);
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item->setCheckState(show ? Qt::Checked : Qt::Unchecked);
        m_treeView->setColumnHidden(mt, !show);  // since we haven't connected the controls yet
        return item;
    });

    // connect column select behavior
    connect(&m_showColumnModel, &QStandardItemModel::itemChanged, this, &DataColumnPaneBase::toggleColumnInteractive);
}

template <class MD> void DataColumnPaneBase::setupFilterColumn()
{
    if (m_filterColumn == nullptr)
        return;

    // Populate combobox for column selection
    m_filterColumnModel.appendRow(new QStandardItem(filterAllHeader));

    ModelMetaData::setupComboBox<MD>(*m_filterColumn, m_filterColumnModel);

    connect(m_filterColumn, static_cast<void (QComboBox::*)(int)> (&QComboBox::currentIndexChanged),
            this, &DataColumnPaneBase::setFilterColumn);

    setFilterColumn(0);
}

template <class MD> void DataColumnPaneBase::setWidgets(const DefColumns& defColumns,
                                                        QLineEdit *ft, QComboBox* fc, QComboBox* sc,
                                                        QWidget* fb, QLabel* fv)
{
    m_filterText    = ft;
    m_filterColumn  = fc;
    m_showColumn    = sc;
    paneFilterBar   = fb;
    m_filterValid   = fv;

    setupLineEdit();
    setupCompleter();
    setupFilterColumn<MD>();
    showDefaultColumns(defColumns);
    setupShowColumnComboBox<MD>(defColumns);
    setupFilterStatusIcons();
    showFilterStatusIcon();

    if (m_showColumn != nullptr) {
        m_showColumn->setToolTip(tr("Select display columns."));
        m_showColumn->setWhatsThis(m_showColumn->toolTip());
    }

    if (m_filterColumn != nullptr) {
        m_filterColumn->setToolTip(tr("Default search column, if not specified in query."));
        m_filterColumn->setWhatsThis(m_filterColumn->toolTip());
    }

    // Resize widgets to some sane defaults
    if (QSplitter* splitter = dynamic_cast<QSplitter*>(paneFilterBar); splitter != nullptr) {
        for (int child = 0; child < splitter->count(); ++child) {
            const QWidget* widget = splitter->widget(child);
            if (dynamic_cast<const QLineEdit*>(widget) != nullptr ||
                widget->findChild<QLineEdit*>() != nullptr)
                splitter->setStretchFactor(child, 80);
            else if (dynamic_cast<const QComboBox*>(widget) != nullptr)
                splitter->setStretchFactor(child, 10);
            else if (dynamic_cast<const QToolButton*>(widget) != nullptr)
                splitter->setStretchFactor(child, 1);
        }
    }
}

template <class P> auto DataColumnPaneBase::DisableSort(const QObject& mainWindow)
{
    QList<SortDisabler> ds;
    for (auto& pane : mainWindow.findChildren<P*>())
        ds.append(pane);
    return ds;
}

#endif // DATACOLUMNPANEBASE_H
