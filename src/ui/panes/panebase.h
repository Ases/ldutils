/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANEBASE_H
#define PANEBASE_H

#include <QMenu>
#include <QGroupBox>
#include <QModelIndexList>
#include <QLayout>

#include <src/ui/panes/panebase.h>
#include <src/ui/panes/panegroupbase.h>
#include <src/core/settings.h>

class QItemSelectionModel;
class CfgDataBase;
class MainWindowBase;
class QSplitter;
class QLayout;

enum class PaneAction {
    // commands on data in the pane
    _Begin = 0,
    _BeginData = _Begin,
    ShowAll = _BeginData,
    ExpandAll,
    CollapseAll,
    SelectAll,
    SelectNone,
    ResizeToFit,
    SetFiltersVisible,
    ViewAsTree,
    CopySelected,
    _EndData,

    // commands on the pane itself
    _BeginPane = _EndData,
    PaneAdd    = _BeginPane,
    PaneAddGroup,
    PaneReplace,
    PaneSplitH,
    PaneSplitV,
    PaneClose,
    PaneLeft,
    PaneRight,
    PaneRowUp,
    PaneRowDown,
    PaneBalanceSiblings,
    PaneBalanceTab,
    PaneAddTab,
    PaneRenameTab,
    PaneCloseTab,
    _EndPane,
    _Count = _EndPane
};

// Class for operations provided by UI panels
class PaneBase : public QGroupBox, public Settings
{
    Q_OBJECT

public:
    typedef QGroupBox Superclass;
    typedef PaneGroupBase Container;

    PaneBase(MainWindowBase& mainWindow, int paneClass, QWidget* parent);
    virtual ~PaneBase() override;

    // *** begin Pane API
    virtual void showAll() { }
    virtual void expandAll() { }
    virtual void collapseAll() { }
    virtual void selectAll() { }
    virtual void selectNone() { }
    virtual void resizeToFit(int /*defer*/ = -1) { }
    virtual void setFiltersVisible(bool);
    virtual void copySelected() const { }
    virtual void focusIn() { }
    virtual void focusOut() { }
    virtual void setQuery(const QString&) { }
    virtual QString getQuery() const { return ""; }

    virtual void sort() { } // trigger resort, to avoid doing it on every data change

    virtual bool isSelected(const QModelIndex&) const { return false; }
    virtual bool hasSelection() const { return false; }
    virtual bool hasItems() const { return true; }
    virtual QModelIndexList getSelections() const;
    virtual QItemSelectionModel* selectionModel() const { return nullptr; }
    virtual void newConfig() { }
    virtual bool supportsAction(PaneAction) const { return true; }

    virtual void viewAsTree(bool /*asTree*/) { }
    virtual bool viewIsTree() const { return false; }
    virtual void paneToggled(bool checked);

    virtual PaneBase* clone() const;
    // *** end Pane API

    static bool needsFocus(PaneAction);
    static bool separatorAfter(PaneAction pa);

    static const constexpr char* containerName = "PaneContainer";

    // *** begin Settings API
    virtual void save(QSettings&) const override;
    virtual void load(QSettings&) override;
    // *** end Settings API

    virtual void postLoadHook(); // things we can only do after the model data is loaded.

    int paneId() const { return m_paneId; } // query unique Id

protected slots:
    virtual void showPaneContextMenu(const QPoint &pos);

protected:
    friend class UndoPaneBase;
    friend class UndoPaneState;

    virtual const CfgDataBase& cfgData() const;
    const MainWindowBase& mainWindow() const { return m_mainWindow; }
    const MainWindowBase& constMainWindow() { return m_mainWindow; }
    MainWindowBase& mainWindow() { return m_mainWindow; }

    void setPaneFilterBar(QWidget* fb) { paneFilterBar = fb; }
    void setPaneFilterBar(QLayout* fb) { paneFilterLayout = fb; }
    void setFiltersVisible(bool visible, QLayout* layout);
    void setFiltersVisible(bool visible, QSplitter* splitter);
    void setFiltersVisible(bool visible, QWidget* splitter);
    void disableToolTipsFor(QObject* child);

    bool eventFilter(QObject* obj, QEvent* event) override;

    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void reenableChildren();

    virtual void addContextMenuRange(QMenu* menu, PaneAction begin, PaneAction end);
    virtual void setupPaneHeaderContextMenus();
    virtual void setupActionContextMenu(QMenu& contextMenu);

    void setupViewContextMenu(QMenu& contextMenu);
    void setupPaneContextMenu(QMenu& contextMenu);

    MainWindowBase&  m_mainWindow;     // main window reference
    QMenu            paneMenu;         // for pane operations (add/remove/etc)
    QWidget*         paneFilterBar;    // pane's filter bar area if widget, or nullptr
    QLayout*         paneFilterLayout; // pane's filter bar area if layout, or nullptr
    QObject*         contentArea;      // for hiding tooltips
    int              thisClass;        // pane class ID
    int              m_paneId;         // unique ID

    template <typename T> void deleteUI(T& ui) {
        delete ui;
        ui = nullptr;
    }

    PaneBase(const PaneBase&) = delete;
    PaneBase& operator=(const PaneBase&) = delete;
};

#endif // PANEBASE_H
