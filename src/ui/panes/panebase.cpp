/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSplitter>
#include <QLayout>
#include <QTemporaryFile>

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/ui/windows/mainwindowbase.h>

#include "panebase.h"

PaneBase::PaneBase(MainWindowBase& mainWindow, int paneClass, QWidget* parent) :
    QGroupBox(parent),
    m_mainWindow(mainWindow),
    paneMenu(QObject::tr("Pane Control")),
    paneFilterBar(nullptr),
    paneFilterLayout(nullptr),
    contentArea(nullptr),
    thisClass(paneClass),
    m_paneId(mainWindow.nextPaneId())
{
}

PaneBase::~PaneBase()
{
}

void PaneBase::paneToggled(bool checked)
{
    setFiltersVisible(checked);
    reenableChildren();
}

PaneBase *PaneBase::clone() const
{
    // save this pane settings to temp file, then reload new pane's settings
    QTemporaryFile paneStateTempFile;
    QSettings paneState(paneStateTempFile.fileName(), QSettings::IniFormat);

    SL::Save(paneState, "clonedPane", *this);
    PaneBase* newPane = static_cast<PaneBase*>(mainWindow().paneFactory(thisClass));

    if (newPane)
        SL::Load(paneState, "clonedPane", *newPane);

    return newPane;
}

void PaneBase::showPaneContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

const CfgDataBase &PaneBase::cfgData() const
{
    return mainWindow().cfgData();
}

void PaneBase::setFiltersVisible(bool visible)
{
    if (paneFilterLayout != nullptr)
        setFiltersVisible(visible, paneFilterLayout);
    else if (QSplitter* splitter = dynamic_cast<QSplitter*>(paneFilterBar); splitter != nullptr)
        setFiltersVisible(visible, splitter);
    else if (paneFilterBar != nullptr)
        setFiltersVisible(visible, paneFilterBar);
}

// Disable all under layout
void PaneBase::setFiltersVisible(bool visible, QLayout* layout)
{
    setChecked(visible);

    for (int i = 0; i < layout->count(); ++i)
        if (QWidget* widget = layout->itemAt(i)->widget())
            widget->setVisible(visible);
}

// Disable all under splitter
void PaneBase::setFiltersVisible(bool visible, QSplitter *splitter)
{
    setChecked(visible);

    for (int i = 0; i < splitter->count(); ++i)
        if (QWidget* widget = splitter->widget(i))
            widget->setVisible(visible);
}

// Disable all under splitter
void PaneBase::setFiltersVisible(bool visible, QWidget *widget)
{
    setChecked(visible);
    widget->setVisible(visible);
}

QModelIndexList PaneBase::getSelections() const
{
    return QModelIndexList();
}

// After re-using the group checkbox for something else, we have to re-enable all
// the children.
void PaneBase::reenableChildren()
{
    for (int w = 0; w < layout()->count(); ++w)
        if (QWidget* widget = layout()->itemAt(w)->widget())
            widget->setEnabled(true);
}

void PaneBase::mousePressEvent(QMouseEvent *event)
{
    setFocus();     // focus on title click
    QGroupBox::mousePressEvent(event);
}

void PaneBase::disableToolTipsFor(QObject *child)
{
    if (child == nullptr)
        return;

    contentArea = child;
    child->installEventFilter(this);
}

bool PaneBase::eventFilter(QObject* obj, QEvent* event)
{
    if (obj == contentArea && event->type() == QEvent::ToolTip)
        return true;

    return QGroupBox::eventFilter(obj, event);
}

bool PaneBase::separatorAfter(PaneAction pa)
{
    switch (pa) {
    case PaneAction::ShowAll:           [[fallthrough]];
    case PaneAction::SelectNone:        [[fallthrough]];
    case PaneAction::ViewAsTree:        [[fallthrough]];
    case PaneAction::PaneClose:         [[fallthrough]];
    case PaneAction::PaneRowDown:       [[fallthrough]];
    case PaneAction::PaneBalanceTab:    return true;
    default:                            return false;
    }
}

bool PaneBase::needsFocus(PaneAction pa)
{
    switch (pa) {
    case PaneAction::PaneAdd:         [[fallthrough]];
    case PaneAction::PaneAddGroup:    [[fallthrough]];
    case PaneAction::PaneAddTab:      [[fallthrough]];
    case PaneAction::PaneRenameTab:   [[fallthrough]];
    case PaneAction::PaneBalanceTab:  return false;
    default:                          return true;
    }
}

void PaneBase::addContextMenuRange(QMenu* menu, PaneAction begin, PaneAction end)
{
    if (menu == nullptr)
        return;

    for (PaneAction pa = begin; pa < end; Util::inc(pa)) {
        if (!supportsAction(pa))  // skip ones that don't apply to this pane type
            continue;

        if (pa == PaneAction::PaneAdd)
            if (QMenu* addMenu = menu->addMenu(Icons::get("window-new"), "Add Pane in Group"))
                addMenu->addActions(mainWindow().paneClassAddActions());

        if (pa == PaneAction::PaneAddGroup)
            if (QMenu* addMenu = menu->addMenu(Icons::get("window-new"), "Add Group Sibling"))
                addMenu->addActions(mainWindow().paneClassGrpActions());

        if (pa == PaneAction::PaneReplace)
            if (QMenu* replaceMenu = menu->addMenu(Icons::get("window-duplicate"), "Replace Pane"))
                replaceMenu->addActions(mainWindow().paneClassRepActions());

        if (QAction* action = mainWindow().getPaneAction(pa)) {
            menu->addAction(action);  // add action

            if (PaneBase::separatorAfter(pa))
                menu->addSeparator();
        }
    }
}

void PaneBase::setupViewContextMenu(QMenu& contextMenu)
{
    if (QMenu* viewMenu = contextMenu.addMenu("View"))
        addContextMenuRange(viewMenu, PaneAction::_BeginData, PaneAction::_EndData);
}

void PaneBase::setupPaneContextMenu(QMenu& contextMenu)
{
    if (QMenu* paneMenu = contextMenu.addMenu("Pane"))
        addContextMenuRange(paneMenu, PaneAction::_BeginPane, PaneAction::_EndPane);
}

void PaneBase::setupActionContextMenu(QMenu& contextMenu)
{
    setupViewContextMenu(contextMenu);
    setupPaneContextMenu(contextMenu);
}

void PaneBase::setupPaneHeaderContextMenus()
{
    addContextMenuRange(&paneMenu, PaneAction::_BeginPane, PaneAction::_EndPane);

    // pane context menu
    connect(this, &PaneBase::customContextMenuRequested, this, &PaneBase::showPaneContextMenu);

    setContextMenuPolicy(Qt::CustomContextMenu);
}

void PaneBase::save(QSettings& settings) const
{
    SL::Save(settings, "filtersVisible", isChecked());
    SL::Save(settings, "viewIsTree",     viewIsTree());
    SL::Save(settings, "paneClass",      thisClass);
    SL::Save(settings, "hasFocus",       hasFocus());

    if (QSplitter* splitter = dynamic_cast<QSplitter*>(paneFilterBar); splitter != nullptr)
        SL::Save(settings, "filterBarState", splitter->saveState());
}

void PaneBase::load(QSettings& settings)
{
    paneToggled(SL::Load(settings, "filtersVisible", true));
    viewAsTree(SL::Load<bool>(settings, "viewIsTree", true));

    if (SL::Load<bool>(settings, "hasFocus", false))
        mainWindow().setPostLoadFocus(this);

    if (QSplitter* splitter = dynamic_cast<QSplitter*>(paneFilterBar); splitter != nullptr)
        if (settings.contains("filterBarState"))
            splitter->restoreState(settings.value("filterBarState").toByteArray());
}

void PaneBase::postLoadHook()
{
    expandAll();
    resizeToFit();
}
