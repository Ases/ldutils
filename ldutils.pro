#-------------------------------------------------
#
# Project created by QtCreator 2019-04-09T12:12:23
#
#-------------------------------------------------

QT       += core gui widgets svg xml

CONFIG(debug, debug|release) {
   TARGET = ldutilsd
} else {
   TARGET = ldutils
}

TEMPLATE = lib
CONFIG += staticlib

PKGVERSION = $$cat(VERSION)    # package version
PKGVERSION = $$replace(PKGVERSION, "\"", "")

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES   += $$files(src/*.cpp, true)
HEADERS   += $$files(src/*.h, true)
FORMS     += $$files(src/*.ui, true)

# for "make tar"
tar.target   = tar
tar.commands = tar --transform="s:^[.]/:./$${TARGET}-$${PKGVERSION}/:" -C "$$PWD" --exclude '*.pro.user' --exclude-vcs --exclude-backups --owner loopdawg --group loopdawg --xz -cvf "/tmp/$${TARGET}-$${PKGVERSION}.tar.xz" .

QMAKE_EXTRA_TARGETS += tar

unix {
    QMAKE_LFLAGS_RELEASE='-Wl,--strip-all,--dynamic-list-cpp-typeinfo'
    QMAKE_LFLAGS_DEBUG='-Wl,--dynamic-list-cpp-typeinfo'
    QMAKE_CXXFLAGS += -std=c++17
    QMAKE_CXXFLAGS_RELEASE += -DNDEBUG

    # put includes and lib in a sibling directory by default, but override with env variable if set.
    LDUTILS_INCLUDE = $$(LDUTILS_INCLUDE)
    LDUTILS_LIB = $$(LDUTILS_LIB)

    isEmpty(LDUTILS_INCLUDE) {
        LDUTILS_INCLUDE = $$clean_path($$_PRO_FILE_PWD_/../libldutils)
    }
    isEmpty(LDUTILS_LIB) {
        LDUTILS_LIB = $$clean_path($$_PRO_FILE_PWD_/../libldutils)
    }

    for(header, HEADERS) {
        file = headers_$${header}

        eval($${file}.files += $${header})
        eval($${file}.path = $${LDUTILS_INCLUDE}/$$dirname(header))
        INSTALLS *= $${file}
    }

    TARGET_MULTIARCH = $$system(gcc -print-multiarch)
 
    target.path   = $${LDUTILS_LIB}/lib/$${TARGET_MULTIARCH}

    INSTALLS += target
}
